// Copyright 13-Apr-2023 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Version 2023.04

import haxe.io.Path;
import Simix;
import Fileix;
import Modules;
import reader.Reader;
import reader.cdr.Cdr;

class Main {
  // Path without directory.
  static final mainKut = "jskut/t4";

static final tmpMds: Array<String> = [];

  public static function run () {
    trace("Run " + Fileix.paths[0]);
  }

  /// Application entry
  public static function main() {
    Simix.init();

    final fMainKut = Path.normalize(mainKut);
    Fileix.setRoot(Path.directory(fMainKut));

    final path = Path.withoutDirectory(fMainKut) + ".jkut";
    final toRead = [[fMainKut, path]];
    var end = false;
    function read () {
      function readCode (path: String, code: String) {
        final fix = Fileix.add(path);
        final md = Reader.readMainBlock(new Cdr(fix, code));
        Modules.array[fix] = md;
        for (imp in md.is) {
          final imp = Fileix.paths[imp.fix];
          toRead.push([path, imp]);
        }
      }

      if (toRead.length == 0) {
        end = true;
        return;
      }
      final paths = toRead.pop();

      final path1 = Path.join([Path.directory(paths[0]), paths[1]]);
      final path2 = Path.join([Fileix.getRoot(), paths[1]]);
      if (Fileix.paths.contains(path1) || Fileix.paths.contains(path2)) {
        read();
        return;
      }
      Fileix.read(path1, tx -> {
        if (tx == null) {
          Fileix.read(path2, tx -> {
            if (tx == null) {
              final mdPath = paths[1].substring(0, paths[1].length - 5);
              throw(new haxe.Exception("Module " + mdPath + " not found"));
            } else {
              readCode(path2, tx);
              read();
            }
          });
        } else {
          readCode(path1, tx);
          read();
        }
      });
    }

    read();
    final tm = new haxe.Timer(10);
    tm.run = () -> {
      if (end) {
        tm.stop();
        run();
      }
    };
  }
}
