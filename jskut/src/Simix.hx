// Copyright 13-Apr-2023 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Object for mapping indexes to symbols.

import kut.Test.eq;

class Simix {
  public static final BREAK = 0;
  public static final CATCH = 1;
  public static final CONTINUE = 2;
  public static final DEFAULT = 3;
  public static final ELSE = 4;
  public static final FALSE = 5;
  public static final FINALLY = 6;
  public static final FOR = 7;
  public static final IF = 8;
  public static final IMPORT = 9;
  public static final RETURN = 10;
  public static final SWITCH = 11;
  public static final TRACE = 12;
  public static final TRUE = 13;
  public static final TRY = 14;
  public static final WHILE = 15;

  public static final ENUM_SEPARATOR = 16;
  // Built-in modules
  public static final ARR = 17;
  public static final B64 = 18;
  public static final BYTES = 19;
  public static final CRYP = 20;
  public static final DIC = 21;
  public static final ITER = 22;
  public static final JS = 23;
  public static final FILE = 24;
  public static final MATH = 25;
  public static final PATH = 26;
  public static final REGEX = 27;
  public static final STR = 28;
  public static final SYS = 29;
  public static final TCP = 30;
  public static final THREAD = 31;
  public static final TIME = 32;

  public static final ENUM_END = 33;

  static final syms = [
    // Reserverd words
    "break", "catch", "continue", "default",
    "else", "false", "finally", "for",
    "if", "import", "return", "switch",
    "trace", "true", "try", "while",
    "-separator-",
    // Built-in modules
    "arr", "b64", "bytes", "cryp",
    "dic", "iter", "js", "file",
    "math", "path", "regex", "str",
    "sys", "tcp", "thread", "time",
    "-finalizador-"
  ];

  /// Initialize symbols array.
  public static function init () {
    eq(syms[BREAK], "break");
    eq(syms[CATCH], "catch");
    eq(syms[CONTINUE], "continue");
    eq(syms[DEFAULT], "default");

    eq(syms[ELSE], "else");
    eq(syms[FALSE], "false");
    eq(syms[FINALLY], "finally");
    eq(syms[FOR], "for");

    eq(syms[IF], "if");
    eq(syms[IMPORT], "import");
    eq(syms[RETURN], "return");
    eq(syms[SWITCH], "switch");

    eq(syms[TRACE], "trace");
    eq(syms[TRUE], "true");
    eq(syms[TRY], "try");
    eq(syms[WHILE], "while");

    eq(syms[ARR], "arr");
    eq(syms[B64], "b64");
    eq(syms[BYTES], "bytes");
    eq(syms[CRYP], "cryp");

    eq(syms[DIC], "dic");
    eq(syms[ITER], "iter");
    eq(syms[JS], "js");
    eq(syms[FILE], "file");

    eq(syms[MATH], "math");
    eq(syms[PATH], "path");
    eq(syms[REGEX], "regex");
    eq(syms[STR], "str");

    eq(syms[SYS], "sys");
    eq(syms[TCP], "tcp");
    eq(syms[THREAD], "thread");
    eq(syms[TIME], "time");
  }

  /// If 'sym' is not in symbols array], it is added. Otherwise do nothing.
  /// In any case this function returns ever the symbol index.
  public static function add (sym: String): Int {
    final ix = syms.indexOf(sym);
    if (ix != -1) return ix;
    syms.push(sym);
    return syms.length - 1;
  }

  /// Returns the symnbol with index 'ix'.
  /// If such index does no exist, it throw an exception.
  public static function get (ix: Int): String {
    return syms[ix];
  }

}
