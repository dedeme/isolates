// Copyright 14-Apr-2023 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Exceptions maker.

import haxe.Exception;

class Exc {
  public static function mk (msg: String): Exception {
    return new Exception(msg);
  }

  public static function mkIllegalArgument(
    msg: String, expected: String, actual: String
  ): Exception {
    return mk(
      msg +
      "\nExpected: " + expected +
      "\n  Actual: " + actual
    );
  }
}

