// Copyright 13-Apr-2023 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Object for mapping indexes to files.

class Fileix {
  static var root = "";

  /// Module paths.
  public static final paths: Array<String> = [];

  /// Set the program working directory.
  public static function setRoot (root: String): Void {
    Fileix.root = root;
  }

  /// Returns the program working directory.
  public static function getRoot (): String {
    return root;
  }

  /// Adds 'ipath' to code file paths array and returns its index.
  /// If path already has been added, it do nothing and also returns its index.
  ///    path: A normalized file path with the extesion ".kut":
  public static function add (path: String): Int {
    final ix = paths.indexOf(path);
    if (ix != -1) return ix;

    paths.push(path);
    return paths.length - 1;
  }

  /// Returns the path with index 'ix' "shorted" to 50 bytes, to print with
  /// print - trace (Without extension and without making it canonical).
  public static function toStr (ix: Int): String {
    if (ix < 0) return "Built-in";
    final path = paths[ix];
    var s = path.substring(0, path.length - 5);
    if (s.length > 50) s = "..." + s.substring(s.length - 47);
    return s;
  }

  /// Returns the path with index 'ix' to print in error messages. It is with
  /// extension and made canonical.
  public static function toFail (ix: Int): String {
    if (ix < 0) return "Built-in";
    return paths[ix];
  }

  /// Read asynchronically the file 'f' and execute 'fcode' with
  /// its text.
  /// Throw an exception if file can not be found.
  public static function read (f: String, fcode: String -> Void): Void {
    js.Syntax.code("
      fetch({0})
        .then(response => {
          if (!response.ok) {
            return null;
          }
          return response.text();
        })
        .then(tx => fcode(tx))
        .catch(() => fcode(null));",
      f,
      fcode
    );
  }
}
