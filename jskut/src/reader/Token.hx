// Copyright 14-Apr-2023 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Immutable classses of jkut tokens.

package reader;

import Exc;

class Token {
  public static final BOOL = 0;
  public static final INT = 1;
  public static final FLOAT = 2;
  public static final STRING = 3;
  public static final LINE_COMMENT = 4;
  public static final COMMENT = 5;
  public static final SYMBOL = 6;
  public static final OPERATOR = 7;

  public final type: Int;
  public final value: Dynamic;

  function new (type: Int, value: Dynamic) {
    this.type = type;
    this.value = value;
  }

  /// Creates a token of the indicated type.
  public static function mkBool (value: Bool): Token {
    return new Token(BOOL, value);
  }

  /// Creates a token of the indicated type.
  public static function mkInt (value: Int): Token {
    return new Token(INT, value);
  }

  /// Creates a token of the indicated type.
  public static function mkFloat (value: Float): Token {
    return new Token(FLOAT, value);
  }

  /// Creates a token of the indicated type.
  public static function mkString (value: String): Token {
    return new Token(STRING, value);
  }

  /// Creates a token of the indicated type.
  public static function mkLineComment (value: String): Token {
    return new Token(LINE_COMMENT, value);
  }

  /// Creates a token of the indicated type.
  public static function mkComment (value: String): Token {
    return new Token(COMMENT, value);
  }

  /// Creates a token of the indicated type.
  public static function mkSymbol (value: String): Token {
    return new Token(SYMBOL, value);
  }

  /// Creates a token of the indicated type.
  public static function mkOperator (value: String): Token {
    return new Token(OPERATOR, value);
  }

  /// Returns TRUE if 'this' is an unary operator.
  public function isUnary (): Bool {
    return type == OPERATOR && (value == "!" || value == "-");
  }

  /// Returns TRUE if 'this' is a binary operator.
  public function isBinary (): Bool {
    return type == OPERATOR && (
      value == "+" || value == "-" || value == "==" ||
      value == "*" || value == "/" || value == "%" ||
      value == "!=" || value == ">" || value == ">=" ||
      value == "<" || value == "<=" || value == "&" ||
      value == "|"
    );
  }

  /// Returns TRUE if 'this' is a binary operator of precedence '1'
  /// ("*", "/", "%").
  public function isBinary1 (): Bool {
    return type == OPERATOR && (
      value == "*" || value == "/" || value == "%"
    );
  }

  /// Returns TRUE if 'this' is a binary operator of precedence '2'
  /// ("+", "-").
  public function isBinary2 (): Bool {
    return type == OPERATOR && (value == "+" || value == "-" );
  }

  /// Returns TRUE if 'this' is a binary operator of precedence '3'
  /// ("==", "!=", ">", ">=", "<", "<=").
  public function isBinary3 (): Bool {
    return type == OPERATOR && (
      value == "==" || value == "!=" || value == ">" ||
      value == ">=" || value == "<" || value == "<="
    );
  }

  /// Returns TRUE if 'this' is a binary operator of precedence '4'
  /// ("&", "|").
  public function isBinary4 (): Bool {
    return type == OPERATOR && (value == "&" || value == "|" );
  }

  /// Returns TRUE if 'this' is the ternary operator.
  public function isTernary (): Bool {
    return type == OPERATOR && value == "?";
  }

  /// Returns TRUE if 'this' is an operator of the indicated type.
  /// ("=", "+=", "-=", "*=", "/=", "%=", "|=", "&=")
  public function isAssign (): Bool {
    return type == OPERATOR && (
      value == "=" || value == "+=" || value == "-=" ||
      value == "*=" || value == "/=" || value == "%=" ||
      value == "&=" || value == "|="
    );
  }

  /// Returns TRUE if 'this' is an operator of the indicated type.
  /// ("=")
  public function isEquals (): Bool {
    return type == OPERATOR && value == "=";
  }

  /// Returns TRUE if 'this' is an operator of the indicated type.
  /// (".")
  public function isPoint (): Bool {
    return type == OPERATOR && value == ".";
  }

  /// Returns TRUE if 'this' is an operator of the indicated type.
  /// (",")
  public function isComma (): Bool {
    return type == OPERATOR && value == ",";
  }

  /// Returns TRUE if 'this' is an operator of the indicated type.
  /// (":")
  public function isColon (): Bool {
    return type == OPERATOR && value == ":";
  }

  /// Returns TRUE if 'this' is an operator of the indicated type.
  /// (";")
  public function isSemicolon (): Bool {
    return type == OPERATOR && value == ";";
  }

  /// Returns TRUE if 'this' is an operator of the indicated type.
  /// ("!")
  public function isExclamation (): Bool {
    return type == OPERATOR && value == "!";
  }

  /// Returns TRUE if 'this' is an operator of the indicated type.
  /// ("\")
  public function isBackslash (): Bool {
    return type == OPERATOR && value == "\\";
  }

  /// Returns TRUE if 'this' is an operator of the indicated type.
  /// ("->")
  public function isArrow (): Bool {
    return type == OPERATOR && value == "->";
  }

  /// Returns TRUE if 'this' is an operator of the indicated type.
  /// ("(")
  public function isOpenPar (): Bool {
    return type == OPERATOR && value == "(";
  }

  /// Returns TRUE if 'this' is an operator of the indicated type.
  /// (")")
  public function isClosePar (): Bool {
    return type == OPERATOR && value == ")";
  }

  /// Returns TRUE if 'this' is an operator of the indicated type.
  /// ("[")
  public function isOpenSquare (): Bool {
    return type == OPERATOR && value == "[";
  }

  /// Returns TRUE if 'this' is an operator of the indicated type.
  /// ("]")
  public function isCloseSquare (): Bool {
    return type == OPERATOR && value == "]";
  }

  /// Returns TRUE if 'this' is an operator of the indicated type.
  /// ("{")
  public function isOpenBracket (): Bool {
    return type == OPERATOR && value == "{";
  }

  /// Returns TRUE if 'this' is an operator of the indicated type.
  /// ("}")
  public function isCloseBracket (): Bool {
    return type == OPERATOR && value == "}";
  }

  /// Returns TRUE if 'this' is the symbol 'else'.
  public function isElse (): Bool {
    return type == SYMBOL && value == "else";
  }

  /// Returns TRUE if 'this' is the symbol 'catch'.
  public function isCatch (): Bool {
    return type == SYMBOL && value == "catch";
  }

  /// Returns TRUE if 'this' is the symbol 'finally'.
  public function isFinally (): Bool {
    return type == SYMBOL && value == "finally";
  }

  ///
  public function typeToStr (): String {
    switch (type) {
      case BOOL: return "Bool";
      case INT: return "Int";
      case FLOAT: return "Float";
      case STRING: return "String";
      case LINE_COMMENT: return "Line_comment";
      case COMMENT: return "Comment";
      case SYMBOL: return "Symbol";
      case OPERATOR: return "Operator";
      default: throw(Exc.mkIllegalArgument(
          "Bad token type identifier",
          "0 to " + OPERATOR,
          "" + type
        ));
    }
  }

  ///
  public function toString (): String {
    switch (type) {
      case BOOL: return "Bool: " + (value ? "true" : "false");
      case INT: return "Int: " + value;
      case FLOAT: return "Float: " + value;
      case STRING: return "String: " + value;
      case LINE_COMMENT: return "Line_comment: " + value;
      case COMMENT: return "Comment: " + value;
      case SYMBOL: return "Symbol: " + value;
      case OPERATOR: return "Operator: " + value;
      default: throw (Exc.mkIllegalArgument(
          "Bad token type identifier",
          "0 to " + OPERATOR,
          "" + type
        ));
    };
  }

}

