// Copyright 14-Apr-2023 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Code reader

package reader;

import reader.cdr.Cdr;
import Modules;
import Imports;

class Reader {
  public static function readMainBlock (cdr: Cdr): Module {
    return {
      is: new Imps()
    };
  }
}
