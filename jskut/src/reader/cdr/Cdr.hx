// Copyright 14-Apr-2023 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Low level code reader.

package reader.cdr;

import reader.Token;
import reader.cdr.CommentReader;

class Cdr {
  /// File index.
  public final fix: Int;

  /// Code.
  public final code: String;

  final len: Int;
  var codeIx: Int;
  var nline: Int;
  var nextTk: Token;
  var nextNline: Int;


  /// Constructor
  ///   fix: File index.
  ///   code: Code of the corresponding file.
  public function new (fix: Int, code: String) {
    this.fix = fix;
    this.code = code;
    len = code.length;
    codeIx = 0;
    nline = 1;
    nextNline = 1;
    nextTk = readToken0();
  }

  function readOperator (ch: String): Token {
    switch (ch) {
      case "=", "!", ">", "<", "+", "*", "/", "%", "|", "&" : {
          final ch2 = readChar();
          if (ch2 == "=") return Token.mkOperator(ch + "=");
          unreadChar(ch2);
        }
      case "-": {
          final ch2 = readChar();
          if (ch2 == "=" || ch == ">") return Token.mkOperator(ch + ch2);
          unreadChar(ch2);
        }
    }
    return Token.mkOperator(ch);
  }

  // Returns NULL at end of text.
  function readToken0 (): Token {
    final ch = readChar();
    switch (ch) {
      case "": return null;
      case "/": {
          final tk = CommentReader.read(this);
          return tk.type == Token.OPERATOR
            ? readOperator("/") // Operator '/'
            : readToken0() // Skip comment
          ;
        }
      case "'", "\"": return null;
      default: return readOperator(ch);
    }
  }

  // PUBLIC INTERFACE

  public function getNextNline () {
    return nextNline;
  }

  /// Returns "" if end of code is reached.
  public function readChar (): String {
    if (codeIx < len) {
      final ch = code.charAt(codeIx);
      codeIx += 1;
      if (ch == "\n") nextNline += 1;
      return ch;
    }
    return "";
  }

  /// Back to the previous character. If the current character is in position 0
  /// an exception is thrown.
  public function unreadChar (ch: String): Void {
    if (codeIx == 0) throw (Exc.mk("Backing from code start"));
    if (ch == "\n") nextNline += 1;
    codeIx -= 1;
  }

  /// Returns an error message.
  public function failLine (msg: String, nline: Int): String {
    return msg + "\n  " + Fileix.toFail(fix) + ":" + nline + ":";
  }

  /// Returns an error message with the current line number.
  public function fail (msg: String): String {
    return failLine(msg, nline);
  }

  /// Returns an expected-found error message.
  public function failExpectLine (
    msg: String, expected: String, found: String, nline: Int
  ): String {
    return failLine("Expected: " + expected + "%s\nFound   : " + found, nline);
  }

  /// Returns an expected-found error message with the current line number.
  public function failExpect (
    msg: String, expected: String, found: String
  ): String {
    return failExpectLine(msg, expected, found, nline);
  }
}
