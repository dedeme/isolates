// Copyright 14-Apr-2023 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Comment reader.

package reader.cdr;

import reader.Token;
import reader.cdr.Cdr;

class CommentReader {

  static function readLineComment (cdr: Cdr, bf: StringBuf): Token {
    while (true) {
      final ch = cdr.readChar();
      switch (ch) {
        case "", "\n": return Token.mkLineComment(bf.toString());
        default: bf.add(ch);
      }
    }
  }

  static function readComment (cdr: Cdr, bf: StringBuf): Token {
    final nline = cdr.getNextNline();
    while (true) {
      final ch = cdr.readChar();
      switch (ch) {
        case "": throw(Exc.mk(cdr.failLine("Unclosed comment.", nline)));
        case "*": {
            final ch2 = cdr.readChar();
            if (ch2 == "")
              throw(Exc.mk(cdr.failLine("Unclosed comment.", nline)));
            if (ch2 == "/")
              return Token.mkComment(bf + "*/");
            bf.add(ch + ch2);
          }
        default: bf.add(ch);
      }
    }
  }

  /// Read a comment token (multiline or monoline) or an operator token ('/')
  /// Throws Exception.
  public static function read (cdr: Cdr): Token {
    final bf = new StringBuf();
    bf.add("/");
    final ch = cdr.readChar();
    switch (ch) {
      case "": throw(Exc.mk(
          cdr.failLine("Unexpected end of file.", cdr.getNextNline())
        ));
      case "/":
        bf.add("/");
        return readLineComment(cdr, bf);
      case "*":
        bf.add("*");
        return readComment(cdr, bf);
      default:
        cdr.unreadChar(ch);
        return Token.mkOperator("/");
    }
  }
}
