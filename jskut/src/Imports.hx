// Copyright 13-Apr-2023 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Map from symbol to file index

/// Imports array
typedef Imps = Array<Import>;

/// Record
typedef Import = {
  final symbol: Int;
  final fix: Int;
};

class Imports {
  public static function add (is: Imps, symbol: Int, fix: Int): Void {
    is.push({symbol: symbol, fix: fix});
  }

  public static function getFix (is: Imps, symbol: Int): Int {
    for (e in is) if (e.symbol == symbol) return e.fix;
    return -1;
  }
}
