// Copyright 13-Apr-2023 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Management of an Array<Module>, where each index match the
/// same file index of the corresponding module.
/// NOTE 1:
///   - A module has an index (ix) in the array.
///   - A module is defined in a file 'f'.
///   - 'f' has a file index 'fix'.
///   - 'ix' == 'fix'

import Imports;

///
typedef Mods = Array<Module>

///
typedef Module = {
  final is: Imps;
};

class Modules {
  public static final array : Mods = [];
}

