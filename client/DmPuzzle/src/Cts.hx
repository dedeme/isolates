// Copyright 02-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

import dm.Ui;
import dm.Ui.Q;
import dm.Client;
import I18n._;
import I18n._args;
import dm.Dimension;
import dm.Point;
import dm.Board;
import dm.Device;

/// Direction
enum Direction { UP; LEFT; DOWN; RIGHT; }

/// Constants and global functions.
class Cts {
  /// Application name.
  public static final appName = "DmPuzzle";
  /// Application version.
  public static final appVersion = "202012";
  /// Store key.
  public static final storeKey = appName + "__data";
  /// Level names.
  public static var levels(get, never): Array<String>;
  static function get_levels () {
    return [_("Easy"), _("Medium"), _("Expert")];
  }
  /// Dimension of easy jigsaw.
  public static final easyDim = new Dimension(4, 3);
  /// Dimension of easy jigsaw.
  public static final mediumDim = new Dimension(8, 6);
  /// Dimension of easy jigsaw.
  public static final expertDim = new Dimension(12, 9);
  /// Parts number of each pice (both width and height)
  public static final partsN = 7;

  /// Page foot.
  public static final foot = Q("table")
    .klass("main")
    .add(Q("tr")
      .add(Q("td")
        .add(Q("hr"))))
    .add(Q("tr")
      .add(Q("td")
        .style("text-align: right;color:#808080;font-size:x-small;")
        .html('- © ºDeme. ${appName} (${appVersion}) -')))
  ;
  /// Default picture.
  public static final defaultPicture: Board = mkDefaultPicture();
  // Board dimension
  public static final boardDim: Dimension = Device.isMobil
    ? Device.isMobilH
      ? new Dimension(
          Std.int(Device.screen.w * 0.7), Std.int (Device.screen.h * 0.45)
        )
      : new Dimension(
          Std.int(Device.screen.w * 0.9), Std.int (Device.screen.h * 0.70)
        )
    : new Dimension(
        Std.int(Device.screen.w * 0.80), Std.int (Device.screen.h * 0.6)
      )
  ;
  /// jigsaw initial value of width.
  public static final jigsawVal: Float = Device.isMobilH
    ? boardDim.h * 0.7 * 4 / 3
    : Device.isMobilV
      ? boardDim.w * 0.7
      : boardDim.w / 2
  ;

  static function mkDefaultPicture (): Board {
    final board = new dm.Board(800, 600);
    final ctx = board.canvas.getContext2d();
    ctx.fillStyle='#ffffff';
    ctx.fillRect(0, 0, board.width, board.height);
    final i = Ui.img("logo");
    i.on(LOAD, e -> {
        var pattern = ctx.createPattern(cast(i.e, js.html.ImageElement), 'repeat');
        ctx.fillStyle = pattern;
        ctx.fillRect(0, 0, 800, 600);
      });
    return board;
  }

  /// Format time
  public static function formatTime (millis: Int): String {
    var mll = Std.string(millis % 1000);
    if (mll.length == 1) mll = "00" + mll;
    else if (mll.length == 2) mll = "0" + mll;

    var rs = Std.int(millis / 1000);
    var s = Std.string(rs % 60);
    if (s.length == 1) s = "0" + s;
    var rs = Std.int(rs / 60);
    var m = Std.string(rs % 60);
    if (m.length == 1) m = "0" + m;
    var h = Std.string(Std.int(rs / 60));
    if (h.length == 1) h = "0" + h;
    return h + ":" + m + ":" + s + "," + mll;
  }

  /// Make led
  public static function mkLed (selected = false) {
    var ratio = js.Browser.window.devicePixelRatio;
    if (ratio == null) ratio = 1;
    return selected
      ? Ui.led("#e9d9b9", Std.int(6 * ratio))
      : Ui.led("#b9d9e9", Std.int(6 * ratio))
    ;
  }

}
