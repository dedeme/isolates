// Copyright 02-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Application entry.
class Main {
  /// Application entry.
  static public function main (): Void {
    Model.init();
    View.init();
    Loader.load(() -> {
      View.update();
    });
  }
}
