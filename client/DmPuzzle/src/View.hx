// Copyright 22-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.Opt;
import dm.Rnd;
import I18n._;
import dm.Point;
import dm.Device;
import view.Index;
import view.Puzzle;
import data.Jigsaw;
import data.Piece;
import data.Group;

class View {

  public static final modalBoxDiv = Q("div");
  static final bodyWg = Q("td");
  static var index: Index;
  static var puzzle: Puzzle;

  /// Initializer.
  public static function init () {
    Q("@head")
      .add(Q("meta")
        .att("name", "lang")
        .att("content", js.Browser.navigator.language))
      .add(Q("link")
        .att("rel", "stylesheet")
        .att("href", Device.isBig ? "stylesM.css" : "styles.css")
        .att("type", "text/css"))
    ;

    Q("@body")
      .removeAll()
      .add(Q("table")
        .klass("main")
        .add(Q("tr").add(bodyWg))
        .add(Q("tr").add(Q("td").add(Cts.foot))))
      .add(modalBoxDiv)
    ;

    if (js.Browser.window.screen.orientation != null) {
      js.Browser.window.screen.orientation.onchange =
        e -> {
          Model.orientationChanged.setValue(true);
          js.Browser.location.reload();
        }
    }
  }

  public static function update (): Void {
    switch (Model.page.value) {
      case INDEX:
        Model.orientationChanged.setValue(false);
        index = new Index();
        index.show(bodyWg);
      case PICTURE(j):
        final orientationChanged = Model.orientationChanged.value;
        Model.orientationChanged.setValue(false);

        if (orientationChanged) {
          final grs = j.groups.copy();
          j = Jigsaw.mk(j.picture, j.level);
          j.start();
          final groups = j.groups;
          groups.splice(0, groups.length);
          j.nextGroupId = 0;

          Rnd.shuffle(grs);
          grs.sort((e1, e2) ->
            e2.dim.w * e2.dim.h - e1.dim.w * e1.dim.h
          );

          var ix = 0;
          var y = Cts.boardDim.h - j.dim.h * j.pieceSide;
          var x0 = Cts.boardDim.w - j.dim.w * j.pieceSide;
          for (r in 0...j.dim.h) {
            var x = x0;
            for (c in 0...j.dim.w) {
              if (ix >= grs.length) break;
              final gr = grs[ix++];

              final pieces: Array<Array<Option<Piece>>> = [];
              for (r in 0...gr.dim.h) {
                final a = [];
                for (c in 0...gr.dim.w) {
                  a.push(None);
                }
                pieces.push(a);
              }
              final newGroup = new Group(
                j.nextGroupId++, new Point(x, y), 500 - ix, j.pieceSide, pieces
              );
              newGroup.setPos(new Point(x, y));
              for (r in 0...gr.dim.h) {
                for (c in 0...gr.dim.w) {
                  switch(gr.pieces[r][c]) {
                    case Some(p):
                      final newp = j.getPiece(p.idRow, p.idCol);
                      newp.setGroup(newGroup.id);
                      newGroup.add(newp, r, c);
                    case None:
                  }
                }
              }
              j.groups.push(newGroup);
              j.normalizeZindex();

              x += j.pieceSide;
            }
            y += j.pieceSide;
          }

          Model.page.setValue(PICTURE(j));
          puzzle = new Puzzle(j);
          puzzle.show(bodyWg);
        } else {
          puzzle = new Puzzle(j);
          puzzle.show(bodyWg);
        }
    }
  }

}
