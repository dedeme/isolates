// Copyright 04-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view;

//import js.html.CanvasRenderingContext2D;
import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.Rnd;
import dm.Opt;
import dm.It;
import dm.Dt;
import I18n._;
import dm.Board;
import dm.Sprite;
import dm.Point;
import dm.Rectangle;
import dm.Pointer;
import data.Jigsaw;
import data.Piece;
import data.Group;

/// Puzzle page.
class Puzzle {
  function redrawPiece (piece: Piece, pieceWg: Board) {
    final ps = piece.partSide;
    final cs = ps * (Cts.partsN - 6);
    final cx = pieceWg.canvas.getContext2d();

    function up (x: Float, y) {
      cx.beginPath();
      cx.lineTo(x, y);
      x = x + ps * 2;
      cx.lineTo(x, y);
      y = y - ps;
      cx.lineTo(x, y);
      x = x + cs;
      cx.lineTo(x, y);
      y = y + ps;
      cx.lineTo(x, y);
      x = x + ps * 2;
      cx.lineTo(x, y);
      cx.stroke();
    }

    function down (x: Float, y: Float) {
      cx.beginPath();
      cx.lineTo(x, y);
      x = x + ps * 2;
      cx.lineTo(x, y);
      y = y + ps;
      cx.lineTo(x, y);
      x = x + cs;
      cx.lineTo(x, y);
      y = y - ps;
      cx.lineTo(x, y);
      x = x + ps * 2;
      cx.lineTo(x, y);
      cx.stroke();
    }

    function hline (x: Float, y: Float) {
      cx.beginPath();
      cx.lineTo(x, y);
      x = x + ps * 4 + cs;
      cx.lineTo(x, y);
      cx.stroke();
    }

    function left (x: Float, y) {
      cx.beginPath();
      cx.lineTo(x, y);
      y = y + ps * 2;
      cx.lineTo(x, y);
      x = x - ps;
      cx.lineTo(x, y);
      y = y + cs;
      cx.lineTo(x, y);
      x = x + ps;
      cx.lineTo(x, y);
      y = y + ps * 2;
      cx.lineTo(x, y);
      cx.stroke();
    }

    function right (x: Float, y: Float) {
      cx.beginPath();
      cx.lineTo(x, y);
      y = y + ps * 2;
      cx.lineTo(x, y);
      x = x + ps;
      cx.lineTo(x, y);
      y = y + cs;
      cx.lineTo(x, y);
      x = x - ps;
      cx.lineTo(x, y);
      y = y + ps * 2;
      cx.lineTo(x, y);
      cx.stroke();
    }

    function vline (x: Float, y: Float) {
      cx.beginPath();
      cx.lineTo(x, y);
      y = y + ps * 4 + cs;
      cx.lineTo(x, y);
      cx.stroke();
    }

    final blanks = piece.blanks();
    for (r in blanks.up) {
      cx.clearRect(r.x, r.y, r.w, r.h);
    }
    for (r in blanks.right) {
      cx.clearRect(r.x, r.y, r.w, r.h);
    }
    for (r in blanks.down) {
      cx.clearRect(r.x, r.y, r.w, r.h);
    }
    for (r in blanks.left) {
      cx.clearRect(r.x, r.y, r.w, r.h);
    }

    cx.lineWidth = 1;

    cx.strokeStyle = "#000000";
    var x = ps;
    var y = ps;
    if (piece.up == 1) up(x, y);
    else if (piece.up == -1) down(x, y);
    else hline(x, y);

    x = ps;
    y = ps * 5 + cs;
    cx.strokeStyle = "#000000";
    if (piece.down == 1) down(x, y);
    else if (piece.down == -1) up(x, y);
    else hline(x, y);

    cx.strokeStyle = "#000000";
    var x = ps;
    var y = ps;
    if (piece.left == 1) left(x, y);
    else if (piece.left == -1) right(x, y);
    else vline(x, y);

    x = ps * 5 + cs;
    y = ps;
    cx.strokeStyle = "#000000";
    if (piece.right == 1) right(x, y);
    else if (piece.right == -1) left(x, y);
    else vline(x, y);

  }

  /// Object -------------------------------------------------------------------

  final wg = Q("div");
  final jigsaw: Jigsaw;
  final board: Board;
  var pieces: Array<Array<Board>> = [];
  var groups: Map<Int, Sprite> = [];
  var draggedGroup: Group;

  public function new (jigsaw: Jigsaw) {
    this.jigsaw = jigsaw;

    board = new Board(Cts.boardDim.w, Cts.boardDim.h)
      .setBackground("#a0b0c0")
    ;

    final pcs = jigsaw.pieces;
    final side = jigsaw.pieceSide;
    final ds = jigsaw.pieceDisplazament;
    final img = jigsaw.image;
    var p = new Point(0, 0);
    for (r in 0...jigsaw.dim.h) {
      final a = [];
      for (c in 0...jigsaw.dim.w) {
        final pc = Opt.eget(
          It.from(jigsaw.pieces).find(e ->
            e.idRow == r && e.idCol == c
          )
        );

        final pcWg = img.cut(p.x, p.y, side, side);
        redrawPiece (pc, pcWg);
        a.push(pcWg);

        p = new Point(p.x + ds, p.y);
      }
      pieces.push(a);
      p = new Point(0, p.y + ds);
    }

    mkGroups();
  }

  function back () {
    Model.page.setValue(INDEX);
    View.update();
  }

  function changeAudio () {
    Model.audio.setValue(!Model.audio.value);
    update();
  }

  function save () {
    Ui.alert("save");
  }

  function start () {
    board
      .copyFrom(
        jigsaw.smallImage,
        jigsaw.partSide, jigsaw.partSide
      )
    ;

    final grs = jigsaw.groups.copy();
    final ps = jigsaw.pieceSide;
    Rnd.shuffle(grs);
    var ix = 0;
    var y = board.height - pieces.length * ps;
    var x0 = board.width - pieces[0].length * ps;
    for (r in 0...pieces.length) {
      var x = x0;
      for (c in 0...pieces[0].length) {
        if (ix >= grs.length) break;
        final gr = grs[ix++];
        gr.setPos(new Point(x, y));
        groups[gr.id].moveTo(x, y, 350);
        x += ps;
      }
      y += ps;
    }

    jigsaw.start();

    Model.page.setValue(PICTURE(jigsaw));
  }

  /// DOM ----------------------------------------------------------------------

  public function show (parent: Domo) {
    parent.removeAll().add(wg);
    update();
  }

  public function update () {
    board.clear();
    final ctx = board.canvas.getContext2d();
    var pattern = ctx.createPattern(
      cast(Loader.back2.e, js.html.ImageElement), 'repeat'
    );
    ctx.fillStyle = pattern;
    ctx.fillRect(0, 0, board.width, board.height);

    final actionDiv = Q("div");
    actionDiv
      .add(Ui.link(changeAudio)
        .add(Loader.mkIcon(Model.audio.value ? AUDIO_ON : AUDIO_OFF)))
      .add(Q("span")
        .html("&nbsp;&nbsp;&nbsp;&nbsp;"))
      .add(mkSave())
    ;

    if (jigsaw.started) {
      board
        .copyFrom(
          jigsaw.smallImage,
          jigsaw.partSide, jigsaw.partSide
        )
      ;

      for (jgr in It.from(jigsaw.groups).reverse().to()) {
        final p = jgr.pos;
        groups[jgr.id].put(p.x, p.y);
      }

    } else {
      for (jgr in jigsaw.groups) {
        final p = jgr.pos;
        groups[jgr.id].put(p.x, p.y);
      }

      haxe.Timer.delay(start, 1000);
    }

    wg
      .removeAll()
      .add(Q("table")
        .klass("main")
        .add(Q("tr")
        .add(Q("td")
          .style("text-align:left;width:5px;vertical-align:bottom")
          .add(Ui.link(back)
            .att("title", _("Back"))
            .add(Loader.mkIcon(LEFT))))
        .add(Q("td")
          .add(Q("div")
            .klass("title")
            .text(Cts.appName)))
        .add(Q("td")
          .style(
            "text-align:right;width:5px;vertical-align:bottom;" +
            "white-space:nowrap;"
          )
          .add(actionDiv))))
      .add(Q("table")
        .att("align", "center")
        .add(Q("tr")
          .add(Q("td")
            .klass("frame")
            .add(Q(board.wg)
              .klass("frame0")))))
    ;
  }

  function mkSave (): Domo {
    final link = Q("a")
      .att("download", "DmPuzzle" + Dt.to(Date.now()) + ".json")
      .att("hidden", "true")
    ;
    return Q("span")
      .add(link)
      .add(Ui.link(ev -> {
          link.att(
            "href",
            "data:application/octet-stream;base64," + Model.serialize()
          );
          link.e.click();
        })
        .add(Loader.mkIcon(SAVE))
        .att("title", _("Save")))
    ;
  }

  function mkGroups () {
    final side = jigsaw.pieceSide;
    final ds = jigsaw.pieceDisplazament;
    final partSide = jigsaw.partSide;
    final partSide2 = partSide * 2;
    final inBlank = side - partSide2 * 2;

    groups.clear();
    for (g in jigsaw.groups) {
      final pcs = g.pieces;
      final gWg = new Board(
        (g.dim.w - 1) * ds + side, (g.dim.h - 1) * ds + side
      );
      final sprite = new Sprite(board, gWg.canvas, true)
        .setZindex(g.zindex);
      sprite
        .addMouseDown(e -> {
            draggedGroup = g;
            final abs = Pointer.absolute(e);
            if (sprite.inBlank(abs)) {
              draggedGroup = null;
              for (jgr in jigsaw.groups) {
                final spr = groups[jgr.id];
                if (
                  Pointer.bounds(spr.canvas).contains(abs) &&
                  !spr.inBlank(abs)
                ) {
                  draggedGroup = jgr;
                  try {
                    final tev = cast(e, js.html.TouchEvent);
                    spr.canvas.dispatchEvent(new js.html.TouchEvent(
                      "touchstart",
                      Pointer.copyTouchEvent(tev)
                    ));
                  } catch (ex) {
                    spr.canvas.dispatchEvent(new js.html.MouseEvent(
                      "mousedown", Pointer.copyEvent(e)
                    ));
                  }
                  break;
                }
              }
              if (draggedGroup == null) {
                e.preventDefault();
                return;
              }
            }
            draggedGroup.setZindex(1000);
            final jgrs = jigsaw.normalizeZindex();
            for (jgr in jgrs) {
              groups[jgr.id].setZindex(jgr.zindex);
            }
          })
        .addDrop(e -> {
            if (draggedGroup == null) return;

            draggedGroup.setPos(new Point(
              Std.parseInt(sprite.canvas.style.left),
              Std.parseInt(sprite.canvas.style.top)
            ));
            final rs = jigsaw.connect(draggedGroup);

            Model.page.setValue(PICTURE(jigsaw));
            if (rs.connected) {
              for (g in groups) g.quit();
              mkGroups();
              if (Model.audio.value) Loader.audio.play();
              update();
              if (rs.finished) {
                Model.solved.add(Model.level.value, jigsaw.picture);
                Model.page.setValue(INDEX);
                haxe.Timer.delay(() -> {
                  final gr = groups[groups.keys().next()];
                  gr.moveTo(
                    Std.int(board.width / 2) -
                      Std.int(gr.canvas.width / 2),
                    Std.int(board.height / 2) -
                      Std.int(gr.canvas.height / 2),
                    600
                  );
                  haxe.Timer.delay(() -> {
                    board.copyFrom(
                      jigsaw.image,
                      Std.int(board.width / 2) -
                        Std.int(gr.canvas.width / 2),
                      Std.int(board.height / 2) -
                        Std.int(gr.canvas.height / 2)
                    );
                    gr.quit();
                    new CongratulationsBox(
                      jigsaw.picture, Cts.levels[Model.level.value]
                    ).show();
                  }, 800);
                }, 500);
              }
            } else {
              for (g in groups) g.quit();
              mkGroups();
              update();
            }
          });

      var p = new Point(0, 0);
      for (r in 0...g.dim.h) {
        for (c in 0...g.dim.w) {
          function addBlanks (blanks: Array<Rectangle>): Void {
            for (b in blanks) {
              sprite.addBlank(new Rectangle(
                b.x + c * ds, b.y + r * ds, b.w, b.h
              ));
            }
          }

          switch (pcs[r][c]) {
            case Some(pc): {
              gWg.copyFrom(
                pieces[pc.idRow][pc.idCol], p.x, p.y
              );
              final blanks = pc.blanks();
              if (r == 0 || pcs[r - 1][c] == None)
                addBlanks(blanks.up);
              if (r == g.dim.h - 1 || pcs[r + 1][c] == None)
                addBlanks(blanks.down);
              if (c == 0 || pcs[r][c - 1] == None)
                addBlanks(blanks.left);
              if (c == g.dim.w - 1 || pcs[r][c + 1] == None)
                addBlanks(blanks.right);
            }
            case None:
              sprite.addBlank(new Rectangle(
                p.x + partSide, p.y + partSide,
                ds, ds
              ));
              if (r == 0)
                addBlanks([new Rectangle(0, 0, side, partSide)]);
              if (r == g.dim.h - 1)
                addBlanks([new Rectangle(0, side - partSide, side, partSide)]);
              if (c == 0)
                addBlanks([new Rectangle(0, 0, partSide, side)]);
              if (c == g.dim.w - 1)
                addBlanks([new Rectangle(side - partSide, 0, partSide, side)]);
          }


          p = new Point(p.x + ds, p.y);
        }
        p = new Point(0, p.y + ds);
      }

      groups[g.id] = sprite;
    }
  }

}
