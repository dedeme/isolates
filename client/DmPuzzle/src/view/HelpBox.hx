// Copyright 15-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view;

using StringTools;
import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.Menu;
import dm.ModalBox;
import dm.Device;
import I18n._;
import I18n._args;

/// Help dialog.
class HelpBox {
  final wg = Q("div");
  final bodyDiv = Q("div");
  final menuDiv = Q("div");
  final box: ModalBox;

  var menuOption: String;

  public function new () {
    bodyDiv
      .klass("frame")
      .style(
          'width: ${Std.int(Cts.boardDim.w * 0.8)}px;' +
          'height: ${Std.int(Cts.boardDim.h * 0.8)}px;' +
          'overflow-x: hidden;' +
          'overflow-y: auto;' +
          'text-align: left;' +
          'padding: ${Device.isBig ? 40 : 20}px;'
        )
    ;
    box = new ModalBox(wg, false);
    menuOption = "index";

    wg
      .add(Q("table")
        .klass("main")
        .add(Q("tr")
          .add(Q("td")
            .add(menuDiv)
            .add(bodyDiv))))

    ;
  }

  function replacement (tx: String): String {
    tx = tx
      .replace("${LED}", Std.string(Std.int(
            js.Browser.window.devicePixelRatio * 6
          )))
      .replace("${APP_DATE}", Cts.appVersion.substring(0, 4))
    ;

    if (Device.isBig) {
      return tx.replace("${IMG_DIM}", "M");
    } else {
      return tx.replace("${IMG_DIM}", "");
    }
  }

  function menuSel (option: String) {
    menuOption = option;
    update();
  }

  function close () {
    box.show(false);
  }

  /// DOM ----------------------------------------------------------------------

  public function show (parent: Domo) {
    parent.removeAll().add(box.wg);
    box.show(true);
    update();
  }

  public function update () {
    final lopts = [
      Menu.toption("index", _("Index"), () -> menuSel("index")),
      Menu.separator(),
      Menu.toption("play", _("Play"), () -> menuSel("play")),
      Menu.separator(),
      Menu.toption("credits", _("Credits"), () -> menuSel("credits"))
    ];
    final ropts = [
      Menu.toption("close", _("Close"), close)
    ];
    final menu = new Menu(lopts, ropts, menuOption);
    menuDiv
      .removeAll()
      .add(menu.wg)
    ;

    final url = switch (menuOption) {
      case "play": "pg2";
      case "credits": "pg3";
      default: "pg1";
    }


    Ui.upload('help/${url}-${I18n.lang}.html', tx ->
      bodyDiv.removeAll().html(replacement(tx))
    );
  }
}

