// Copyright 15-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.ModalBox;
import I18n._;
import I18n._args;

/// Open final dialog.
class OpenBox {
  var box: ModalBox;
  var inp = Q("input");
  var marksCheckedWg = Q("span");
  var marksChecked = false;

  public function new () {
    inp
      .att("type", "file")
      .att("hidden", "true")
      .on(CHANGE, fileSelected)
    ;
    final content = mkContent();
    box = new ModalBox(content, false);
  }

  function check () {
    marksChecked = !marksChecked;
    marksCheckedWg
      .removeAll()
      .add(Ui.link(check)
        .add(Cts.mkLed(marksChecked)))
    ;
  }

  function fileSelected () {
    final files = cast(inp.e, js.html.InputElement).files;
    if (files.length == 0) {
      box.show(false);
      return;
    }
    final file = files.item(0);
    final fr = new js.html.FileReader();
    fr.onload = e ->
      if (Model.restore(fr.result, marksChecked)) {
        js.Browser.location.reload();
      } else {
        Ui.alert(_args(_("'%0' is not a valid backup"), [file.name]));
      }
    fr.onerror = e -> {
      fr.abort();
      Ui.alert(_("Fail reading file"));
    }
    fr.readAsText(files.item(0));
  }

  function cancel () {
    box.show(false);
  }

  /// DOM ----------------------------------------------------------------------

  public function show () {
    View.modalBoxDiv
      .removeAll()
      .add(box.wg)
    ;
    box.show(true);
  }

  function mkContent (): Domo {
    return Q("div")
      .add(inp)
      .add(Q("div")
        .html("&nbsp;"))
      .add(Q("div")
        .html("<big>" + _("Restore Jigsaw") + "</big>"))
      .add(Q("div")
        .html("&nbsp;"))
      .add(Q("table")
        .add(Q("tr")
          .add(Q("td")
            .style("vertical-align: top")
            .add(marksCheckedWg
              .add(Ui.link(check).add(Cts.mkLed(marksChecked)
                  ))))
          .add(Q("td")
            .text(_("Restore jicksaw marks")))))
      .add(Q("div")
        .html("&nbsp;"))
      .add(Q("table")
        .att("align", "center")
        .add(Q("tr")
          .add(Q("td")
            .add(Q("button")
              .text(_("Cancel"))
              .on(CLICK, cancel))
            .add(Q("span")
              .html("&nbsp;&nbsp;&nbsp;"))
            .add(Q("button")
              .text(_("Look@"))
              .on(CLICK, e -> inp.e.click())))))
    ;
  }
}
