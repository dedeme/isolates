// Copyright 15-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.It;
import I18n._;
import I18n._args;

class CongratulationsBox {
  final name: String;
  final level: String;

  final acceptWg = Q("button");
  var box = Q("div")
    .style(
      "display: none;" + //Hidden by default
      "position: fixed;" + //Stay in place
      "z-index: 1;" + //Sit on top
      "padding-top: 100px;" + //Location of the box
      "left: 0;" +
      "top: 0;" +
      "width: 100%;" + //Full width
      "height: 100%;" + //Full height
      "overflow: auto;" + //Enable scroll if needed
      "background-color: rgb(0,0,0);" + //Fallback color
      "background-color: rgba(0,0,0,0.4);" + //Black opacity
      "text-align: center;"
    )
  ;
  public function new (name: String, level: String) {
    this.name = name;
    this.level = level;

    box.add(mkContent());
  }

  function accept () {
    box.setStyle("display", "none");
    haxe.Timer.delay( () -> {
      js.Browser.location.reload();
    }, 200);
  }

  /// DOM ----------------------------------------------------------------------

  public function show () {
    View.modalBoxDiv
      .removeAll()
      .add(box)
    ;
    box.setStyle("display", "block");
    acceptWg.e.focus();
  }

  function mkContent (): Domo {
    return Q("table")
      .att("align", "center")
      .add(Q("tr")
        .add(Q("td")
        .klass("congratulations")
        .add(Q("div")
          .html("<big><big>" + _("Congratulations!!!") + "</big></big>"))
        .add(Q("div")
          .add(Loader.mkIcon(It.from(Loader.wins).shuffle().to()[0].i)))
        .add(Q("div")
          .html("<big>" + _args(
              _("Jigsaw<br>correctly finished"), [name, level]
            ) + "</big>"))
        .add(Q("hr"))
        .add(Q("div")
          .add(acceptWg
            .text(_("Accept"))
            .on(CLICK, accept)))))
    ;
  }
}
