// Copyright 02-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.It;
import dm.ModalBox;
import dm.Device;
import I18n._;
import model.Level;
import data.Picture;
import data.Jigsaw;

/// Index page.
class Index {
  /// Object -------------------------------------------------------------------

  final wg = Q("div");

  public function new () {}

  function setLevel (l: Int) {
    Model.level.setValue(l);
    update();
  }

  function setGroup (i: Int) {
    Model.group.setValue(i);
    Loader.load(update);
  }

  function delMarks () {
    if (Ui.confirm(_("Remove 'done marks' from jigsaws?"))) {
      final level = Model.level.value;
      final group = Model.group.value;

      dm.Store.clear(Cts.storeKey);

      Model.level.setValue(level);
      Model.group.setValue(group);
      js.Browser.location.reload();
    }
  }

  function help () {
    new HelpBox().show(View.modalBoxDiv);
  }

  function open () {
    new OpenBox().show();
  }

  function selPicture (p: Picture) {
    Model.page.setValue(PICTURE(Jigsaw.mk(p.name, Model.level.value)));
    View.update();
  }

  /// DOM ----------------------------------------------------------------------

  public function show (parent: Domo) {
    parent.removeAll().add(wg);
    update();
  }

  public function update () {
    var currentLevel = Model.level.value;
    function mkTick(l:Int): Domo{
      return Q("td")
        .style("padding-right:0px")
        .html(l == currentLevel ? "&nbsp;✓" : "")
      ;
    }
    function mkLevelWg(l: Int): Domo {
      return l == currentLevel
        ? Q("td").text(Cts.levels[l])
        : Q("td")
          .add(Ui.link(e -> setLevel(l))
            .klass("link")
            .text(Cts.levels[l]))
      ;
    }
    final sel = Q("table")
      .att("align", "center")
      .att("cellspacing", "0")
      .add(Q("tr")
        .add(Q("td"))
        .add(mkTick(0))
        .add(mkLevelWg(0)))
      .add(Q("tr")
        .add(Q("td")
          .text(_("Level") + ": "))
        .add(mkTick(1))
        .add(mkLevelWg(1)))
      .add(Q("tr")
        .add(Q("td"))
        .add(mkTick(2))
        .add(mkLevelWg(2)))
    ;

    function mkLed (i: Int): Domo {
      return i == Model.group.value
        ? Cts.mkLed(true)
        : Ui.link(e -> setGroup(i))
            .klass("link")
            .add(Cts.mkLed(false))
      ;
    }
    final leds = Device.isMobilV
      ? [ Q("tr")
            .adds(It.range(5).map(i ->
              Q("td")
                .add(mkLed(i))).to()),
          Q("tr")
            .adds(It.range(5, 10).map(i ->
              Q("td")
                .add(mkLed(i))).to())
        ]
      : [ Q("tr")
            .adds(It.range(10).map(i ->
              Q("td")
                .add(mkLed(i))).to())
        ]
    ;

    final module = Device.isMobilV ? 2 : 3;
    final trs: Array<Domo> = [];
    final picts = Loader.getPicts();
    var tr = Q("tr");
    for (i in 0...picts.length) {
      tr
        .add(Q("td")
          .add(mkPictureWg(picts[i])))
      ;
      if (i % module == module - 1) {
        trs.push(tr);
        tr = Q("tr");
      }
    }
    final rest = picts.length % module;
    if (rest > 0) {
      tr.add(Q("td"));
      if (module == 3 && rest == 1) tr.add(Q("td"));
      trs.push(tr);
    }

    wg
      .removeAll()
      .add(Q("table")
        .klass("main")
        .add(Q("tr")
          .add(Q("td"))
          .add(Q("td")
            .att("rowspan", "2")
            .style("vertical-align:middle;")
            .add(sel))
          .add(Q("td")
            .style(
                "text-align:right;width:5px;vertical-align:bottom;" +
                "white-space:nowrap;"
              )
            .add(Q("span")
              .add(Ui.link(delMarks)
                .add(Loader.mkIcon(TRASH))))
            .add(Q("span")
              .html("&nbsp;&nbsp;&nbsp;&nbsp;"))
            .add(Q("span")
              .add(Ui.link(help)
                .add(Loader.mkIcon(HELP))))
            .add(Q("span")
              .html("&nbsp;&nbsp;&nbsp;&nbsp;"))
            .add(Q("span")
              .add(Ui.link(open)
                .add(Loader.mkIcon(OPEN))))))
        .add(Q("tr")
          .add(Q("td")
            .style("width:5px;vertical-align:bottom;")
            .add(Loader.mkIcon(DMPUZZLE)))
          .add(Q("td")
            .style(
              "text-align:right;width:5px;vertical-align:bottom;"
            )
            .add(Q("table")
              .adds(leds)))))
      .add(Q("hr"))
      .add(Q("table")
        .att("align", "center")
        .adds(trs))
    ;
  }

  function mkPictureWg (p: Picture): Domo {
    final tick = Model.solved.contains(Model.level.value, p.name)
      ? "✓ "
      : ""
    ;
    return Q("table")
      .klass("frame")
      .add(Q("tr")
        .add(Q("td")
          .att("colspan", "3")
          .add(Ui.link(e -> selPicture(p))
            .add(p.getMedium()
              .klass("frame0")))))
      .add(Q("tr")
        .add(Q("td")
          .style("text-align: left;width:40%;")
          .text(tick + p.name))
        .add(Q("td")
          .style("text-align: right;width:40%;")
          .add(Q("a")
            .att("href", p.link)
            .att("title", p.author)
            .text("web"))))
    ;
  }

}
