// Copyright 02-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package model;

import dm.Store;
import dm.Js;
import Cts;
import data.Jigsaw;

enum PageType { INDEX; PICTURE (jigsaw: Jigsaw); }


/// Application lang.
class Page {
  static final storeKey = Cts.storeKey + "_page";

  static function mkType (t: String): PageType {
    return switch (t) {
      case "INDEX": INDEX;
      default: PICTURE(Jigsaw.fromJs(Js.from(t)));
    }
  }

  /// Constructor from Store.
  public static function mk (): Page {
    return new Page(
      Store.oget(storeKey, v -> mkType(v), INDEX)
    );
  }

  // Object --------------------------------------------------------------------

  /// Current page.
  public var value(default, null): PageType;

  function new (type: PageType): Void {
    setValue(type);
  }

  /// Sets current page.
  public function setValue(type: PageType): Void {
    Store.put(storeKey, switch(type) {
      case INDEX: "INDEX";
      case PICTURE(j): j.toJs().to();
    });
    value = type;
  }

}
