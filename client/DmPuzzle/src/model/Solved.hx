// Copyright 02-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package model;

import dm.Js;
import dm.Store;
import dm.It;
import Cts;

/// Solved pictures.
class Solved {
  static final storeKey = Cts.storeKey + "_solved";

  /// Constructor from Store.
  public static function mk (): Solved {
    return new Solved(
      Store.oget(storeKey, v -> Js.from(v), Js.wa([]))
    );
  }

  // Object --------------------------------------------------------------------

  var values(default, null): Array<{level: Int, pict: String}>;

  /// Constructor from a serialization of 'this'.
  public function new (js: Js): Void {
    values = js.rArray(e -> {
      final a = e.ra();
      return {level: a[0].ri(), pict: a[1].rs()};
    });
  }

  public function toJs (): Js {
    return Js.wArray(values, e ->
      Js.wa([
        Js.wi(e.level),
        Js.ws(e.pict)
      ])
    );
  }

  /// Marks a new picture as done and purges pictures that are valid no more.
  ///   level: Level of puzzle.
  ///   pict : Picture to mark.
  public function add(level: Int, pict: String): Void {
    final picts = Loader.getAllPicts();

    if (!contains(level, pict)) {
      values.push({level: level, pict: pict});
    }
    values = values.filter(v -> {
      It.from(picts).some(e -> e.name == v.pict);
    });

    Store.put(storeKey, toJs().to());
  }

  /// Adds pictures from 'other' if they are not in 'this'
  public function addFrom (other: Solved): Void {
    for (e in other.values) {
      if (
        !It.from(values).some(v ->
          v.level == e.level && v.pict == e.pict
        )
      ) {
        add(e.level, e.pict);
      }
    }
  }

  /// Returns 'true' if 'pict' of 'level' has been solved.
  ///   level: Level of puzzle.
  ///   pict : Picture to test.
  public function contains (level: Int, pict: String): Bool {
    for (v in values)
      if (v.level == level && v.pict == pict) return true;
    return false;
  }

}
