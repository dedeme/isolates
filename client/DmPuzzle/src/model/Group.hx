// Copyright 16-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package model;

import dm.Store;
import Cts;

/// Selected picture group.
class Group {
  static final storeKey = Cts.storeKey + "_group";

  /// Constructor from Store.
  public static function mk (): Group {
    return new Group(
      Store.oget(storeKey, v -> Std.parseInt(v), 0)
    );
  }

  // Object --------------------------------------------------------------------

  /// Current group.
  public var value(default, null): Int;

  function new (group: Int): Void {
    setValue(group);
  }

  /// Sets current group.
  public function setValue(group: Int): Void {
    Store.put(storeKey, Std.string(group));
    value = group;
  }
}
