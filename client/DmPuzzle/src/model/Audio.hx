// Copyright 02-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package model;

import dm.Store;
import Cts;

/// Audio on-off.
class Audio {
  static final storeKey = Cts.storeKey + "_audio";

  /// Constructor from Store.
  public static function mk (): Audio {
    return new Audio(
      Store.oget(storeKey, v -> v == "1" ? true : false, true)
    );
  }

  // Object --------------------------------------------------------------------

  /// Current state.
  public var value(default, null): Bool;

  function new (value: Bool): Void {
    setValue(value);
  }

  /// Sets current state.
  public function setValue(value: Bool): Void {
    Store.put(storeKey, value ? "1": "0");
    this.value = value;
  }
}
