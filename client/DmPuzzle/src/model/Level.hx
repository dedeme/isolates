// Copyright 02-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package model;

import dm.Store;
import Cts;

/// Selected level.
class Level {
  static final storeKey = Cts.storeKey + "_level";

  /// Constructor from Store.
  public static function mk (): Level {
    return new Level(
      Store.oget(storeKey, v -> Std.parseInt(v), 0)
    );
  }

  // Object --------------------------------------------------------------------

  /// Current level.
  public var value(default, null): Int;

  function new (level: Int): Void {
    setValue(level);
  }

  /// Sets current level.
  public function setValue(level: Int): Void {
    Store.put(storeKey, Std.string(level));
    value = level;
  }
}
