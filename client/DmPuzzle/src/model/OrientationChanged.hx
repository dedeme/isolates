// Copyright 02-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package model;

import dm.Store;
import Cts;

/// Orientation changed.
class OrientationChanged {
  static final storeKey = Cts.storeKey + "_orientationChanged";

  /// Constructor from Store.
  public static function mk (): OrientationChanged {
    return new OrientationChanged(
      switch (Store.get(storeKey)) {
        case Some(v): v == "1" ? true : false;
        case None: false;
      }
    );
  }

  // Object --------------------------------------------------------------------

  /// Current level.
  public var value(default, null): Bool;

  function new (value: Bool): Void {
    setValue(value);
  }

  /// Sets current value.
  public function setValue(value: Bool): Void {
    Store.put(storeKey, value ? "1" : "0");
    this.value = value;
  }
}
