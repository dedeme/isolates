// Copyright 04-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package data;

import dm.Opt;
import dm.Js;
import dm.Sprite;
import dm.Point;
import dm.Board;
import dm.Dimension;
import dm.Rectangle;
import dm.Device;

/// Jigsaw piece.
class Piece {

  public static function fromJs (js: Js): Piece {
    final a = js.ra();
    return new Piece(
      a[0].ri(),
      a[1].ri(),
      a[2].ri(),
      a[3].ri(),
      a[4].ri(),
      a[5].ri(),
      Point.fromJs(a[6]),
      a[7].ri(),
      a[8].ri(),
      a[9].ri(),
      a[10].ri()
    );
  }

  // Object --------------------------------------------------------------------

  public var group(default, null): Int;
  public final idRow: Int;
  public final idCol: Int;
  public var row(default, null): Int;
  public var col(default, null): Int;
  public final partSide: Int;
  public var pos(default, null): Point;
  public final up: Int;
  public final right: Int;
  public final down: Int;
  public final left: Int;

  /// Constructor.
  ///   group   : Group of piece identifier.
  ///   row     : Number of row in group.
  ///   col     : Number of column in group.
  ///   partSide: Size of internal part of piece.
  ///   pos     : Position in board.
  ///   up      : Up side form (1: out, -1: in, 0: plain).
  ///   right   : Right side form (1: out, -1: in, 0: plain).
  ///   down    : Down side form (1: out, -1: in, 0: plain).
  ///   left    : Left side form (1: out, -1: in, 0: plain).
  public function new (
    group: Int, idRow: Int, idCol: Int,
    row: Int, col: Int, partSide: Int, pos: Point,
    up: Int, right: Int, down: Int, left: Int
  ) {
    this.group = group;
    this.idRow = idRow;
    this.idCol = idCol;
    this.row = row;
    this.col = col;
    this.partSide = partSide;
    this.pos = pos;
    this.up = up;
    this.right = right;
    this.down = down;
    this.left = left;
  }

  public function setGroup (groupId: Int): Void {
    group = groupId;
  }

  public function setRow (row: Int): Void {
    this.row = row;
  }

  public function setCol (col: Int): Void {
    this.col = col;
  }

  public function setPos (pos: Point): Void {
    this.pos = pos;
  }

  /// Width and heigth of 'this'
  public function side (): Int {
    return partSide * Cts.partsN;
  }

  /// Width and height displacement of 'this' in board.
  public function displacement (): Int {
    return partSide * (Cts.partsN - 2);
  }

  /// Returns the displacement from 'other' to fit in 'this' when the
  /// former is over the latter. (e.g. this -> other: return LEFT).
  /// If there is not overlay, the method return None.
  public function overlay (other: Piece): Option<Cts.Direction> {
    final dx = other.pos.x - pos.x;
    final dy = other.pos.y - pos.y;
    final adx = Math.abs(dx);
    final ady = Math.abs(dy);
    final dif = Device.isBig ? partSide * 2 : partSide;
    final side = this.displacement();

    return adx < dif
      ? ady > side - dif && ady < side + dif
        ? dy < 0
          ? idRow == other.idRow + 1 && idCol == other.idCol
            ? Some(DOWN)
            : None
          : idRow == other.idRow - 1 && idCol == other.idCol
            ? Some(UP)
            : None
        : None
      : ady < dif
        ? adx > side - dif && adx < side + dif
          ? dx < 0
            ? idRow == other.idRow && idCol == other.idCol + 1
              ? Some(RIGHT)
              : None
            : idRow == other.idRow && idCol == other.idCol - 1
              ? Some(LEFT)
              : None
          : None
        : None
    ;
  }

  /// Returns trasnparent areas.
  public function blanks (): {
    up: Array<Rectangle>,
    right: Array<Rectangle>,
    down: Array<Rectangle>,
    left: Array<Rectangle>,
  } {
    final centerSide = partSide * (Cts.partsN - 6);
    final rup: Array<Rectangle> = [];
    final rright: Array<Rectangle> = [];
    final rdown: Array<Rectangle> = [];
    final rleft: Array<Rectangle> = [];

    // UP
    rup.push(new Rectangle(0, 0, partSide * 3, partSide));
    rup.push(new Rectangle(
      partSide * (Cts.partsN - 3), 0, partSide * 3, partSide
    ));
    if (up == 0) {
      rup.push(new Rectangle(partSide * 3, 0, centerSide, partSide));
    } else if (up == -1) {
      rup.push(new Rectangle(partSide * 3, 0, centerSide, partSide * 2));
    }

    // DOWN
    rdown.push(new Rectangle(
      0, partSide * (Cts.partsN - 1), partSide * 3, partSide
    ));
    rdown.push(new Rectangle(
      partSide * (Cts.partsN - 3), partSide * (Cts.partsN - 1),
      partSide * 3, partSide
    ));
    if (down == 0) {
      rdown.push(new Rectangle(
        partSide * 3, partSide * (Cts.partsN - 1), centerSide, partSide)
      );
    } else if (down == -1) {
      rdown.push(new Rectangle(
        partSide * 3, partSide * (Cts.partsN - 2), centerSide, partSide * 2
      ));
    }

    // LEFT
    rleft.push(new Rectangle(0, 0, partSide, partSide * 3));
    rleft.push(new Rectangle(
      0, partSide * (Cts.partsN - 3), partSide, partSide * 3
    ));
    if (left == 0) {
      rleft.push(new Rectangle(0, partSide * 3, partSide, centerSide));
    } else if (left == -1) {
      rleft.push(new Rectangle(0, partSide * 3, partSide * 2, centerSide));
    }

    // RIGHT
    rright.push(new Rectangle(
      partSide * (Cts.partsN - 1), 0, partSide, partSide * 3
    ));
    rright.push(new Rectangle(
      partSide * (Cts.partsN - 1), partSide * (Cts.partsN - 3),
      partSide, partSide * 3
    ));
    if (right == 0) {
      rright.push(new Rectangle(
        partSide * (Cts.partsN - 1), partSide * 3, partSide, centerSide
      ));
    } else if (right == -1) {
      rright.push(new Rectangle(
        partSide * (Cts.partsN - 2), partSide * 3, partSide * 2, centerSide
      ));
    }

    return {up: rup, right: rright, down: rdown, left: rleft};
  }

  public function toJs (): Js {
    return Js.wa([
      Js.wi(group),
      Js.wi(idRow),
      Js.wi(idCol),
      Js.wi(row),
      Js.wi(col),
      Js.wi(partSide),
      pos.toJs(),
      Js.wi(up),
      Js.wi(right),
      Js.wi(down),
      Js.wi(left)
    ]);
  }

}

