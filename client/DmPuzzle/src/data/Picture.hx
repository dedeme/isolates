// Copyright 02-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package data;

import dm.Domo;
import dm.Ui.Q;
import dm.Board;
import dm.Dimension;
import dm.Sprite;
import dm.Device;
import js.html.ImageElement;

/// Picture for cut.
class Picture {
  var img(default, null): ImageElement = null;

  public var name: String;
  public var file: String;
  public var author: String;
  public var link: String;

  public function new (
    name: String, file: String, author: String, link: String
  ) {
    this.name = name;
    this.file = file;
    this.author = author;
    this.link = link;
  }

  public function setImg (img: Domo): Void {
    this.img = cast(img.e, ImageElement);
  }

  public function getBig (dim: Dimension, partSide: Int): Board {
    final w = dim.w * (Cts.partsN - 2) * partSide;
    final h = dim.h * (Cts.partsN - 2) * partSide;
    final bimg = img == null
      ? new Board(w, h).coverFrom(Cts.defaultPicture)
      : new Board(w, h).coverImage(img)
    ;
    return new Board(w + partSide * 2, h + partSide * 2)
      .copyFrom(bimg, partSide, partSide)
    ;
  }

  public function getMedium (): Domo {
    final div = Device.isMobilV
      ? 2.5
      : Device.isMobilH
        ? 5
        : 4
    ;
    final w = Std.int(Device.screen.w / div);
    final h = Std.int(w * 3 / 4);
    final bimg = img == null
      ? new Board(w, h).coverFrom(Cts.defaultPicture)
      : new Board(w, h).coverImage(img)
    ;
    return Q(bimg.canvas);
  }

  public function getSmall (): Board {
    var w = Std.int(Device.screen.w / 8);
    var h = Std.int(w * 6 / 8);
    if (Device.isMobilV) {
      w *= 2;
      h *= 2;
    }
    final bimg = img == null
      ? new Board(w, h).coverFrom(Cts.defaultPicture)
      : new Board(w, h).coverImage(img)
    ;
    final ctx = bimg.canvas.getContext2d();
    ctx.strokeStyle = "#000000";
    ctx.strokeRect(0, 0, w, h);
    return bimg;
  }

}
