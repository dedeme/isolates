// Copyright 04-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package data;

import dm.Opt;
import dm.Js;
import dm.It;
import dm.Rnd;
import dm.Point;
import dm.Dimension;
import dm.Board;
import data.Piece;
import data.Group;

/// Current jigsaw.
class Jigsaw {

  static function dimFromLevel (level: Int): Dimension {
    return level == 0
      ? Cts.easyDim
      : level == 1
        ? Cts.mediumDim
        : Cts.expertDim
    ;
  }

  /// Constructor
  ///   picture: Name of image to split.
  ///   level  : 0: Easy, 1: Medium, 2: Expert.
  public static function mk (picture: String, level: Int): Jigsaw {
    final dim = dimFromLevel(level);
    final partSide = Std.int(Cts.jigsawVal / (dim.w * (Cts.partsN - 2) + 2));

    final pieces: Array<Array<Piece>> = [];
    final groups: Array<Group> = [];
    final dis = partSide * (Cts.partsN - 2);
    var groupId = 0;
    for (row in 0...dim.h) {
      final a: Array<Piece> = [];
      for (col in 0...dim.w) {
        final gpos = new Point(col * dis, row * dis);
        final pc =new Piece(
          groupId, row, col, 0, 0, partSide, gpos,
          row == 0 ? 0 : pieces[row - 1][col].down * -1,
          col == dim.w - 1 ? 0 : Rnd.i(2) == 0 ? -1 : 1,
          row == dim.h - 1 ? 0 : Rnd.i(2) == 0 ? -1 : 1,
          col == 0 ? 0 : a[col - 1].right * -1
        );
        final group = new Group(
          groupId, gpos, 100 + groupId, partSide, [[None]]
        );
        group.add(pc, 0, 0);
        ++groupId;
        groups.push(group);
        a.push(pc);
      }
      pieces.push(a);
    }

    return new Jigsaw(picture, level, false, groupId, groups);
  }

  public static function fromJs (js: Js): Jigsaw {
    final a = js.ra();
    return new Jigsaw(
      a[0].rs(),
      a[1].ri(),
      a[2].rb(),
      a[3].ri(),
      a[4].ra().map(e -> Group.fromJs(e))
    );
  }

  // Object --------------------------------------------------------------------

  public final dim: Dimension;
  public var partSide(default, null): Int;
  public var picture(default, null): String;
  public var level(default, null): Int;
  public var started(default, null): Bool;
  public var nextGroupId: Int;
  var imageV: Board = null;
  public var image(get, never): Board;
  function get_image (): Board {
    if (imageV == null) {
      final picts = Loader.getPicts();
      final pict = Opt.oget(
        It.from(picts).find(e -> e.name == picture), picts[0]
      );
      imageV = pict.getBig(dimFromLevel(level), partSide);
    }
    return imageV;
  }
  var smallImageV: Board = null;
  public var smallImage(get, never): Board;
  function get_smallImage (): Board {
    if (smallImageV == null) {
      final picts = Loader.getPicts();
      final pict = Opt.oget(
        It.from(picts).find(e -> e.name == picture), picts[0]
      );
      smallImageV = pict.getSmall();
    }
    return smallImageV;
  }
  public final pieces: Array<Piece> = [];
  public var pieceSide(get, never): Int;
  function get_pieceSide () { return partSide * Cts.partsN; }
  public var pieceDisplazament(get, never): Int;
  function get_pieceDisplazament () { return partSide * (Cts.partsN - 2); }
  public var groups(default, null): Array<Group>;

  function new (
    picture: String, level: Int, started: Bool, nextGroupId: Int,
    groups: Array<Group>
  ) {
    this.picture = picture;
    this.level = level;
    this.started = started;
    this.nextGroupId = nextGroupId;
    this.groups = groups;
    for (g in groups) {
      for (row in g.pieces) {
        for (op in row) {
          switch (op) {
            case Some(p): pieces.push(p);
            case None:
          }
        }
      }
    };

    dim = dimFromLevel(level);
    partSide = Std.int(Cts.jigsawVal / (dim.w * (Cts.partsN - 2) + 2));
  }

  /// Returns the piece with 'idRow' and 'idCol' or throw an execption if
  /// such piece does not exist.
  public function getPiece(idRow: Int, idCol: Int): Piece {
    switch (It.from(pieces).find(e -> e.idRow == idRow && e.idCol == idCol)) {
      case Some(p): return p;
      case None: throw new haxe.Exception('Piece ($idRow, $idCol) not found');
    }
  }

  /// Noramalize zindex values y returns a copy of this.groups sorted by
  /// zindex in reverse order.
  public function normalizeZindex (): Array<Group> {
    final grs = groups.copy();
    grs.sort((g1, g2) -> g1.zindex > g2.zindex ? 1 : -1);
    var z = 100;
    for (g in grs) g.setZindex(z++);
    grs.reverse();
    return grs;
  }

  /// Prepares 'this' to play.
  public function start () {
    started = true;
  }

  /// If group is connected, redefine groups. Y the result is the total
  /// group, returns true.
  public function connect (g: Group): {connected: Bool, finished: Bool} {
    var connected = false;
    var finished = false;

    for (grow in 0...g.dim.h) {
      for (gcol in 0...g.dim.w) {
        switch (g.pieces[grow][gcol]) {
          case Some(gpiece):
            for (basePiece in pieces) {
              if (basePiece.group == g.id) continue;

              switch (gpiece.overlay(basePiece)) {
                case Some(dir):
                  final baseGroup = Opt.eget(It.from(groups).find(e ->
                    e.id == basePiece.group
                  ));

                  var startR = -gpiece.row;
                  var baseStartR = -basePiece.row;
                  if (dir == UP) --startR;
                  if (dir == DOWN) ++startR;
                  var dsR = startR - baseStartR;
                  var baseDsR = 0;
                  var pointY = baseGroup.pos.y;
                  if (dsR < 0) {
                    baseDsR = -dsR;
                    dsR = 0;
                    pointY -= baseDsR * pieceDisplazament;
                  }
                  final nrows = dsR + g.dim.h;
                  final baseNrows = baseDsR + baseGroup.dim.h;
                  final maxRows = nrows > baseNrows ? nrows : baseNrows;

                  var startC = -gpiece.col;
                  var baseStartC = -basePiece.col;
                  if (dir == LEFT) --startC;
                  if (dir == RIGHT) ++startC;
                  var dsC = startC - baseStartC;
                  var baseDsC = 0;
                  var pointX = baseGroup.pos.x;
                  if (dsC < 0) {
                    baseDsC = -dsC;
                    dsC = 0;
                    pointX -= baseDsC * pieceDisplazament;
                  }

                  final ncols = dsC + g.dim.w;
                  final baseNcols = baseDsC + baseGroup.dim.w;
                  final maxCols = ncols > baseNcols ? ncols : baseNcols;

                  final newPieces: Array<Array<Option<Piece>>> = [];
                  for (r in 0...maxRows) {
                    final a = [];
                    for (c in 0...maxCols) {
                      a.push(None);
                    }
                    newPieces.push(a);
                  }
                  final newGroup = new Group(
                    nextGroupId++, new Point(pointX, pointY), 1000,
                    g.partSide, newPieces
                  );

                  newGroup.addGroup(g, dsR, dsC);
                  newGroup.addGroup(baseGroup, baseDsR, baseDsC);

                  groups = groups.filter(e ->
                    e.id != g.id && e.id != baseGroup.id
                  );

                  groups.push(newGroup);
                  groups = normalizeZindex();

                  connected = true;
                  if (newGroup.dim.eq(dim)) {
                    finished = true;
                    for (row in newGroup.pieces)
                      for (op in row)
                        if (op == None) finished = false;
                  }

                  return {connected: connected, finished: finished};
                case None:
              };

            }
          case None:
        }
      }
    }

    return {connected: connected, finished: finished};
  }

  public function toJs (): Js {
    return Js.wa([
      Js.ws(picture),
      Js.wi(level),
      Js.wb(started),
      Js.wi(nextGroupId),
      Js.wa(groups.map(e -> e.toJs()))
    ]);
  }

}

