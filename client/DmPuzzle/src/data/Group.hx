// Copyright 04-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package data;

import dm.Opt;
import dm.It;
import dm.Js;
import dm.Sprite;
import dm.Point;
import dm.Board;
import dm.Dimension;

/// Jigsaw pieces group.
class Group {

  public static function fromJs (js: Js): Group {
    final a = js.ra();
    final pieces = a[4].ra().map(jsrow ->
      jsrow.ra().map(jsp -> {
        if (jsp.isNull()) return None;
        return Some(Piece.fromJs(jsp));
      })
    );

    return new Group(
      a[0].ri(),
      Point.fromJs(a[1]),
      a[2].ri(),
      a[3].ri(),
      pieces
    );
  }

  // Object --------------------------------------------------------------------

  public final id: Int;
  public var pos(default, null): Point;
  public var zindex(default, null): Int;
  public final partSide: Int;
  public final pieces: Array<Array<Option<Piece>>>;
  public final dim: Dimension;
  public final pieceDisplacement: Int;

  /// Constructor.
  ///   id    : Group identifier.
  ///   pos   : Pixel position in board.
  ///   zindex: Order in z axis.
  ///   partSide: Size of internal part of piece.
  ///   pieces: Pieces of group.
  public function new (
    id: Int, pos: Point, zindex: Int, partSide: Int,
    pieces: Array<Array<Option<Piece>>>
  ) {
    this.id = id;
    this.pos = pos;
    this.zindex = zindex;
    this.partSide = partSide;
    this.pieces = pieces;

    pieceDisplacement = partSide * (Cts.partsN - 2);
    dim = new Dimension(pieces[0].length, pieces.length);
  }

  public function setPos (pos: Point) {
    this.pos = pos;
    final x = pos.x;
    for (r in 0...dim.h) {
      for (c in 0...dim.w) {
        switch (pieces[r][c]) {
          case Some(p): p.setPos(pos);
          case None:
        }
        pos = new Point(pos.x + pieceDisplacement, pos.y);
      }
      pos = new Point(x, pos.y + pieceDisplacement);
    }
  }

  /// Zindex set the orden in z index of group.
  public function setZindex (ix: Int): Group {
    zindex = ix;
    return this;
  }

  public function overlay (other: Group): Option<Group> {
    return None;
  }

  /// Adds a piece to (row - col) position.
  public function add (piece: Piece, row: Int, col: Int) {
    if (piece.group != id)
      throw new haxe.Exception('Bad piece.group ${piece.group} in group ${id}');

    piece.setPos(new Point(
      pos.x + col * piece.displacement(),
      pos.y + row * piece.displacement()
    ));
    piece.setRow(row);
    piece.setCol(col);
    if (row >= dim.h || col >= dim.w)
      throw new haxe.Exception('($row, $col) out of range (${dim.h}, ${dim.w}');
    pieces[row][col] = Some(piece);
  }

  /// Adds pieces of 'g' to 'this'
  ///   g: Group of less dimension (width and height) than 'this'.
  ///   drow: Displacement of origin row of 'g'.
  ///   dcol: Displacement of origin column of 'g'.
  public function addGroup (g: Group, drow: Int, dcol: Int) {
    for (r in 0...g.dim.h) {
      for (c in 0...g.dim.w) {
        switch (g.pieces[r][c]) {
          case Some (piece):
            piece.setGroup(id);
            add(piece, r + drow, c + dcol);
          case None:
        }
      }
    }
  }

  public function toJs (): Js {
    return Js.wa([
      Js.wi(id),
      pos.toJs(),
      Js.wi(zindex),
      Js.wi(partSide),
      Js.wa(pieces.map(row ->
        Js.wa(row.map(op ->
          switch(op) {
            case Some(p): p.toJs();
            case None: Js.wn();
          }
        ))
      ))
    ]);
  }

}

