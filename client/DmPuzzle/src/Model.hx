// Copyright 02-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

using StringTools;
import dm.Js;
import dm.It;
import dm.B64;
import dm.Opt;
import dm.Device;
import model.OrientationChanged;
import model.Page;
import model.Level;
import model.Audio;
import model.Group;
import model.Solved;
import data.Jigsaw;

/// Application model
class Model {
  public static var orientationChanged(default, null): OrientationChanged;
  public static var page(default, null): Page;
  public static var level(default, null): Level;
  public static var audio(default, null): Audio;
  public static var group(default, null): Group;
  public static var solved(default, null): Solved;

  /// Model initialization.
  public static function init () {
    orientationChanged = OrientationChanged.mk();
    page = Page.mk();
    level = Level.mk();
    audio = Audio.mk();
    group = Group.mk();
    solved = Solved.mk();

    if (js.Browser.navigator.language.startsWith("es")) I18n.es();
    else I18n.en();
  }

  /// Serialize Model in B64.
  public static function serialize (): String {
    final js = Js.wa([
      Js.ws("202012"),                  // 0 (data version)
      switch (page.value) {             // 1
        case INDEX: Js.wn();
        case PICTURE(j): j.toJs();
      },
      Js.wi(level.value),               // 2
      Js.wi(group.value),               // 3
      Js.ws(                            // 4
        Device.isMobilH ? "H" : Device.isMobilV ? "V": ""
      ),
      solved.toJs()                     // 5
    ]);
    return B64.encode(js.to());
  }

  /// Restore Model. If the restoration is not posible, returns 'false'
  ///   s: Seriealized model.
  ///   withMarks: 'true' if jigsaw marks have to be restored.
  public static function restore (s: String, withMarks: Bool): Bool {
    try {
      final a = Js.from(s).ra();
      final pg = a[1].isNull()
        ? INDEX
        : PICTURE(Jigsaw.fromJs(a[1]));
      final lv = a[2].ri();
      final gr = a[3].ri();
      final mv = a[4].rs();
      final sv = new Solved(a[5]);

      page.setValue(pg);
      level.setValue(lv);
      group.setValue(gr);
      if (
        (mv == "H" && !Device.isMobilH) ||
        (mv == "V" && !Device.isMobilV) ||
        (mv == "" && Device.isMobil)
      ) {
        Model.orientationChanged.setValue(true);
      }
      if (withMarks) solved.addFrom(sv);

      return true;
    } catch (e) {
      return false;
    }
  }

}
