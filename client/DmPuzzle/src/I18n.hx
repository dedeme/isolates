// Generate by hxi18n. Don't modify

/// I18n management.
class I18n {

  static var enDic = [
    "'%0' is not a valid backup" => "'%0' is not a valid backup",
    "Accept" => "Accept",
    "Alarm Clock" => "Alarm Clock",
    "Alien" => "Alien",
    "Archs" => "Archs",
    "Back" => "Back",
    "Bats" => "Bats",
    "Boat" => "Boat",
    "Cancel" => "Cancel",
    "Car" => "Car",
    "Cards" => "Cards",
    "Cat" => "Cat",
    "Cauldron" => "Cauldron",
    "Close" => "Close",
    "Congratulations!!!" => "Congratulations!!!",
    "Corridor" => "Corridor",
    "Couple" => "Couple",
    "Credits" => "Credits",
    "Desert" => "Desert",
    "Dices" => "Dices",
    "Dragon" => "Dragon",
    "Duplicate" => "Duplicate",
    "Easy" => "Easy",
    "Evolution" => "Evolution",
    "Expert" => "Expert",
    "Fail reading file" => "Fail reading file",
    "Field" => "Field",
    "Flowers" => "Flowers",
    "Fractal" => "Fractal",
    "Girl" => "Girl",
    "Hexagonal" => "Hexagonal",
    "Hourglass" => "Hourglass",
    "Index" => "Index",
    "Internet" => "Internet",
    "Jigsaw<br>correctly finished" => "Jigsaw<br>correctly finished",
    "Journey" => "Journey",
    "Keys" => "Keys",
    "Lake" => "Lake",
    "Level" => "Level",
    "Literature" => "Literature",
    "Look" => "Look",
    "Look@" => "Look",
    "Lottery" => "Lottery",
    "Market" => "Market",
    "Medium" => "Medium",
    "Monumental" => "Monumental",
    "Mushrooms" => "Mushrooms",
    "Office" => "Office",
    "Owl" => "Owl",
    "Pen" => "Pen",
    "Play" => "Play",
    "Pocket" => "Pocket",
    "Railway" => "Railway",
    "Ravens" => "Ravens",
    "Reflections" => "Reflections",
    "Remove 'done marks' from jigsaws?" => "Remove 'done marks' from jigsaws?",
    "Restore Jigsaw" => "Restore Jigsaw",
    "Restore jicksaw marks" => "Restore jicksaw marks",
    "Ring" => "Ring",
    "River" => "River",
    "Robots" => "Robots",
    "Rose" => "Rose",
    "Roulette" => "Roulette",
    "Save" => "Save",
    "School" => "School",
    "Security" => "Security",
    "Slots" => "Slots",
    "Spaceship" => "Spaceship",
    "Spider" => "Spider",
    "Streetlight" => "Streetlights",
    "Surgery" => "Surgery",
    "Tied" => "Tied",
    "Typewriter" => "Typewriter",
    "Watch" => "Watch",
    "Wolf" => "Wolf",
    "Zap" => "Zap"
  ];

  static var esDic = [
    "'%0' is not a valid backup" => "'%0' no es una copia válida",
    "Accept" => "Aceptar",
    "Alarm Clock" => "Despertador",
    "Alien" => "Extraterrestre",
    "Archs" => "Arcos",
    "Back" => "Atrás",
    "Bats" => "Murciélagos",
    "Boat" => "Barca",
    "Cancel" => "Cancelar",
    "Car" => "Coche",
    "Cards" => "Cartas",
    "Cat" => "Gato",
    "Cauldron" => "Caldero",
    "Close" => "Cerrar",
    "Congratulations!!!" => "¡¡¡Felicidades!!!",
    "Corridor" => "Corredor",
    "Couple" => "Pareja",
    "Credits" => "Créditos",
    "Desert" => "Desierto",
    "Dices" => "Dados",
    "Dragon" => "Dragón",
    "Duplicate" => "Duplicados",
    "Easy" => "Fácil",
    "Evolution" => "Evolución",
    "Expert" => "Experto",
    "Fail reading file" => "Fallo leyendo el archivo",
    "Field" => "Campo",
    "Flowers" => "Flores",
    "Fractal" => "Fractal",
    "Girl" => "Jóven",
    "Hexagonal" => "Hexagonal",
    "Hourglass" => "De arena",
    "Index" => "Índice",
    "Internet" => "Internet",
    "Jigsaw<br>correctly finished" => "Puzle<br>terminado correctamente",
    "Journey" => "Viaje",
    "Keys" => "Llaves",
    "Lake" => "Lago",
    "Level" => "Nivel",
    "Literature" => "Literatura",
    "Look" => "Mirada",
    "Look@" => "Buscar",
    "Lottery" => "Lotería",
    "Market" => "Bolsa",
    "Medium" => "Medio",
    "Monumental" => "Monumental",
    "Mushrooms" => "Hongos",
    "Office" => "Oficina",
    "Owl" => "Buho",
    "Pen" => "Lápiz",
    "Play" => "Juego",
    "Pocket" => "De bolsillo",
    "Railway" => "Ferrocarril",
    "Ravens" => "Cuervos",
    "Reflections" => "Reflejos",
    "Remove 'done marks' from jigsaws?" => "¿Eliminar las marcas de \"hecho\"\nen los puzles?",
    "Restore Jigsaw" => "Restaurar Puzle",
    "Restore jicksaw marks" => "Restaurar las marcas de los puzles",
    "Ring" => "Anillo",
    "River" => "Rio",
    "Robots" => "Robots",
    "Rose" => "Rosa",
    "Roulette" => "Ruleta",
    "Save" => "Guardar",
    "School" => "Escuela",
    "Security" => "Seguridad",
    "Slots" => "Tragaperras",
    "Spaceship" => "Nave espacial",
    "Spider" => "Araña",
    "Streetlight" => "Farolas",
    "Surgery" => "Cirujía",
    "Tied" => "Atada",
    "Typewriter" => "Mecanografía",
    "Watch" => "De pulsera",
    "Wolf" => "Lobo",
    "Zap" => "Zap"
  ];

  public static var lang(default, null) = "es";

  public static function en (): Void {
    lang = "en";
  }

  public static function es (): Void {
    lang = "es";
  }

  public static function _(key: String): String {
    final dic = lang == "en" ? enDic : esDic;
    return dic.exists(key) ? dic[key] : key;
  }

  public static function _args(key: String, args: Array<String>): String {
    var bf = "";
    final v = _(key);
    var isCode = false;
    for (i in 0...v.length) {
      final ch = v.charAt(i);
      if (isCode) {
        if (ch >= "0" && ch <= "9") bf += args[Std.parseInt(ch)];
        else bf += "%" + ch;
        isCode = false;
      } else if (ch == "%") {
        isCode = true;
      } else {
        bf += ch;
      }
    }
    return bf;
  }

}
