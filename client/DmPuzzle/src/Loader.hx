// Copyright 02-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

import dm.Domo;
import dm.Ui;
import dm.Device;
import dm.Audio;
import data.Picture;
import I18n._;

enum Icon {
  AUDIO_OFF; AUDIO_ON;
  BLANK; DMPUZZLE;
  HELP; LEFT; OK;
  OPEN; RUN; SAVE;
  TRASH;

  WIN0; WIN1; WIN2;
  WIN3; WIN4; WIN5; WIN6;
}

/// Files loader.
class Loader {
  static var pictsGroup = 0;

  static final picts: Array<Picture> = [

    // LANDSCAPES
    new Picture(
      _("Lake"), "lake", "Susanne Jutzeler",
      "https://pixabay.com/photos/tr%C3%BCebsee-titlis-switzerland-5337646/"
    ),
    new Picture(
      "Himalaya", "himalaya", "David Mark",
      "https://pixabay.com/photos/ama-dablam-himalaya-mountain-peak-2064522/"
    ),
    new Picture(
      _("Boat"), "boat", "Quang Le",
      "https://pixabay.com/photos/sunrise-boat-rowing-boat-nobody-1014712/"
    ),
    new Picture(
      _("River"), "river", "Ian Turnell",
      "https://www.pexels.com/photo/" +
        "body-of-water-between-green-leaf-trees-709552/"
    ),
    new Picture(
      _("Field"), "field", "Jorg Peter",
      "https://pixabay.com/photos/landscape-autumn-twilight-mountains-615428/"
    ),
    new Picture(
      _("Desert"), "desert", "Jorg Peter",
      "https://pixabay.com/photos/desert-morocco-sand-dune-dry-1270345/"
    ),

    // VINTAGE
    new Picture(
      _("Flowers"), "flowers", "Cooper Hewitt",
      "https://www.si.edu/object/drawing:chndm_1975-73-1-2?"+
        "page=1&edan_q=draws&edan_fq%5B0%5D=media_usage:" +
        "CC0&oa=1&destination=/search/collection-images&" +
        "searchResults=1&id=chndm_1975-73-1-2"
    ),
    new Picture(
      _("Railway"), "railway", "DarkmoonArt_de",
      "https://pixabay.com/photos/locomotive-clock-steampunk-industry-2821169/"
    ),
    new Picture(
      _("Journey"), "journey", "Dariusz Sankowski",
      "https://pixabay.com/photos/journey-adventure-photo-map-old-1130732/"
    ),
    new Picture(
      _("Typewriter"), "typewriter", "Devanath",
      "https://pixabay.com/photos/typewriter-vintage-old-1248088/"
    ),
    new Picture(
      _("Literature"), "literature", "Ylanite Koppens",
      "https://pixabay.com/photos/literature-library-knowledge-3091212/"
    ),
    new Picture(
      _("Ring"), "ring", "Ylanite Koppens",
      "https://pixabay.com/photos/paper-page-ring-romantic-love-3061485/"
    ),

    // GAMBLING
    new Picture(_("Lottery"), "lottery", "ºDeme", "img/pictures/lotery.png"),
    new Picture(_("Dices"), "dices", "Erik Stein",
      "https://pixabay.com/illustrations/cube-random-luck-eye-numbers-1655118/"
    ),
    new Picture(_("Cards"), "cards", "PDPics",
      "https://pixabay.com/photos/card-game-game-cards-black-white-167051/"
    ),
    new Picture(_("Roulette"), "roulette", "Greg Montani",
      "https://pixabay.com/photos/luck-lucky-number-13-roulette-839035/"
    ),
    new Picture(_("Slots"), "slots", "Aidan Howe",
      "https://pixabay.com/photos/slots-slot-slot-machine-5012428/"
    ),
    new Picture(_("Market"), "market", "Gino Crescoli",
      "https://pixabay.com/illustrations/dices-over-newspaper-profit-2656028/"
    ),

    // COMICS
    new Picture(
      _("Girl"), "girl", "The Retrox",
      "https://hipwallpaper.com/view/oQ0YHj"
    ),
    new Picture(
      _("School"), "school", "JakubAnitka",
      "https://hipwallpaper.com/view/oQ0YHj"
    ),
    new Picture(
      _("Zap"), "zap", "Andrew Martin",
      "https://pixabay.com/illustrations/zap-comic-comic-book-fight-1601678/"
    ),
    new Picture(
      _("Internet"), "internet", "OpenClipart-Vectors",
      "https://pixabay.com/vectors/buying-cartoon-comic-2022595/"
    ),
    new Picture(
      _("Pen"), "pen", "Willian Yuki Fujii Memmo",
      "https://pixabay.com/illustrations/" +
        "turn-pen-manga-anime-digital-design-976930/"
    ),
    new Picture(
      _("Couple"), "couple", "01lifeleft",
      "https://pixabay.com/illustrations/" +
        "chibi-anime-cute-manga-character-2380489/"
    ),

    // CLOCKS
    new Picture(
      _("Pocket"), "pocket", "anncapictures",
      "https://pixabay.com/photos/pocket-watch-time-of-sand-time-1637393/"
    ),
    new Picture(
      _("Hourglass"), "hourglass", "anncapictures",
      "https://pixabay.com/photos/hourglass-clock-time-period-hours-2910948/"
    ),
    new Picture(
      _("Alarm Clock"), "alarmClock", "Free-Photos",
      "https://pixabay.com/photos/desk-book-candle-clock-table-1148994/"
    ),
    new Picture(
      _("Office"), "office", "Free-Photos",
      "https://www.pexels.com/photo/blur-business-clock-composition-364671/"
    ),
    new Picture(
      _("Watch"), "watch", "Free-Photos",
      "https://pixabay.com/photos/watch-time-clock-hours-minutes-690288/"
    ),
    new Picture(
      _("Monumental"), "monumental", "Tomasz Mikołajczyk",
      "https://pixabay.com/photos/clock-monument-clock-shield-time-2050857/"
    ),

    // SCI-FI
    new Picture(
      _("Corridor"), "corridor", "Parker_West",
      "https://pixabay.com/illustrations/" +
        "science-fiction-scifi-corridor-3334826/"
    ),
    new Picture(
      _("Surgery"), "surgery", "alan9187",
      "https://pixabay.com/photos/sci-fi-surgery-room-2992797/"
    ),
    new Picture(
      _("Alien"), "alien", "alan9187",
      "https://pixabay.com/photos/sci-fi-science-fiction-fantasy-5501588/"
    ),
    new Picture(
      _("Spaceship"), "spaceship", "Thomas Budach",
      "https://pixabay.com/photos/science-fiction-cover-forward-2793428/"
    ),
    new Picture(
      _("Fractal"), "fractal", "Carroll MacDonald",
      "https://pixabay.com/illustrations/mandelbulb-fractal-sci-fi-1352250/"
    ),
    new Picture(
      _("Evolution"), "evolution", "Pete Linforth",
      "https://pixabay.com/illustrations/alien-pods-space-3d-spacecraft-679474/"
    ),


    // ABSTRACT
    new Picture(
      "Triangular", "triangular", "midhunhk",
      "https://hipwallpaper.com/view/oQ0YHj"
    ),
    new Picture(
      "Reggae", "reggae", "zOnk.oNe",
      "https://hipwallpaper.com/view/cXwLaI"
    ),
    new Picture(
      "X", "x", "dedeme",
      "https://www.deviantart.com/dedeme/art/minimalCross-0-701573926"
    ),
    new Picture(
      _("Look"), "look", "dedeme",
      "https://www.deviantart.com/dedeme/art/Motherboard-3-701574182"
    ),
    new Picture(
      _("Reflections"), "reflections", "Enrique Meseguer",
      "https://pixabay.com/illustrations/" +
        "fractal-light-fractal-flower-bud-1672982/"
    ),
    new Picture(
      _("Hexagonal"), "hexagonal", "Magic Creative",
      "https://pixabay.com/illustrations/hex-hexagonal-abstract-modern-675576/"
    ),

    // CREEPY
    new Picture(
      _("Spider"), "spider", "Christine Trewer",
      "https://pixabay.com/photos/spider-tarantula-arachnophobia-1772769/"
    ),
    new Picture(
      _("Bats"), "bats", "jplenio",
      "https://pixabay.com/photos/halloween-tree-silhouette-moon-fog-4582988/"
    ),
    new Picture(
      _("Ravens"), "ravens", "Christine Sponchia",
      "https://pixabay.com/photos/raven-bridge-horror-birds-animals-5426192/"
    ),
    new Picture(
      _("Owl"), "owl", "cocoparisienne",
      "https://pixabay.com/photos/thunderstorm-flash-weather-sky-2353703/"
    ),
    new Picture(
      _("Wolf"), "wolf", "Pezibear",
      "https://pixabay.com/illustrations/dog-wolf-yelp-moon-tree-night-647528/"
    ),
    new Picture(
      _("Cat"), "cat", "cocoparisienne",
      "https://pixabay.com/illustrations/" +
        "moon-moon-night-full-moon-romance-744184/"
    ),

    // KEYS
    new Picture(
      _("Rose"), "rose", "S. Hermann & F. Richter",
      "https://pixabay.com/photos/heart-key-rose-herzchen-love-1809653/"
    ),
    new Picture(
      _("Keys"), "keys", "Florian Berger",
      "https://pixabay.com/photos/key-colorful-matching-number-74534/"
    ),
    new Picture(
      _("Tied"), "tied", "ongerdesign",
      "https://pixabay.com/photos/key-cord-symbol-symbolism-knot-2091883/"
    ),
    new Picture(
      _("Car"), "car", "J W.",
      "https://pixabay.com/photos/car-keys-interior-leather-pkw-2653311/"
    ),
    new Picture(
      _("Security"), "security", "Hans Braxmeier",
      "https://pixabay.com/photos/key-safe-key-door-key-open-make-up-182918/"
    ),
    new Picture(
      _("Duplicate"), "duplicate", "Arek Socha",
      "https://pixabay.com/illustrations/" +
        "keys-solution-business-success-2114363/"
    ),

    // ILLUSTRATIONS
    new Picture(
      _("Archs"), "archs", "DarkmoonArt_de",
      "https://pixabay.com/illustrations/" +
        "palace-starry-sky-clouds-candles-4320416/"
    ),
    new Picture(
      _("Dragon"), "dragon", "pendleburyannette",
      "https://pixabay.com/illustrations/" +
        "dragon-fantasy-animal-fairytale-4417431/"
    ),
    new Picture(
      _("Streetlight"), "streetlight", "enriquelopezgarre",
      "https://pixabay.com/illustrations/fantasy-night-sea-moon-sky-4235607/"
    ),
    new Picture(
      _("Mushrooms"), "mushrooms", "Susann Mielke",
      "https://pixabay.com/illustrations/" +
        "mushrooms-mushroom-landscape-stones-1722297/"
    ),
    new Picture(
      _("Robots"), "robots", "Stefan Keller",
      "https://pixabay.com/illustrations/" +
        "robot-planet-moon-space-forward-2256814/"
    ),
    new Picture(
      _("Cauldron"), "cauldron", "loulou Nash",
      "https://pixabay.com/illustrations/" +
        "witch-magic-halloween-witchcraft-2146712/"
    ),
  ];

  public static final wins: Array<{i: Icon, lk: String}> = [
    { i: WIN0,
      lk: "https://commons.wikimedia.org/wiki/File:Happy_smiley_face.png" },
    { i: WIN1,
      lk: "https://commons.wikimedia.org/wiki/File:" +
          "The_UserAlbum.com_social_network_logo.jpg" },
    { i: WIN2,
      lk: "https://commons.wikimedia.org/wiki/File:Face-smile-2.png" },
    { i: WIN3,
      lk: "https://commons.wikimedia.org/wiki/File:AMIGO.jpg" },
    { i: WIN4,
      lk: "https://www.iconarchive.com/show/" +
          "crystal-clear-icons-by-everaldo/App-ksmiletris-smiley-icon.html" },
    { i: WIN5,
      lk: "https://www.iconarchive.com/show/" +
          "oxygen-icons-by-oxygen-icons.org/Emotes-face-laugh-icon.html" },
    { i: WIN6,
      lk: "https://www.iconarchive.com/show/" +
          "oxygen-icons-by-oxygen-icons.org/Emotes-face-angel-icon.html" }
  ];

  /// Work board background.
  public static var back2: Domo;

  /// Audio control.
  public static final audio = Audio.mk("sound/connect.mp3", 3);

  public static function load (fn: () -> Void): Void {
    pictsGroup = model.Group.mk().value;
    final first = pictsGroup * 6;
    var ix = 0;

    ++ix;
    back2 = Ui.img("back2")
      .on(LOAD, e -> --ix);

    for (ip in first...first + 6) {
      final p = picts[ip];
      ++ix;
      var img: Domo = null;
      img = Ui.img("pictures/" + p.file)
          .on(LOAD, e -> {
              p.setImg(img);
              --ix;
            })
      ;
    }

    final tm = new haxe.Timer(50);
    var c = 0;
    tm.run = () -> {
      if (ix == 0 || c > 100) {
        tm.stop();
        if (c > 100) Ui.alert(ix + " images can not be loaded");
        fn();
      } else {
        ++c;
      }
    }
  }

  /// Returns pictures of current page.
  public static function getPicts (): Array<Picture> {
    final r = [];
    final first = pictsGroup * 6;
    for (ip in first...first + 6) {
      r.push(picts[ip]);
    }
    return r;
  }

  /// Returns a swallow copy of every picture.
  public static function getAllPicts (): Array<Picture>  {
    return picts.copy();
  }

  // Normal icons --------------------------------------------------------------

  /// Returns an DOM image.
  ///   id: Identifier.
  ///   light: 'true' if image is light (default 'false').
  public static function mkIcon (id: Icon, light = false): Domo {
    var path = switch (id) {
      case AUDIO_OFF: "audioOff";
      case AUDIO_ON: "audioOn";
      case BLANK: "blank";
      case DMPUZZLE: "DmPuzzle";
      case HELP: "help";
      case LEFT: "goLeft";
      case OK: "ok";
      case OPEN: "open";
      case RUN: "run";
      case SAVE: "save";
      case TRASH: "trash";
      default: "";
    }

    if (path == "") {
      var path = switch (id) {
        case WIN0: "win0";
        case WIN1: "win1";
        case WIN2: "win2";
        case WIN3: "win3";
        case WIN4: "win4";
        case WIN5: "win5";
        case WIN6: "win6";
        default: "";
      }

      final r = light
        ? Ui.lightImg(path)
        : Ui.img(path)
      ;
      if (Device.isBig) r.style("width:240px;height:240px");
      else r.style("width:120px;height:120px");

      return r;
    }

    if (Device.isBig) path = path + "M";

    return light
      ? Ui.lightImg(path)
      : Ui.img(path)
    ;
  }

}
