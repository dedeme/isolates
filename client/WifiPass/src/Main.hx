
import dm.Ui.Q;
import dm.Cryp;

class Main {
  static final inWg = Q("input").att("type", "password");
  public static function main() {
    Q("@body")
      .add(Q("p")
        .text("Normal Password:"))
      .add(inWg)
      .add(Q("span")
        .html("&nbsp;&nbsp;"))
      .add(Q("button")
        .text("Ok")
        .on(CLICK, show))
    ;
  }

  static function show () {
    Q("@body")
      .removeAll()
      .add(Q("p")
        .text(Cryp.decryp(
            Cryp.key(inWg.getValue(), 30),
            "0rmLgr/XuaKS8NSq0mfFf7e6fcDWvqLH0IutcA=="
          )))
    ;
  }
}
