// Copyright 13-Nov-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import I18n._;
import I18n._args;

/// Restore dialog.
class RestoreBox {

  // Object --------------------------------------------------------------------

  final wg = Q("div");
  final inp = Q("input");

  public function new () {
    inp
      .att("type", "file")
      .att("hidden", "true")
      .on(CHANGE, e -> onChange())
    ;
  }

  public function onChange () {
    final files = cast(inp.e, js.html.InputElement).files;
    if (files.length == 0) {
      return;
    }
    final file = files.item(0);
    if (!Ui.confirm(_args(_("Restore '%0'?"), [file.name]))) return;
    final fr = new js.html.FileReader();
    fr.onload = e ->
      if (!accept(fr.result)) {
        Ui.alert(_args(_("'%0' is not a valid backup"), [file.name]));
      }
    fr.onerror = e -> {
      fr.abort();
      Ui.alert(_("Fail reading file"));
    }
    fr.readAsText(files.item(0));
  }

  // CONTROL -------------------------------------------------------------------

  function cancel (): Void {
    Model.isRestore = false;
    View.update();
  }

  function accept (data: String): Bool {
    if (Model.initFromJs(data)) {
      Model.isRestore = false;
      js.Browser.location.reload();
      return true;
    }
    return false;
  }

  // DOM -----------------------------------------------------------------------

  public function show (parent: Domo) {
    parent.removeAll().add(wg);
    update();
  }

  function update () {
    wg
      .removeAll()
      .add(inp)
      .add(Q("div")
        .klass("header")
        .text(_("Restore Backup")))
      .add(Q("table")
        .att("align", "center")
        .add(Q("tr")
          .add(Q("td")
            .add(Q("button")
              .text(_("Select file"))
              .on(CLICK, e -> inp.e.click())))
          .add(Q("td")
            .add(Q("button")
              .text(_("Cancel"))
              .on(CLICK, cancel)))))
    ;
  }
}
