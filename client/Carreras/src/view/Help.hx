// Copyright 30-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;

enum HelpType { CIRCUIT; TOUR; RACE; }

/// Help view.
class Help {
  /// Object -------------------------------------------------------------------

  final wg = Q("div");

  var type: HelpType;

  public function new (type: HelpType) {
    this.type = type;
  }

  /// DOM ----------------------------------------------------------------------

  public function show (parent: Domo): Void {
    parent.removeAll().add(wg);
    update();
  }

  public function update (): Void {
    wg.removeAll().add(Q("p").text(Std.string(type)));
  }
}
