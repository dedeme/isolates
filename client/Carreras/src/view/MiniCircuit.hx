// Copyright 06-Nov-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view;

import js.html.CanvasRenderingContext2D as Cx;
import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.Opt;
import view.plot.MiniCircuitPlotter;
import data.Circuit;

/// Mini circuit component.
class MiniCircuit {
  static final fore = "#80a0ff";
  static final fore2 = "#ffffff";
  static final back = "#505535";
  // Object --------------------------------------------------------------------

  var circuit: Circuit;
  public var sel = false;

  final wg = Q("div");

  var action: Option<Circuit -> Void> = None;
  var cv: Domo;
  var cx: Cx;

  public function new (circuit: Circuit, ?action: Circuit -> Void) {
    this.circuit = circuit;
    if (action != null) this.action = Some(action);

    cv = Q("canvas")
      .att("width", MiniCircuitPlotter.circuitWidth)
      .att("height", MiniCircuitPlotter.circuitHeight)
      .style("background: #000000")
      .on(CLICK, e -> onClick())
    ;
    final cve = cast(cv.e, js.html.CanvasElement);
    cx = cve.getContext2d();
  }

  function onClick () {
    switch (action) {
      case Some(ac):
        ac(circuit);
      case None:
    }
  }

  // DOM -----------------------------------------------------------------------

  public function show (parent: Domo): Void {
    parent.removeAll().add(wg);
    update();
  }

  public function update (): Void {
    final bk = sel ? fore2 : back;
    final fr = sel ? back : fore;
    MiniCircuitPlotter.plot(cx, circuit, bk, fr);
    wg
      .removeAll()
      .add(cv)
    ;
  }

}
