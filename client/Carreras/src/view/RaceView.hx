// Copyright 30-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.Opt;
import dm.ModalBox;
import view.race.Title;
import view.race.Clock;
import view.race.CircuitView;
import view.race.EndMsg;

/// Race view.
class RaceView {
  /// Object -------------------------------------------------------------------

  final wg = Q("div");
  final titleDiv = Q("div");
  final clockDiv = Q("div");
  final circuitDiv = Q("div");
  final endMessageDiv = Q("div");

  public var title(default, null): Title;
  public var clock(default, null): Clock;
  public var circuit(default, null): CircuitView;
  var endMessage: ModalBox;

  public function new () {
    if (Cts.isMobil) {
      wg
        .removeAll()
        .add(Q("table")
          .att("align", "center")
          .add(Q("tr")
            .add(Q("td")
              .add(circuitDiv)))
          .add(Q("tr")
            .add(Q("td")
              .add(Q("table")
                .att("align", "center")
                .klass("frame")
                .add(Q("tr")
                  .add(Q("td")
                    .add(titleDiv))))))
          .add(Q("tr")
            .add(Q("td")
              .add(Q("table")
                .att("align", "center")
                .add(Q("tr")
                  .add(Q("td")
                    .add(clockDiv)))))))
      ;
    } else {
      wg
        .removeAll()
        .add(Q("table")
          .klass("main")
          .add(Q("tr")
            .add(Q("td")
              .style("vertical-align:top;width:5px;height:5px")
              .add(Q("div")
                .klass("frame")
                .add(titleDiv)))
            .add(Q("td")
              .att("rowspan", "2")
              .style("vertical-align:top;text-align: center")
              .add(circuitDiv)))
          .add(Q("tr")
            .add(Q("td")
              .style("vertical-align:top")
              .add(clockDiv))))
      ;
    }

    endMessage = new ModalBox(endMessageDiv, false);
    wg.add(endMessage.wg);
  }

  /// DOM ----------------------------------------------------------------------

  public function show (parent: Domo) {
    parent.removeAll().add(wg);
    update();
  }

  public function update () {
    title = new Title();
    title.show(titleDiv);
    clock = new Clock();
    clock.show(clockDiv);
    circuit = new CircuitView();
    circuit.show(circuitDiv);

    if (Model.tourFinished()) {
      new EndMsg(TOUR).show(endMessageDiv);
      endMessage.show(true);
    } else if (Opt.eget(Model.race).state == POST_RACE) {
      new EndMsg(RACE).show(endMessageDiv);
      endMessage.show(true);
    }
  }

}
