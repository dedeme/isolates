// Copyright 29-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.Opt;
import model.SelMenu;
import Loader.mkIcon;
import I18n._;

class Menu {
  static final selStyle =
    "background:#fffff9;border-style: solid; border-width: 1px";

  /// Sort separator.
  public static function mkSeparator (): Domo {
    return Q("span").text(" ");
  }

  /// Long separator
  public static function mkSeparator2 (isMobil: Bool): Domo {
    return mkIcon(SEPARATOR);
  }

  // Obejct --------------------------------------------------------------------

  final circuitSpan = Q("span");
  final tourDesignSpan = Q("span");
  final tourSelSpan = Q("span");
  final tourEndSpan = Q("span");
  final raceStartSpan = Q("span");
  final raceStopSpan = Q("span");
  final raceEndSpan = Q("span");
  final rankingSpan = Q("span");
  final resetSpan = Q("span");
  final loadSpan = Q("span");
  final saveSpan = Q("span");
  final helpSpan = Q("span");
  final changeLangSpan = Q("span");
  final appEndSpan = Q("span");
  final wg: Domo;

  public function new () {
    wg = Q("div")
      .add(Q("table")
        .klass("main")
        .add(Q("tr")
          .add(Q("td")
            .style("padding-right:4px;text-align:left;white-space:nowrap")
            .add(circuitSpan)
            .add(mkSeparator())
            .add(tourDesignSpan)
            .add(mkSeparator2(Cts.isMobil))
            .add(tourSelSpan)
            .add(mkSeparator())
            .add(tourSelSpan)
            .add(mkSeparator())
            .add(tourEndSpan)
            .add(mkSeparator2(Cts.isMobil))
            .add(raceStartSpan)
            .add(mkSeparator())
            .add(raceStopSpan)
            .add(mkSeparator())
            .add(raceEndSpan)
            .add(mkSeparator2(Cts.isMobil))
            .add(rankingSpan))
          .add(Q("td")
            .style("padding-left:4px;text-align:right;white-space:nowrap")
            .add(resetSpan)
            .add(mkSeparator2(Cts.isMobil))
            .add(loadSpan)
            .add(mkSeparator())
            .add(saveSpan)
            .add(mkSeparator2(Cts.isMobil))
            .add(helpSpan)
            .add(mkSeparator())
            .add(changeLangSpan)
            .add(mkSeparator())
            .add(appEndSpan))))
      .add(Q("hr"))
    ;
  }

  // CONTROL -------------------------------------------------------------------

  function menuSel (sel: MenuOption) {
    switch (sel) {
      case CIRCUIT:
        Model.newCircuitDesign();
        Model.selMenu.setValue(sel);
        View.update();
      case TOUR_DESIGN:
        Model.selMenu.setValue(sel);
        View.update();
      case TOUR_SEL:
        Model.selMenu.setValue(sel);
        View.update();
      case TOUR_END:
        if (Ui.confirm(
          _("Time of this tour will not be added to results.\nContinue?")
        )) {
          Model.finishTour();
          Model.selMenu.setValue(TOUR_SEL);
          View.update();
        }
      case RACE_START:
        final race = Opt.eget(Model.race);
        race.start();
        race.save();
        Model.selMenu.setValue(sel);
        View.menu.update();
        Model.getRaceTimer().start();
      case RACE_STOP:
        Model.getRaceTimer().stop(() -> {
          Opt.eget(Model.race).save();
          Model.saveRunners(false);
          Model.selMenu.setValue(sel);
          View.update();
        });
      case RACE_END:
        if (Ui.confirm(
          _("Time of this stage will not be added to results.\nContinue?")
        )) {
          final race = Opt.eget(Model.race);
          race.end();
          race.save();
          View.body.race.update();
        }
      case RANKING:
        Model.selMenu.setValue(sel);
        View.update();
      case RESET:
        Model.reset();
        js.Browser.location.reload();
      case RESTORE:
        Model.isRestore = true;
        View.update();
      case HELP_CIRCUIT | HELP_RACE:
        Model.selMenu.setValue(sel);
        View.update();
      case CHANGE_LANG:
        Model.lang.setValue(Model.lang.value == "es" ? "en" : "es");
        js.Browser.location.reload();
      case APP_END:
        Model.getRaceTimer().stop(() -> {
          Model.isAppFinished = true;
          View.update();
        });
      default:
        Ui.alert(sel);
    }
  }

  // DOM -----------------------------------------------------------------------

  public function show (parent: Domo): Void {
    parent.removeAll().add(wg);
    update();
  }

  public function update (): Void {
    final sel = Model.selMenu.value;

    circuitSpan.removeAll().add(mkCircuit0());
    tourDesignSpan.removeAll().add(mkTourDesign0());
    tourSelSpan.removeAll().add(mkTourSel0());
    tourEndSpan.removeAll().add(mkTourEnd0());
    raceStartSpan.removeAll().add(mkRaceStart0());
    raceStopSpan.removeAll().add(mkRaceStop0());
    raceEndSpan.removeAll().add(mkRaceEnd0());
    rankingSpan.removeAll().add(mkRanking0());
    resetSpan.removeAll().add(mkReset0());
    loadSpan.removeAll().add(mkLoad0());
    saveSpan.removeAll().add(mkSave0());
    helpSpan.removeAll().add(mkHelp0());
    changeLangSpan.removeAll().add(mkLang0());
    appEndSpan.removeAll().add(mkEnd0());

    if (sel != APP_END) {
      changeLangSpan.removeAll().add(mkLang());
      appEndSpan.removeAll().add(mkEnd());
    }

    switch (sel) {
      case CIRCUIT:
        circuitSpan.removeAll().add(mkCircuit1());
        tourDesignSpan.removeAll().add(mkTourDesign());
        switch (Model.race) {
          case Some(r): raceStopSpan.removeAll().add(mkRaceStop2());
          case None: tourSelSpan.removeAll().add(mkTourSel());
        }
        rankingSpan.removeAll().add(mkRanking());
        resetSpan.removeAll().add(mkReset());
        loadSpan.removeAll().add(mkLoad());
        saveSpan.removeAll().add(mkSave());
        helpSpan.removeAll().add(mkHelpCircuit());
      case TOUR_DESIGN:
        circuitSpan.removeAll().add(mkCircuit());
        tourDesignSpan.removeAll().add(mkTourDesign1());
        switch (Model.race) {
          case Some(r): raceStopSpan.removeAll().add(mkRaceStop2());
          case None: tourSelSpan.removeAll().add(mkTourSel());
        }
        rankingSpan.removeAll().add(mkRanking());
        resetSpan.removeAll().add(mkReset());
        loadSpan.removeAll().add(mkLoad());
        saveSpan.removeAll().add(mkSave());
        helpSpan.removeAll().add(mkHelpTourDesign());
      case TOUR_SEL:
        circuitSpan.removeAll().add(mkCircuit());
        tourDesignSpan.removeAll().add(mkTourDesign());
        tourSelSpan.removeAll().add(mkTourSel1());
        rankingSpan.removeAll().add(mkRanking());
        resetSpan.removeAll().add(mkReset());
        loadSpan.removeAll().add(mkLoad());
        saveSpan.removeAll().add(mkSave());
        helpSpan.removeAll().add(mkHelpTourSel());
      case TOUR_END:
      case RACE_START:
        raceStartSpan.removeAll().add(mkRaceStart1());
        raceStopSpan.removeAll().add(mkRaceStop());
      case RACE_STOP:
        circuitSpan.removeAll().add(mkCircuit());
        tourDesignSpan.removeAll().add(mkTourDesign());
        tourEndSpan.removeAll().add(mkTourEnd());
        raceStartSpan.removeAll().add(mkRaceStart());
        raceStopSpan.removeAll().add(mkRaceStop1());
        raceEndSpan.removeAll().add(mkRaceEnd());
        rankingSpan.removeAll().add(mkRanking());
        resetSpan.removeAll().add(mkReset());
        loadSpan.removeAll().add(mkLoad());
        saveSpan.removeAll().add(mkSave());
        helpSpan.removeAll().add(mkHelpRace());
      case RACE_END:
      case RANKING:
        circuitSpan.removeAll().add(mkCircuit());
        tourDesignSpan.removeAll().add(mkTourDesign());
        switch (Model.race) {
          case Some(r): raceStopSpan.removeAll().add(mkRaceStop2());
          case None: tourSelSpan.removeAll().add(mkTourSel());
        }
        rankingSpan.removeAll().add(mkRanking1());
        resetSpan.removeAll().add(mkReset());
        loadSpan.removeAll().add(mkLoad());
        saveSpan.removeAll().add(mkSave());
        helpSpan.removeAll().add(mkHelpCircuit());
      case RESET:
      case RESTORE:
      case HELP_CIRCUIT:
        circuitSpan.removeAll().add(mkCircuit());
      case HELP_TOUR_DESIGN:
        tourDesignSpan.removeAll().add(mkTourDesign());
      case HELP_TOUR_SEL:
        tourDesignSpan.removeAll().add(mkTourSel());
      case HELP_RACE:
        raceStopSpan.removeAll().add(mkRaceStop2());
      case CHANGE_LANG:
      case APP_END:
    }
  }

  function mkCircuit0 (): Domo {
    return mkIcon(CIRCUIT_DESIGN, true)
      .att("title", _("Circuit Design"))
    ;
  }

  function mkCircuit1 (): Domo {
    return mkIcon(CIRCUIT_DESIGN)
      .att("title", _("Circuit Design"))
      .style(selStyle)
    ;
  }

  function mkCircuit (): Domo {
    return Ui.link(e -> menuSel(CIRCUIT))
      .add(mkIcon(CIRCUIT_DESIGN))
      .att("title", _("Circuit Design"))
    ;
  }

  function mkTourDesign0 (): Domo {
    return mkIcon(TOUR_DESIGN, true)
      .att("title", _("Tour Design"))
    ;
  }

  function mkTourDesign1 (): Domo {
    return mkIcon(TOUR_DESIGN)
      .att("title", _("Tour Design"))
      .style(selStyle)
    ;
  }

  function mkTourDesign (): Domo {
    return Ui.link(e -> menuSel(TOUR_DESIGN))
      .add(mkIcon(TOUR_DESIGN))
      .att("title", _("Tour Design"))
    ;
  }

  function mkTourSel0 (): Domo {
    return mkIcon(TOUR_SEL, true)
      .att("title", _("Tour Selection"))
    ;
  }

  function mkTourSel1 (): Domo {
    return mkIcon(TOUR_SEL)
      .att("title", _("Tour Selection"))
      .style(selStyle)
    ;
  }

  function mkTourSel (): Domo {
    return Ui.link(e -> menuSel(TOUR_SEL))
      .add(mkIcon(TOUR_SEL))
      .att("title", _("Tour Selection"))
    ;
  }

  function mkTourEnd0 (): Domo {
    return mkIcon(END, true)
      .att("title", _("End Tour"))
    ;
  }

  function mkTourEnd (): Domo {
    return Ui.link(e -> menuSel(TOUR_END))
      .add(mkIcon(END))
      .att("title", _("End Tour"))
    ;
  }

  function mkRaceStop0 (): Domo {
    return mkIcon(LOGO, true)
      .att("title", _("Stage"))
    ;
  }

  function mkRaceStop1 (): Domo {
    return mkIcon(LOGO)
      .att("title", _("Stage"))
      .style(selStyle)
    ;
  }

  function mkRaceStop2 (): Domo {
    return Ui.link(e -> menuSel(RACE_STOP))
      .add(mkIcon(LOGO))
      .att("title", _("Stage"))
    ;
  }

  function mkRaceStop (): Domo {
    return Ui.link(e -> menuSel(RACE_STOP))
      .add(mkIcon(STOP))
      .att("title", _("Stage Stop"))
    ;
  }

  function mkRaceStart0 (): Domo {
    return mkIcon(RACE_START, true)
      .att(
          "title",
          switch(Model.race) {
            case Some(r):
              r.state == PRE_RACE ? _("Start Stage") : _("Continue Stage");
            case None: _("Stage");
          }
        )
    ;
  }

  function mkRaceStart1 (): Domo {
    return mkIcon(RACE_START)
      .att("title", _("Start Stage"))
      .style(selStyle)
    ;
  }

  function mkRaceStart (): Domo {
    return Ui.link(e -> menuSel(RACE_START))
      .add(mkIcon(RACE_START))
      .att(
          "title",
          Opt.eget(Model.race).state == PRE_RACE
            ? _("Start Stage")
            : _("Continue Stage")
        )
    ;
  }

  function mkRaceEnd0 (): Domo {
    return mkIcon(END_RACE, true)
      .att("title", _("End Stage"))
    ;
  }

  function mkRaceEnd (): Domo {
    return Ui.link(e -> menuSel(RACE_END))
      .add(mkIcon(END_RACE))
      .att("title", _("End Stage"))
    ;
  }

  function mkRanking0 (): Domo {
    return mkIcon(RANKING, true)
      .att("title", _("Ranking"))
    ;
  }

  function mkRanking (): Domo {
    return Ui.link(e -> menuSel(RANKING))
      .add(mkIcon(RANKING))
      .att("title", _("Ranking"))
    ;
  }

  function mkRanking1 (): Domo {
    return mkIcon(RANKING)
      .att("title", _("Ranking"))
      .style(selStyle)
    ;
  }

  function mkReset0 (): Domo {
    return mkIcon(RESET, true)
      .att("title", _("Reset"))
    ;
  }

  function mkReset (): Domo {
    return Ui.link(e ->
        if (Ui.confirm(_("Runners data will be reset.\nContinue?")))
          menuSel(RESET)
      )
      .add(mkIcon(RESET))
      .att("title", _("Reset"))
    ;
  }

  function mkLoad0 (): Domo {
    return mkIcon(SELECT, true)
      .att("title", _("Restore"))
    ;
  }

  function mkLoad (): Domo {
    return Ui.link(e -> menuSel(RESTORE))
      .add(mkIcon(SELECT))
      .att("title", _("Restore"))
    ;
  }

  function mkSave0 (): Domo {
    return mkIcon(SAVE, true)
      .att("title", _("Backup"))
    ;
  }

  function mkSave (): Domo {
    final link = Q("a")
      .att("download", "carrerasBackup.json")
      .att("hidden", "true")
    ;
    return Q("span")
      .add(link)
      .add(Ui.link(ev -> {
          link.att(
            "href",
            "data:application/octet-stream;base64," + Model.serialize()
          );
          link.e.click();
        })
        .add(mkIcon(SAVE))
        .att("title", _("Backup")))
    ;
  }

  function mkHelp0 (): Domo {
    return mkIcon(HELP, true)
      .att("title", _("Help"))
    ;
  }

  function mkHelpCircuit (): Domo {
    return Ui.link(e -> menuSel(HELP_CIRCUIT))
      .add(mkIcon(HELP))
      .att("title", _("Help"))
    ;
  }

  function mkHelpTourDesign (): Domo {
    return Ui.link(e -> menuSel(HELP_TOUR_DESIGN))
      .add(mkIcon(HELP))
      .att("title", _("Help"))
    ;
  }

  function mkHelpTourSel (): Domo {
    return Ui.link(e -> menuSel(HELP_TOUR_SEL))
      .add(mkIcon(HELP))
      .att("title", _("Help"))
    ;
  }

  function mkHelpRace (): Domo {
    return Ui.link(e -> menuSel(HELP_RACE))
      .add(mkIcon(HELP))
      .att("title", _("Help"))
    ;
  }

  function mkLang0 (): Domo {
    return (Model.lang.value == "en"
        ? mkIcon(EN_FLAG, true)
        : mkIcon(ES_FLAG, true)
      )
      .att("title", "Change language\nCambiar lenguaje")
    ;
  }

  function mkLang (): Domo {
    return Ui.link(e -> menuSel(CHANGE_LANG))
      .add(Model.lang.value == "en"
        ? mkIcon(EN_FLAG)
        : mkIcon(ES_FLAG)
        )
      .att("title", "Change language\nCambiar lenguaje")
    ;
  }

  function mkEnd0 (): Domo {
    return mkIcon(CROSS, true)
      .att("title", _("End application"))
    ;
  }

  function mkEnd (): Domo {
    return Ui.link(e -> menuSel(APP_END))
      .add(mkIcon(CROSS))
      .att("title", _("End application"))
    ;
  }

}
