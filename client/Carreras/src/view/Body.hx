// Copyright 30-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;

/// View body.
class Body {
  /// Object -------------------------------------------------------------------

  final wg = Q("div");
  public var circuit(default, null): CircuitDesignView;
  public var tourDesign(default, null): TourDesignView;
  public var tourSel(default, null): TourSel;
  public var race(default, null): RaceView;

  public function new () {}

  /// DOM ----------------------------------------------------------------------

  public function show (parent: Domo) {
    parent.removeAll().add(wg);
    update();
  }

  public function update () {
    switch (Model.selMenu.value) {
      case CIRCUIT:
        circuit = new CircuitDesignView();
        circuit.show(wg);
      case TOUR_DESIGN:
        tourDesign = new TourDesignView();
        tourDesign.show(wg);
      case TOUR_SEL:
        tourSel = new TourSel();
        tourSel.show(wg);
      case RACE_START | RACE_STOP | RACE_END:
        race = new RaceView();
        race.show(wg);
      case RANKING:
        new RankingView().show(wg);
      case HELP_CIRCUIT:
        new Help(CIRCUIT).show(wg);
      case HELP_RACE:
        new Help(RACE).show(wg);
      default:
        wg.removeAll().add(Q("p").text("Body"));
    }
  }

}
