// Copyright 16-Nov-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view.tourDesign;

import js.html.CanvasRenderingContext2D as Cx;
import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.It;
import dm.Opt;
import dm.Menu;
import dm.Spinner;
import I18n._;
import I18n._args;

/// Left panel of circuit design.
class LeftPanel {
  /// Object -------------------------------------------------------------------

  final wg = Q("div");
  final nameIn = Q("input")
    .att("type", "text")
    .style("width: " + (Cts.isMobil ? "400px" : "200px"))
  ;
  final newSpan = Q("span");
  final saveSpan = Q("span");
  final delSpan = Q("span");
  final upSpan = Q("span");
  final downSpan = Q("span");
  final leftSpan = Q("span");
  final rightSpan = Q("span");
  final circuitsTable = Q("table");

  public final spinners: Array<Spinner> = [];

  var cx: Cx;

  public function new () {
    nameIn.on(INPUT, e -> name(nameIn.getValue()));

    final lopts = [
      new MenuEntry(None, newSpan),
      new MenuEntry(None, view.Menu.mkSeparator()),
      new MenuEntry(None, saveSpan),
      new MenuEntry(None, view.Menu.mkSeparator2(Cts.isMobil)),
      new MenuEntry(None, delSpan),
      new MenuEntry(None, view.Menu.mkSeparator2(Cts.isMobil)),
      new MenuEntry(None, leftSpan),
      new MenuEntry(None, view.Menu.mkSeparator()),
      new MenuEntry(None, upSpan),
      new MenuEntry(None, view.Menu.mkSeparator()),
      new MenuEntry(None, downSpan),
      new MenuEntry(None, view.Menu.mkSeparator()),
      new MenuEntry(None, rightSpan)
    ];
    final menu = new Menu(lopts, [], "");

    wg
      .removeAll()
      .add(Q("table")
        .att("align", "center")
        .add(Q("tr")
          .add(Q("td")
            .add(nameIn))))
      .add(menu.wg)
      .add(circuitsTable)
    ;
  }

  // CONTROL -------------------------------------------------------------------

  function name (name: String): Void {
    Model.mtour.setId(name);
    View.body.tourDesign.leftPanel.update();
  }

  function laps (ix: Int, value: Int): Void {
    Model.mtour.setLaps(ix, value);
  }

  function newTour (): Void {
    if (!Ui.confirm(_("Start a new tour?"))) return;
    Model.mtour.reset();
    View.body.tourDesign.leftPanel.update();
  }

  function save (): Void {
    if (Model.duplicateTour()){
      if (!Ui.confirm(_args(
        _("Already exists other tour named '%0'.\nContinue?"),
        [Model.mtour.value.id]
      ))) {
        return;
      };
      Model.delTour(Model.mtour.value.id);
    }

    Model.saveTour();
    Ui.alert(_("Tour successfully saved"));
    Model.mtour.reset();
    View.body.tourDesign.leftPanel.update();
  }

  function sel (ix: Int): Void {
    Model.mtour.setSel(ix);
    View.body.tourDesign.leftPanel.update();
  }

  function del (): Void {
    if (!Ui.confirm(_("Remove cirucuit?"))) return;
    Model.mtour.del();
    View.body.tourDesign.leftPanel.update();
  }

  function up (): Void {
    Model.mtour.up();
    View.body.tourDesign.leftPanel.update();
  }

  function right (): Void {
    Model.mtour.right();
    View.body.tourDesign.leftPanel.update();
  }

  function down (): Void {
    Model.mtour.down();
    View.body.tourDesign.leftPanel.update();
  }

  function left (): Void {
    Model.mtour.left();
    View.body.tourDesign.leftPanel.update();
  }

  // DOM -----------------------------------------------------------------------

  public function show (parent: Domo) {
    parent.removeAll().add(wg);
    update();
  }

  public function update () {
    final tour = Model.mtour.value;
    final len = tour.getTotalCircuits();
    final rows = len == 0 ? 0 : Std.int(len / Cts.circuitsDesignCols) + 1;

    final circuits: Array<MiniCircuit> = [];
    spinners.splice(0, spinners.length);

    for (i in 0...tour.getTotalCircuits()) {
      final circuit = tour.getCircuit(i);
      final miniCircuit = new MiniCircuit(circuit, c -> sel(i));
      if (i == tour.sel) miniCircuit.sel = true;
      circuits.push(miniCircuit);
      final spinner = new Spinner(1, 9, tour.getLaps(i));
      spinner.onChange = v -> laps(i, v);
      spinners.push(spinner);
    }

    nameIn
      .value(tour.id)
    ;

    circuitsTable
      .removeAll()
      .klass("frame")
      .att("align", "center")
      .adds(rows == 0
        ? [Q("tr").add(Q("td").text(_("Without circuits")))]
        : It.range(rows).map(r ->
            Q("tr")
            .adds(It.range(Cts.circuitsDesignCols).map(c -> {
              final ix = r * Cts.circuitsDesignCols + c;
              final circuitTd = Q("td");
              final spinnerTd = Q("td");
              if (ix < len) {
                circuits[ix].show(circuitTd);
                spinnerTd.add(spinners[ix].wg.att("align", "center"));
              }
              return Q("td")
                .add(Q("table")
                  .klass("main")
                  .add(Q("tr").add(circuitTd))
                  .add(Q("tr").add(spinnerTd)))
              ;
            }).to())
          ).to())
    ;

    final newEl = Ui.link(newTour).add(Loader.mkIcon(NEW));
    var saveEl = Loader.mkIcon(SAVE, true);
    var delEl = Loader.mkIcon(TRASH, true);
    var leftEl = Loader.mkIcon(GO_LEFT, true);
    var upEl = Loader.mkIcon(GO_UP, true);
    var downEl = Loader.mkIcon(GO_DOWN, true);
    var rightEl = Loader.mkIcon(GO_RIGHT, true);

    if (tour.id != "" && tour.getTotalCircuits() > 0)
      saveEl = Ui.link(save).add(Loader.mkIcon(SAVE));

    if (tour.sel != -1) {
      delEl = Ui.link(del).add(Loader.mkIcon(TRASH));

      final tt = tour.getTotalCircuits();
      final nc = Cts.circuitsDesignCols;
      if (tour.sel > 0)
        leftEl = Ui.link(left).add(Loader.mkIcon(GO_LEFT));
      if (tour.sel < tt - 1)
        rightEl = Ui.link(right).add(Loader.mkIcon(GO_RIGHT));
      if (tour.sel >= nc)
        upEl = Ui.link(up).add(Loader.mkIcon(GO_UP));
      if (tour.sel < tt - nc)
        downEl = Ui.link(down).add(Loader.mkIcon(GO_DOWN));
    }

    newSpan.removeAll().add(newEl);
    saveSpan.removeAll().add(saveEl);
    delSpan.removeAll().add(delEl);
    leftSpan.removeAll().add(leftEl);
    upSpan.removeAll().add(upEl);
    downSpan.removeAll().add(downEl);
    rightSpan.removeAll().add(rightEl);
  }
}
