// Copyright 16-Nov-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view.tourDesign;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.It;
import I18n._;

/// Right panel of circuit design.
class RightPanel {
  /// Object -------------------------------------------------------------------

  final wg = Q("div");
  final delSpan = Q("span");
  final leftSpan = Q("span");
  final upSpan = Q("span");
  final downSpan = Q("span");
  final rightSpan = Q("span");
  final body = Q("div");

  public function new () {
    final cs = Model.circuits.getValue();
    final len = cs.length;
    final rows = Std.int(len / Cts.circuitsDesignCols) + 1;
    wg
      .removeAll()
      .add(Q("table")
        .klass("frame")
        .adds(It.range(rows).map(r ->
          Q("tr")
            .adds(It.range(Cts.circuitsDesignCols).map(c -> {
              final ix = r * Cts.circuitsDesignCols + c;
              final td = Q("td");
              if (ix < len) {
                new MiniCircuit(
                  cs[ix],
                  c -> add(cs[ix].id)
                ).show(td);
              }
              return td;
            }).to())
        ).to()))
    ;
  }

  // CONTROL -------------------------------------------------------------------

  function add (circuitId: Int): Void {
    final tour = Model.mtour.value;
    tour.addCircuit(circuitId, 5);
    Model.mtour.setValue(tour);
    View.body.tourDesign.leftPanel.update();
  }

  // DOM -----------------------------------------------------------------------

  public function show (parent: Domo) {
    parent.removeAll().add(wg);
    update();
  }

  public function update () {
  }
}
