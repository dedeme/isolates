// Copyright 30-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view.race;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.It;
import dm.Opt;
import I18n._;

/// Title view.
class Title {
  static final clockPadding = "padding-top:" +
    (Cts.isMobil ? "10" : "5") + "px;";
  static final numberStyle = "height:" + (Cts.isMobil ? "40" : "22") + "px;" +
    "vertical-align:middle;";

  /// Object -------------------------------------------------------------------

  final wg = Q("div");
  final lapTd = Q("td").klass("frame");
  final timeTd = Q("td");

  var lastTime = "";

  public function new () {
    final race = Opt.eget(Model.race);
    final circuit = race.circuit;
    final tour = Model.getTour(race.tour);
    final totalCircuits = tour.getTotalCircuits();

    wg
      .removeAll()
      .add(Q("div")
        .klass("header")
        .text(tour.id))
      .add(Q("table")
        .klass("main")
        .add(Q("tr")
          .add(Q("td")
            .text(_("Stage") + ": "))
          .add(Q("td")
            .klass("frame")
            .text((race.circuitIx + 1) + "/" + totalCircuits))
          .add(Q("td")
            .html("&nbsp;&nbsp;"))
          .add(Q("td")
            .text(_("Lap") + ": "))
          .add(lapTd))
        .add(Q("tr")
          .add(Q("td")
            .att("colspan", "5")
            .style(clockPadding)
            .add(Q("table")
              .klass("frame")
              .att("align", "center")
              .add(Q("tr")
                .add(timeTd))))))
    ;
  }

  /// DOM ----------------------------------------------------------------------

  public function show (parent: Domo) {
    parent.removeAll().add(wg);
    update();
  }

  public function update () {
    final race = Opt.eget(Model.race);
    lapTd
      .removeAll()
      .text((race.lap + 1) + "/" + (race.totalLaps))
    ;

    var time = Cts.formatTime(race.time);
    time = time.substring(0, time.length - 4);
    if (time == lastTime) return;
    lastTime = time;
    final len = time.length;
    timeTd
      .removeAll()
      .adds(It.range(len).map(i ->
          i == len - 3 || i == len - 6
            ? Ui.img("nm/d").style(numberStyle)
            : Ui.img("nm/" + time.charAt(i)).style(numberStyle)
        ).to())
    ;
  }
}
