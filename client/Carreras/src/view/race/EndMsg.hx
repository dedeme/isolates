// Copyright 30-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view.race;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.Opt;
import I18n._;

enum EndMsgType { TOUR; RACE; }

/// End race or tour message.
class EndMsg {
  static final flag = Cts.isMobil ? "raceBigM" : "raceBig";

  // Object --------------------------------------------------------------------

  final wg = Q("div");

  var type: EndMsgType;

  public function new (type: EndMsgType) {
    this.type = type;
  }

  // CONTROL -------------------------------------------------------------------

  function endStage (): Void {
    final race = Opt.eget(Model.race);
    final ix = race.circuitIx + 1;
    if (ix == Model.getTour(race.tour).getTotalCircuits()) {
      endTour();
      return;
    }
    Model.newRace(race.tour, ix);
    Model.selMenu.setValue(RACE_STOP);
    View.update();
  }

  function endTour (): Void {
    Model.finishTour();
    Model.selMenu.setValue(TOUR_SEL);
    View.update();
  }

  // DOM -----------------------------------------------------------------------

  public function show (parent: Domo) {
    parent.removeAll().add(wg);
    update();
  }

  function update () {
    wg
      .removeAll()
      .add(type == TOUR ? tour() : race())
    ;
  }

  function head (): Domo {
    return Q("table")
      .klass("frame")
      .att("align", "center")
      .add(Q("tr")
        .add(Q("td")
          .add(Ui.img(flag))))
      .add(Q("tr")
        .add(Q("td")
          .add(Q("div")
            .klass("header")
            .text(Cts.appName))))
    ;
  }

  function race (): Domo {
    return Q("div")
      .add(head())
      .add(Q("div").html("&nbsp;"))
      .add(Q("div")
        .text(_("Stage finished")))
      .add(Q("div").html("&nbsp;"))
      .add(Q("button")
        .klass("button")
        .html(_("Continue"))
        .on(CLICK, endStage))
    ;
  }

  function tour (): Domo {
    return Q("div")
      .add(head())
      .add(Q("div").html("&nbsp;"))
      .add(Q("div")
        .text(_("Tour finished")))
      .add(Q("div").html("&nbsp;"))
      .add(Q("button")
        .klass("button")
        .html(_("Continue"))
        .on(CLICK, endTour))
    ;
  }

}
