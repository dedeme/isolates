// Copyright 30-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view.race;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.It;
import dm.Opt;
import data.Runner;
import view.plot.CircuitPlotter;

/// Circuit view.
class CircuitView {
  /// Object -------------------------------------------------------------------

  final wg = Q("div");
  final cx: js.html.CanvasRenderingContext2D;

  public function new () {
    final cv = Q("canvas")
      .att("width", CircuitPlotter.circuitWidth)
      .att("height", CircuitPlotter.circuitHeight)
      .style("background: " + Cts.circuitBackground)
    ;
    final cve = cast(cv.e, js.html.CanvasElement);
    cx = cve.getContext2d();
    final circuit = Opt.eget(Model.race).circuit;
    It.from(circuit.sections).eachIx((r, ir) ->
      It.from(r).eachIx((s, ic) ->
        CircuitPlotter.plotSection(cx, s, ir, ic)
      ));

    wg.add(cv);
  }

  /// DOM ----------------------------------------------------------------------

  public function show (parent: Domo) {
    parent.removeAll().add(wg);
    for (r in Opt.eget(Model.race).runners)
      update(r);
  }

  public function update (runner: Runner) {
     CircuitPlotter.moveRunner(cx, runner);
  }
}
