// Copyright 30-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view.race;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;

/// Title view.
class Clock {
  static final crossImg = Cts.isMobil ? "crossM" : "cross";
  static final clockImg = Cts.isMobil ? "clockM" : "clock";
  static final padding = "height:" + (Cts.isMobil ? "10" : "5") + "px;";

  /// Object -------------------------------------------------------------------

  final wg = Q("div");
  final menuDiv = Q("div");
  final clockDiv = Q("div");

  public function new () {
    wg
      .add(Q("div")
        .style(padding))
      .add(Q("hr"))
      .add(menuDiv)
      .add(clockDiv)
      .add(Q("hr"))
    ;
  }

  // CONTROL -------------------------------------------------------------------

  function showClock () {
    Model.clockShow.setValue(!Model.clockShow.value);
    View.body.race.clock.update();
  }

  // DOM -----------------------------------------------------------------------

  public function show (parent: Domo) {
    parent.removeAll().add(wg);
    update();
  }

  public function update () {
    menuDiv
      .removeAll()
      .style("text-align: right")
      .add(Model.clockShow.value
        ? Ui.link(showClock).add(Ui.img(crossImg))
        : Ui.link(showClock).add(Ui.img(clockImg)))
    ;

    clockDiv
      .removeAll()
      .style("text-align:center")
    ;

    if (Model.clockShow.value) {
      final cl = new dm.Clock();
      if (Cts.isMobil) {
        cl.height *= 3;
        cl.width *= 3;
      }
      clockDiv.add(cl.wg);
    }
  }
}
