// Copyright 30-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.It;
import dm.Opt;
import dm.Menu;
import data.Tour;
import data.TourDesign;
import I18n._;
import I18n._args;

/// Tour selector view.
class TourSel {
  static final rows = Cts.isMobil ? 5 : 3;
  static final cols = Cts.isMobil ? 3 : 5;

  /// Object -------------------------------------------------------------------

  final wg = Q("div");
  var selected: Option<String> = None;

  public function new () {}

  // CONTROL -------------------------------------------------------------------

  function runTour (id: String): Void {
    Model.newRace(id, 0);
    View.body.tourSel.update();
  }

  function selTour (id: String): Void {
    selected = Some(id);
    View.body.tourSel.update();
  }

  function startTour (): Void {
    final id = Opt.eget(selected);
    Model.newRace(id, 0);
    for (r in Model.getRunners()) r.startTour();
    Model.selMenu.setValue(RACE_STOP);
    View.update();
  }

  function modifyTour (): Void {
    final id = Opt.eget(selected);
    final tour = Model.getTour(id);
    Model.mtour.setValue(new TourDesign(
      id, tour.getCircuits(), tour.getLapsArray(), -1
    ));
    Model.selMenu.setValue(TOUR_DESIGN);
    View.update();
  }

  function delTour (): Void {
    final id = Opt.eget(selected);
    if (Ui.confirm(_args(_("Delete tour '%0'"), [id]))) {
      Model.delTour(id);
      selected = None;
      update();
    }
  }

  // DOM -----------------------------------------------------------------------

  public function show (parent: Domo): Void {
    parent.removeAll().add(wg);
    update();
  }

  public function update (): Void {
    final tours = Model.getTours();
    final tourId = switch (selected) {
      case Some(s): selected;
      case None: Opt.bind(Model.race, rc -> Some(rc.tour));

    };
    final tour = Opt.bind(tourId, id -> Some(Model.getTour(id)));

    final lopts = tours.map(e -> return Menu.toption(
      e.id, " ·" + e.id + "· ", () -> selTour(e.id)
    ));
    final ropts = switch(tour) {
      case Some(t): [
        new MenuEntry(
          None,
          Ui.link(startTour).add(Loader.mkIcon(RACE_START))
        ),
        new MenuEntry(None, Loader.mkIcon(SEPARATOR)),
        new MenuEntry(
          None,
          Ui.link(modifyTour).add(Loader.mkIcon(TOUR_DESIGN))
        ),
        new MenuEntry(
          None,
          tours.length < 2
            ? Loader.mkIcon(TRASH, true)
            : Ui.link(delTour).add(Loader.mkIcon(TRASH))
        )
      ];
      case None: [
        new MenuEntry(None, Loader.mkIcon(RACE_START, true)),
        new MenuEntry(None, Loader.mkIcon(SEPARATOR)),
        new MenuEntry(None, Loader.mkIcon(TOUR_DESIGN, true)),
        new MenuEntry(None, Loader.mkIcon(TRASH, true))
      ];
    }
    final menu = new Menu(lopts, ropts, Opt.oget(tourId, ""), true);

    wg
      .removeAll()
      .add(menu.wg)
      .add(Q("table")
        .att("align", "center")
        .adds(
          switch (tour) {
            case Some(t):
              It.range(rows).map(r ->
                Q("tr")
                  .adds(It.range(cols).map(c -> {
                      final ix = r * cols + c;
                      return ix < t.getTotalCircuits()
                        ? Q("td").add(mkCircuit(t, ix))
                        : Q("td")
                      ;
                    }).to())
              ).to();
            case None:
              [ Q("tr")
                  .add(Q("td")
                    .klass("frame")
                    .style("text-align:center")
                    .text(_("No Tour Selected")))
              ];
          }))
    ;
  }

  function mkCircuit (tour: Tour, ix: Int): Domo {
    final circuitTd = Q("td");
    new MiniCircuit(tour.getCircuit(ix)).show(circuitTd);

    return Q("table")
      .klass("frame")
      .add(Q("tr")
        .add(Q("td")
          .att("colspan", "2")
          .add(circuitTd)))
      .add(Q("tr")
        .add(Q("td")
          .att("colspan", "2")
          .add(Q("hr"))))
      .add(Q("tr")
        .add(Q("td")
          .style("text-align: left")
          .text(Std.string(ix + 1)))
        .add(Q("td")
          .style("text-align: right")
          .text("[" + _("Laps") + ": " + Std.string(tour.getLaps(ix)) + "]")))
    ;
  }

}
