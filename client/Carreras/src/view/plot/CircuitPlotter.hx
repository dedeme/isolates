// Copyright 01-Nov-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view.plot;

import js.html.CanvasRenderingContext2D as Cx;
import dm.Domo;
import dm.Opt;
import dm.It;
import data.Section;
import data.Runner;

/// Circuit plotter.
class CircuitPlotter {
  static final normal = Cts.sectionColors[0];
  static final wet = Cts.sectionColors[1];
  static final hilly = Cts.sectionColors[2];
  static final wetHilly = Cts.sectionColors[3];
  static final black = "#002040";
  static final sectionWidth = Cts.gridCols * Cts.cellSide;
  static final sectionHeight = Cts.gridRows * Cts.cellSide;
  public static final circuitWidth = Cts.circuitCols * sectionWidth;
  public static final circuitHeight = Cts.circuitRows * sectionHeight;

  public static function removeRunner (cx: Cx, runner: Runner): Void {
    switch (runner.lastPos) {
      case Some(p):
        final s = Opt.eget(
          Opt.eget(Model.race).circuit.sections[p.srow][p.scol]
        );
        final ix = s.isHilly
          ? s.isWet ? 3 : 2
          : s.isWet ? 1: 0
        ;
        cx.drawImage(
          cast(Loader.getBlank(ix).e, js.html.ImageElement),
          p.scol * sectionWidth + p.gcol * Cts.cellSide,
          p.srow * sectionHeight + p.grow * Cts.cellSide
        );
      case None:
    }
  }

  public static function plotRunner (cx: Cx, runner: Runner, img: Domo): Void {
    switch (runner.pos) {
      case Some(p):
      cx.drawImage(
        cast(img.e, js.html.ImageElement),
        p.scol * sectionWidth + p.gcol * Cts.cellSide,
        p.srow * sectionHeight + p.grow * Cts.cellSide
      );
      case None:
    }
  }

  /// Plots a circuit section.
  ///   cx     : Graphic context.
  ///   section: Section to plot.
  ///   r      : Row of section in circuit.
  ///   c      : Column of section in circuit.
  public static function plotSection (
    cx: Cx, section: Option<Section>, r: Int, c: Int
  ) {
    final s = Opt.get(section);
    if (s == null) return;

    final sx = c * sectionWidth;
    final sy = r * sectionHeight;
    final back = s.isHilly
      ? s.isWet ? wetHilly : hilly
      : s.isWet ? wet: normal
    ;

    cx.fillStyle = back;
    cx.fillRect(
      sx + Cts.cellSide,
      sy + Cts.cellSide,
      Cts.cellSide * (Cts.gridCols - 2),
      Cts.cellSide * (Cts.gridCols - 2)
    );

    if (s.inOpen == UP || s.outOpen == UP) {
      cx.fillStyle = back;
      cx.fillRect(
        sx + Cts.cellSide,
        sy,
        Cts.cellSide * (Cts.gridCols - 2),
        Cts.cellSide
      );
    } else {
      cx.fillStyle = black;
      cx.fillRect(
        sx,
        sy + Cts.cellSide - 2,
        sectionWidth,
        2
      );
    }

    if (s.inOpen == DOWN || s.outOpen == DOWN) {
      cx.fillStyle = back;
      cx.fillRect(
        sx + Cts.cellSide,
        sy + sectionWidth - Cts.cellSide,
        Cts.cellSide * (Cts.gridCols - 2),
        Cts.cellSide
      );
    } else {
      cx.fillStyle = black;
      cx.fillRect(
        sx,
        sy + sectionWidth - Cts.cellSide,
        sectionWidth,
        2
      );
    }

    if (s.inOpen == LEFT || s.outOpen == LEFT) {
      cx.fillStyle = back;
      cx.fillRect(
        sx,
        sy + Cts.cellSide,
        Cts.cellSide,
        Cts.cellSide * (Cts.gridRows - 2)
      );
    } else {
      cx.fillStyle = black;
      cx.fillRect(
        sx + Cts.cellSide - 2,
        sy,
        2,
        sectionHeight
      );
    }

    if (s.inOpen == RIGHT || s.outOpen == RIGHT) {
      cx.fillStyle = back;
      cx.fillRect(
        sx + sectionWidth - Cts.cellSide,
        sy + Cts.cellSide,
        Cts.cellSide,
        Cts.cellSide * (Cts.gridRows - 2)
      );
    } else {
      cx.fillStyle = black;
      cx.fillRect(
        sx + sectionWidth - Cts.cellSide,
        sy,
        2,
        sectionHeight
      );
    }

    if (
      s.inOpen == DOWN && s.outOpen == RIGHT ||
      s.inOpen == RIGHT && s.outOpen == DOWN
    ) {
      cx.fillStyle = Cts.circuitBackground;
      cx.fillRect(sx, sy, Cts.cellSide, Cts.cellSide);

      cx.fillStyle = black;
      cx.fillRect(sx + Cts.cellSide - 2, sy + Cts.cellSide - 2, 2, 2);
      cx.fillRect(
        sx + sectionWidth - Cts.cellSide,
        sy + sectionHeight - Cts.cellSide,
        2, Cts.cellSide
      );
      cx.fillRect(
        sx + sectionWidth - Cts.cellSide,
        sy + sectionHeight - Cts.cellSide,
        Cts.cellSide, 2
      );
    } else if (
      s.inOpen == LEFT && s.outOpen == DOWN ||
      s.inOpen == DOWN && s.outOpen == LEFT
    ) {
      cx.fillStyle = Cts.circuitBackground;
      cx.fillRect(
        sx + sectionWidth - Cts.cellSide, sy,
        Cts.cellSide, Cts.cellSide
      );

      cx.fillStyle = black;
      cx.fillRect(
        sx + sectionWidth - Cts.cellSide,
        sy + Cts.cellSide - 2,
        2, 2
      );
      cx.fillRect(
        sx, sy + sectionHeight - Cts.cellSide,
        Cts.cellSide, 2
      );
      cx.fillRect(
        sx + Cts.cellSide - 2,
        sy + sectionHeight - Cts.cellSide,
        2, Cts.cellSide
      );
    } else if (
      s.inOpen == UP && s.outOpen == RIGHT ||
      s.inOpen == RIGHT && s.outOpen == UP
    ) {
      cx.fillStyle = Cts.circuitBackground;
      cx.fillRect(
        sx, sy + sectionHeight - Cts.cellSide,
        Cts.cellSide, Cts.cellSide
      );

      cx.fillStyle = black;
      cx.fillRect(
        sx + Cts.cellSide - 2,
        sy + sectionHeight - Cts.cellSide,
        2, 2
      );
      cx.fillRect(
        sx + sectionWidth - Cts.cellSide,
        sy,
        2, Cts.cellSide
      );
      cx.fillRect(
        sx + sectionWidth - Cts.cellSide,
        sy + Cts.cellSide - 2,
        Cts.cellSide, 2
      );
    } else if (
      s.inOpen == UP && s.outOpen == LEFT ||
      s.inOpen == LEFT && s.outOpen == UP
    ) {
      cx.fillStyle = Cts.circuitBackground;
      cx.fillRect(
        sx + sectionWidth - Cts.cellSide,
        sy + sectionHeight - Cts.cellSide,
        Cts.cellSide, Cts.cellSide
      );

      cx.fillStyle = black;
      cx.fillRect(
        sx + sectionWidth - Cts.cellSide,
        sy + sectionHeight - Cts.cellSide,
        2, 2
      );
      cx.fillRect(
        sx + Cts.cellSide - 2,
        sy,
        2, Cts.cellSide
      );
      cx.fillRect(
        sx,
        sy + Cts.cellSide - 2,
        Cts.cellSide, 2
      );
    }

    cx.fillStyle = black;
    cx.fillRect(0, 0, circuitWidth, 2);
    cx.fillRect(0, 0, 2, circuitHeight);
    cx.fillRect(0, circuitHeight - 2, circuitWidth, 2);
    cx.fillRect(circuitWidth - 2, 0, 2, circuitHeight);
  }

  /// Plots a circuit section.
  ///   cx: Graphic context.
  ///   r : Runner to plot.
  public static function moveRunner (cx: Cx, runner: Runner) {
    removeRunner(cx, runner);
    plotRunner(cx, runner, Loader.getRunner(runner.id));
  }

}

