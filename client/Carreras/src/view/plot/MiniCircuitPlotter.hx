// Copyright 01-Nov-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view.plot;

import js.html.CanvasRenderingContext2D as Cx;
import dm.Domo;
import dm.Opt;
import dm.It;
import data.Circuit;

/// Minicircuit plotter.
class MiniCircuitPlotter {
  static final side = Cts.cellSideMini;
  static final sectionWidth = Cts.gridCols * side;
  public static final circuitWidth = Cts.circuitCols * sectionWidth;
  static final sectionHeight = Cts.gridRows * side;
  public static final circuitHeight = Cts.circuitRows * sectionHeight;

  public static function plot (
    cx: Cx, circuit: Circuit, back: String, fore: String
  ): Void {
    It.from(circuit.sections).eachIx((secs, sr) -> {
      It.from(secs).eachIx((sec, sc) -> {
        switch (sec) {
          case Some(section):
            It.from(section.grid).eachIx((grid, gr) -> {
              It.from(grid).eachIx((value, gc) -> {
                final color = value == 2 ? back : fore;
                cx.fillStyle = color;
                cx.fillRect(
                  sc * sectionWidth + gc * side,
                  sr * sectionHeight + gr * side,
                  side, side
                );
              });
            });
          case None:
            It.range(Cts.gridRows).each(gr -> {
              It.range(Cts.gridCols).each(gc -> {
                cx.fillStyle = back;
                cx.fillRect(
                  sc * sectionWidth + gc * side,
                  sr * sectionHeight + gr * side,
                  side, side
                );
              });
            });
        }
      });
    });
  }

}
