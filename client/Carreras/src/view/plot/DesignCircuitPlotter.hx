// Copyright 01-Nov-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view.plot;

import js.html.CanvasRenderingContext2D as Cx;
import dm.Domo;
import dm.Opt;
import dm.It;
import data.Coor;
import data.CircuitDesign;

/// Minicircuit plotter.
class DesignCircuitPlotter {
  static final side = Cts.cellSideDesign;
  static final sectionWidth = Cts.gridCols * side;
  public static final circuitWidth = Cts.circuitCols * sectionWidth;
  static final sectionHeight = Cts.gridRows * side;
  public static final circuitHeight = Cts.circuitRows * sectionHeight;

  static function cell (
    cx: Cx, sr: Int, sc: Int, gr: Int, gc: Int, color: String) {
    cx.fillStyle = color;
    cx.fillRect(
      sc * sectionWidth + gc * side,
      sr * sectionHeight + gr * side,
      side, side
    );
  }

  public static function plot (
    cx: Cx, circuit: CircuitDesign,
    back: String, fore: String, forePos: String
  ): Void {
    It.from(circuit.sections).eachIx((secs, sr) -> {
      It.from(secs).eachIx((sec, sc) -> {
        It.range(Cts.gridRows).each(gr ->
          It.range(Cts.gridCols).each(gc ->
            cell(cx, sr, sc, gr, gc, back)
          )
        );

        if (
          (sec.inOpen != NONE && sec.outOpen != NONE) ||
          circuit.pos.eq(new Coor(sr, sc))
        ) {
          final color = circuit.pos.eq(new Coor(sr, sc)) ? forePos : fore;

          It.range(1, Cts.gridRows - 1).each(gr ->
            It.range(1, Cts.gridCols - 1).each(gc ->
              cell(cx, sr, sc, gr, gc, color)
            )
          );

          if (sec.inOpen == UP || sec.outOpen == UP)
            It.range(1, Cts.gridCols - 1).each(c ->
              cell(cx, sr, sc, 0, c, color)
            );
          if (sec.inOpen == DOWN || sec.outOpen == DOWN)
            It.range(1, Cts.gridCols - 1).each(c ->
              cell(cx, sr, sc, Cts.gridCols - 1, c, color)
            );
          if(sec.inOpen == LEFT || sec.outOpen == LEFT)
            It.range(1, Cts.gridCols - 1).each(r ->
              cell(cx, sr, sc, r, 0, color)
            );
          if (sec.inOpen == RIGHT || sec.outOpen == RIGHT)
            It.range(1, Cts.gridCols - 1).each(r ->
              cell(cx, sr, sc, r, Cts.gridRows - 1, color)
            );
        }
      });
    });
  }

}
