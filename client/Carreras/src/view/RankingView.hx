// Copyright 14-Nov-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.It;
import data.Runner;
import model.Runners;
import I18n._;

/// Help view.
class RankingView {
  static function separator (): Domo {
    return Q("td").html("&nbsp");
  }

  /// Object -------------------------------------------------------------------

  final wg = Q("div");

  public function new () {}

  /// DOM ----------------------------------------------------------------------

  public function show (parent: Domo): Void {
    parent.removeAll().add(wg);
    update();
  }

  public function update (): Void {
    final stageWg = mkRank(_("Stage"), Model.getStageRanking());
    final tourWg = mkRank(_("Tour"), Model.getTourRanking());
    final totalWg = mkRank(_("Total"), Model.getTotalRanking());

    if (Cts.isMobil) {
      wg
        .removeAll()
        .add(Q("table")
          .att("align", "center")
          .add(Q("tr")
            .add(Q("td")
              .add(stageWg)))
          .add(Q("tr")
            .add(Q("td")
              .add(tourWg)))
          .add(Q("tr")
            .add(Q("td")
              .add(totalWg))))
      ;
      return;
    }
    wg
      .removeAll()
      .add(Q("table")
        .att("align", "center")
        .add(Q("tr")
          .add(Q("td")
            .add(stageWg))
          .add(separator())
          .add(Q("td")
            .add(tourWg))
          .add(separator())
          .add(Q("td")
            .add(totalWg))))
    ;
  }

  function mkRank (
    title: String,
    ranking: RunnersRanking
  ): Domo {
    final runners = ranking.runners;
    final teams = ranking.teams;
    final cut1 = 4 + Cts.teamsNumber;
    final rtotal = Cts.teamsNumber * Cts.runnersNumber;
    final rrest = rtotal - cut1;
    var cut2 = Std.int(rrest / 2);
    if (rrest % 2 == 1) ++cut2;

    final teamTable = Q("table")
      .klass("main")
      .add(Q("tr")
        .add(Q("td")
          .add(Q("div")
            .klass("header")
            .html("<small>" + _("Teams") + "</small>"))))
      .add(Q("tr")
        .add(Q("td")
          .add(Q("table")
            .klass("main")
            .add(Q("tr")
              .adds(mkTeam(teams[0].team, "", teams[0].time)))
            .adds(It.range(1, Cts.teamsNumber).map(i ->
                Q("tr")
                  .adds(mkTeam(teams[i].team, "a", teams[i].time))
              ).to()))))
    ;

    final table = Q("table")
      .klass("main")
      .add(Q("tr")
        .adds(mkRunner(runners[0].runner, "", runners[0].time))
        .add(Q("td")
          .att("colspan", "3")
          .att("rowspan", "" + cut1)
          .klass("frame")
          .add(teamTable)))
      .adds(It.range(1, cut1).map(i ->
        Q("tr")
          .adds(mkRunner(runners[i].runner, "a", runners[i].time))
        ).to())
      .adds(It.range(cut2).map(i -> {
        final i1 = cut1 + i;
        final i2 = cut1 + i + cut2;
        return Q("tr")
          .adds(mkRunner(runners[i1].runner, "a", runners[i1].time))
          .adds(i2 < rtotal
            ? mkRunner(runners[i2].runner, "a", runners[i2].time)
            : [Q("td"), Q("td"), Q("td")]);
        }).to())
    ;

    return Q("table")
      .att("align", "center")
      .add(Q("tr")
        .add(Q("td")
          .add(Q("div")
            .klass("header")
            .text(title))))
      .add(Q("tr")
        .add(Q("td")
          .klass("frame")
          .add(table)))
    ;
  }

  function mkTeam (team: Int, sep: String, time: Int): Array<Domo> {
    return [
      Q("td")
        //.style("border-left: 1px solid rgb(110,130,150);")
        .add(Q("img")
          .style("vertical-align: middle;")
          .att("src", Loader.getTeam(team).getAtt("src"))),
      Q("td").text(sep),
      Q("td")
        //.style("text-align:right;border-right: 1px solid rgb(110,130,150);")
        .text(Cts.formatTime(time))
    ];
  }

  function mkRunner (runner: Runner, sep: String, time: Int): Array<Domo> {
    return [
      Q("td")
        .style("border-left: 1px solid rgb(110,130,150);")
        .add(Q("img")
          .style("vertical-align: middle;")
          .att("src", Loader.getRunner(runner.id).getAtt("src"))),
      Q("td").text(sep),
      Q("td")
        .style("text-align:right;border-right: 1px solid rgb(110,130,150);")
        .text(Cts.formatTime(time))
    ];
  }
}
