// Copyright 10-Nov-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import I18n._;
import I18n._args;

/// Bye page.
class Bye {
  /// Object -------------------------------------------------------------------

  final wg = Q("div");

  public function new () {}

  /// DOM ----------------------------------------------------------------------

  public function show (parent: Domo) {
    parent.removeAll().add(wg);
    update();
  }

  public function update () {
    final tx = "<a href=''>" + _("here") + "</a>";
    final reload =
      "<p><b>" +
      _args(_("Click %0 to continue."), [tx]) +
      "</b></p>";
    wg
      .removeAll()
      .add(Q("div")
        .klass("header")
        .style("padding-bottom:20px;")
        .text(Cts.appName))
      .add(Q("table")
        .klass("main")
        .add(Q("tr")
          .add(Q("td")
            .add(Q("table")
              .klass("border")
              .att("width", "100%")
              .style("background-color: #f8f8f8; border-collapse: collapse")
              .add(Q("tr")
                .add(Q("td")
                  .style("padding: 10px 0px 0px 10px;")
                  .html(_("<p>Program is finised.<p>") + reload)
                ))))));
  }
}
