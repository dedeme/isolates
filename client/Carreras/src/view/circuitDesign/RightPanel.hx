// Copyright 30-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view.circuitDesign;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.Menu;
import I18n._;
import I18n._args;

/// Right panel of circuit design.
class RightPanel {
  /// Object -------------------------------------------------------------------

  final wg = Q("div");
  final delSpan = Q("span");
  final leftSpan = Q("span");
  final upSpan = Q("span");
  final downSpan = Q("span");
  final rightSpan = Q("span");
  final body = Q("div");

  public function new (radio: Domo) {
    final lopts = [
      new MenuEntry(None, radio),
      new MenuEntry(None, view.Menu.mkSeparator2(Cts.isMobil)),
      new MenuEntry(None, delSpan),
      new MenuEntry(None, view.Menu.mkSeparator2(Cts.isMobil)),
      new MenuEntry(None, leftSpan),
      new MenuEntry(None, view.Menu.mkSeparator()),
      new MenuEntry(None, upSpan),
      new MenuEntry(None, view.Menu.mkSeparator()),
      new MenuEntry(None, downSpan),
      new MenuEntry(None, view.Menu.mkSeparator()),
      new MenuEntry(None, rightSpan)
    ];
    final menu = new Menu(lopts, [], "");

    wg
      .removeAll()
      .add(menu.wg)
      .add(body)
    ;
  }

  // CONTROL -------------------------------------------------------------------

  function sel (ix: Int): Void {
    Model.selMenuCircuitDesign.setValue(SORT);
    Model.selCircuitDesign.setValue(ix);
    View.body.circuit.update();
  }

  function del (): Void {
    final r = Model.delCircuit();
    if (r.length == 0) View.body.circuit.rightPanel.update();
    else Ui.alert(_args(
      _("Circuit can not be deleted.\nIt is used by the following tours:\n%0"),
      [r.join("\n")]
    ));
  }

  function move (inc: Int): Void {
    Model.moveCircuit(inc);
    View.body.circuit.rightPanel.update();
  }

  function add (circuitId: Int): Void {
    final tour = Model.mtour.value;
    tour.addCircuit(circuitId, 5);
    Model.mtour.setValue(tour);
    View.body.tourDesign.leftPanel.update();
  }

  // DOM -----------------------------------------------------------------------

  public function show (parent: Domo) {
    parent.removeAll().add(wg);
    update();
  }

  public function update () {
    final cols = Cts.circuitsDesignCols;

    // Menu

    var delEl = Loader.mkIcon(TRASH, true);
    var leftEl = Loader.mkIcon(GO_LEFT, true);
    var upEl = Loader.mkIcon(GO_UP, true);
    var downEl = Loader.mkIcon(GO_DOWN, true);
    var rightEl = Loader.mkIcon(GO_RIGHT, true);

    if (Model.selMenuCircuitDesign.value == SORT) {
      final ix = Model.selCircuitDesign.value;
      delEl = Ui.link(e -> {
          if (ix == 0) {
            Ui.alert("First circuit can no be deleted");
            return;
          }
          if (!Ui.confirm(_("Remove circuit?"))) return;
          del();
        }).add(Loader.mkIcon(TRASH));

      if (ix != 0) {
        if (ix > 1)
          leftEl = Ui.link(e -> move(-1)).add(Loader.mkIcon(GO_LEFT));
        if (ix > cols)
          upEl = Ui.link(e -> move(-cols)).add(Loader.mkIcon(GO_UP));
        if (ix < Model.circuits.length() - cols)
          downEl = Ui.link(e -> move(cols)).add(Loader.mkIcon(GO_DOWN));
        if (ix < Model.circuits.length() - 1)
          rightEl = Ui.link(e -> move(1)).add(Loader.mkIcon(GO_RIGHT));
      }
    }

    delSpan.removeAll().add(delEl);
    leftSpan.removeAll().add(leftEl);
    upSpan.removeAll().add(upEl);
    downSpan.removeAll().add(downEl);
    rightSpan.removeAll().add(rightEl);

    // Body
    final csn = Model.circuits.length();
    var csc = 0;
    final trs: Array<Domo> = [];
    var tr = Q("tr");
    while (csc < csn) {
      final ix = csc;
      var td = Q("td").style("vertical-align: bottom");
      final cir = new MiniCircuit(Model.circuits.getByIndex(csc), c -> sel(ix));
      td.klass("frame0");
      if (csc == Model.selCircuitDesign.value) {
        cir.sel = true;
        td = Q("td").klass("frame");
      }
      cir.show(td);
      tr.add(td);
      ++csc;
      if (csc % cols == 0) {
        trs.push(tr);
        tr = Q("tr");
      }
    }
    if (csc % cols != 0 ) {
      while (csc % cols != 0) {
        tr.add(Q("td"));
        ++csc;
      }
      trs.push(tr);
    }

    body
      .removeAll()
      .add(Q("table")
        .adds(trs))
    ;
  }
}
