// Copyright 30-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view.circuitDesign;

import js.html.CanvasRenderingContext2D as Cx;
import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.It;
import dm.Opt;
import dm.Menu;
import view.plot.DesignCircuitPlotter;
import I18n._;

/// Left panel of circuit design.
class LeftPanel {
  static final back = "#505535";
  static final fore = "#80a0ff";
  static final forePos = "#ffa080";

  /// Object -------------------------------------------------------------------

  final wg = Q("div");
  final newSpan = Q("span");
  final saveSpan = Q("span");
  final upSpan = Q("span");
  final downSpan = Q("span");
  final leftSpan = Q("span");
  final rightSpan = Q("span");

  var cx: Cx;

  public function new (radio: Domo) {
    final lopts = [
      new MenuEntry(None, radio),
      new MenuEntry(None, view.Menu.mkSeparator2(Cts.isMobil)),
      new MenuEntry(None, newSpan),
      new MenuEntry(None, view.Menu.mkSeparator()),
      new MenuEntry(None, saveSpan),
      new MenuEntry(None, view.Menu.mkSeparator2(Cts.isMobil)),
      new MenuEntry(None, leftSpan),
      new MenuEntry(None, view.Menu.mkSeparator()),
      new MenuEntry(None, upSpan),
      new MenuEntry(None, view.Menu.mkSeparator()),
      new MenuEntry(None, downSpan),
      new MenuEntry(None, view.Menu.mkSeparator()),
      new MenuEntry(None, rightSpan)
    ];
    final menu = new Menu(lopts, [], "");

    final cv = Q("canvas")
      .att("width", DesignCircuitPlotter.circuitWidth)
      .att("height", DesignCircuitPlotter.circuitHeight)
      .style("background: #000000")
      .on(CLICK, e -> changed(false))
    ;
    final cve = cast(cv.e, js.html.CanvasElement);
    cx = cve.getContext2d();

    wg
      .removeAll()
      .add(menu.wg)
      .add(cv)
    ;
  }

  // CONTROL -------------------------------------------------------------------

  function changed (isLeft: Bool): Void {
    Model.selMenuCircuitDesign.setValue(isLeft ? SORT : DESIGN);
    View.body.circuit.update();
  }

  function newCircuit () {
    Model.newCircuitDesign();
    View.body.circuit.leftPanel.update();
  }

  function save () {
    if (!Model.saveCircuitDesign()) {
      Ui.alert("Circuit is duplicated");
      return;
    }
    View.body.circuit.update();
  }

  function upf (): Void {
    Model.circuitDesign.move(UP);
    View.body.circuit.leftPanel.update();
  }

  function downf (): Void {
    Model.circuitDesign.move(DOWN);
    View.body.circuit.leftPanel.update();
  }

  function leftf (): Void {
    Model.circuitDesign.move(LEFT);
    View.body.circuit.leftPanel.update();
  }

  function rightf (): Void {
    Model.circuitDesign.move(RIGHT);
    View.body.circuit.leftPanel.update();
  }

  /// DOM ----------------------------------------------------------------------

  public function show (parent: Domo) {
    parent.removeAll().add(wg);
    update();
  }

  public function update () {
    var newEl = Loader.mkIcon(NEW, true);
    var saveEl = Loader.mkIcon(SAVE, true);
    var leftEl = Loader.mkIcon(GO_LEFT, true);
    var upEl = Loader.mkIcon(GO_UP, true);
    var downEl = Loader.mkIcon(GO_DOWN, true);
    var rightEl = Loader.mkIcon(GO_RIGHT, true);

    final cir = Model.circuitDesign;
    final pos = cir.pos;
    final sec = cir.sections[pos.row][pos.col];

    function active (up: Bool, right: Bool, down: Bool, left: Bool): Void {
      if (Model.selMenuCircuitDesign.value == SORT) return;

      if (up)
        upEl = Ui.link(upf).add(Loader.mkIcon(GO_UP));
      if (right)
        rightEl = Ui.link(rightf).add(Loader.mkIcon(GO_RIGHT));
      if (down)
        downEl = Ui.link(downf).add(Loader.mkIcon(GO_DOWN));
      if (left)
        leftEl = Ui.link(leftf).add(Loader.mkIcon(GO_LEFT));
    }

    function isNone (incRow: Int, incCol: Int): Bool {
      final r = pos.row + incRow;
      if (r >= Cts.circuitRows || r < 0) return false;
      final c = pos.col + incCol;
      if (c >= Cts.circuitCols || c < 0) return false;
      return cir.sections[r][c].inOpen == NONE;
    }

    if (Model.selMenuCircuitDesign.value == DESIGN) {
      newEl = Ui.link(newCircuit).add(Loader.mkIcon(NEW));

      if (cir.finished) {
        saveEl = Ui.link(save).add(Loader.mkIcon(SAVE));
      }

      switch (sec.inOpen) {
        case NONE: active(false, false, false, false);
        case UP:
          active(
            false,
            sec.outOpen == RIGHT || (sec.outOpen == NONE && isNone(0, 1)),
            sec.outOpen == DOWN || (sec.outOpen == NONE && isNone(1, 0)),
            sec.outOpen == LEFT || (sec.outOpen == NONE && isNone(0, -1))
          );
        case RIGHT:
          active(
            sec.outOpen == UP || (sec.outOpen == NONE && isNone(-1, 0)),
            false,
            sec.outOpen == DOWN || (sec.outOpen == NONE && isNone(1, 0)),
            sec.outOpen == LEFT || (sec.outOpen == NONE && isNone(0, -1))
          );
        case DOWN:
          active(
            sec.outOpen == UP || (sec.outOpen == NONE && isNone(-1, 0)),
            sec.outOpen == RIGHT || (sec.outOpen == NONE && isNone(0, 1)),
            false,
            sec.outOpen == LEFT || (sec.outOpen == NONE && isNone(0, -1))
          );
        case LEFT:
          active(
            sec.outOpen == UP || (sec.outOpen == NONE && isNone(-1, 0)),
            sec.outOpen == RIGHT || (sec.outOpen == NONE && isNone(0, 1)),
            sec.outOpen == DOWN || (sec.outOpen == NONE && isNone(1, 0)),
            false
          );
      }
    }

    newSpan.removeAll().add(newEl);
    saveSpan.removeAll().add(saveEl);
    leftSpan.removeAll().add(leftEl);
    upSpan.removeAll().add(upEl);
    downSpan.removeAll().add(downEl);
    rightSpan.removeAll().add(rightEl);

    DesignCircuitPlotter.plot(
      cx, Model.circuitDesign, back, fore, forePos
    );

  }
}
