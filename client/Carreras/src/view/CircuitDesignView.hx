// Copyright 30-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.It;
import dm.ModalBox;
import view.circuitDesign.LeftPanel;
import view.circuitDesign.RightPanel;
import I18n._;

/// Circuit design view.
class CircuitDesignView {

  /// Object -------------------------------------------------------------------

  final wg = Q("div");
  final designDiv = Q("div");
  final sortDiv = Q("div");

  public var leftPanel: LeftPanel;
  public var rightPanel: RightPanel;

  public function new () {
    wg
      .removeAll()
      .add(Q("table")
        .att("align", "center")
        .adds(Cts.isMobil
          ? [
            Q("tr")
              .add(Q("td").add(designDiv)),
            Q("tr")
              .add(Q("td").add(sortDiv)),
          ]
          : [
            Q("tr")
              .add(Q("td")
                .style("vertical-align:top")
                .add(designDiv))
              .add(Q("td")
                .style("vertical-align:top")
                .add(sortDiv))
          ]
        ))
    ;
  }

  // CONTROL -------------------------------------------------------------------

  function changed (isLeft: Bool): Void {
    Model.selMenuCircuitDesign.setValue(isLeft ? SORT : DESIGN);
    View.body.circuit.update();
  }

  // DOM -----------------------------------------------------------------------

  public function show (parent: Domo): Void {
    parent.removeAll().add(wg);
    update();
  }

  public function update (): Void {
    final isLeft = Model.selMenuCircuitDesign.value == DESIGN;
    var lback = "frame";
    var rback = "frame0";
    if (!isLeft) {
      lback = "frame0";
      rback = "frame";
    }
    final lradio = Ui.link(e -> changed(false))
      .add(Loader.mkIcon(isLeft ? RADIO_ON : RADIO_OFF))
    ;
    final rradio = Ui.link(e -> changed(true))
      .add(Loader.mkIcon(isLeft ? RADIO_OFF : RADIO_ON))
    ;
    leftPanel = new LeftPanel(lradio);
    leftPanel.show(designDiv.klass(lback));
    rightPanel = new RightPanel(rradio);
    rightPanel.show(sortDiv.klass(rback));
  }
}
