// Copyright 30-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package view;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.It;
import dm.ModalBox;
import view.tourDesign.LeftPanel;
import view.tourDesign.RightPanel;
import I18n._;

/// Tour design view.
class TourDesignView {

  /// Object -------------------------------------------------------------------

  final wg = Q("div");
  final designDiv = Q("div");
  final circuitsDiv = Q("div");

  public var leftPanel: LeftPanel;

  public function new () {
    wg
      .removeAll()
      .add(Q("table")
        .att("align", "center")
        .adds(Cts.isMobil
          ? [
            Q("tr")
              .add(Q("td").add(designDiv)),
            Q("tr")
              .add(Q("td").add(circuitsDiv)),
          ]
          : [
            Q("tr")
              .add(Q("td")
                .style("vertical-align:top")
                .add(designDiv))
              .add(Q("td")
                .style("vertical-align:top")
                .add(circuitsDiv))
          ]
        ))
    ;
  }

  /// DOM ----------------------------------------------------------------------

  public function show (parent: Domo): Void {
    parent.removeAll().add(wg);
    update();
  }

  public function update (): Void {
    leftPanel = new LeftPanel();
    leftPanel.show(designDiv);
    new RightPanel().show(circuitsDiv);
  }
}
