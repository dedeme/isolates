// Copyright 18-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package model;

import dm.Js;
import dm.Store;
import Cts;

enum MenuOption {
  CIRCUIT; TOUR_DESIGN;
  TOUR_SEL; TOUR_END;
  RACE_START; RACE_STOP; RACE_END; RANKING;
  RESET; RESTORE;
  HELP_CIRCUIT; HELP_TOUR_DESIGN; HELP_TOUR_SEL; HELP_RACE;
  CHANGE_LANG; APP_END;
}

/// Menu option.
class SelMenu {
  static final storeKey = Cts.storeKey + "_menu";

  /// Constructor from Store.
  public static function mk () {
    return new SelMenu(
      switch (Store.get(storeKey)) {
        case Some(value): haxe.Unserializer.run(value);
        case None: RACE_STOP;
      }
    );
  }

  public static function fromJs (js: Js): SelMenu {
    return new SelMenu(haxe.Unserializer.run(js.rs()));
  }

  // Object --------------------------------------------------------------------

  public var value(default, null): MenuOption;

  function new (option: MenuOption) {
    setValue(option);
  }

  public function setValue (option: MenuOption): Void {
    Store.put(storeKey, haxe.Serializer.run(option));
    value = option;
  }

  public function toJs (): Js {
    return Js.ws(haxe.Serializer.run(value));
  }
}
