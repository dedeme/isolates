// Copyright 31-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package model;

import dm.Js;
import dm.Store;
import dm.It;
import dm.B64;
import Cts;
import data.Circuit;

class Circuits {
  static final storeKey = Cts.storeKey + "_circuits";

  /// Constructor from Store.
  public static function mk (): Circuits {
    return new Circuits(
      switch (Store.get(storeKey)) {
        case Some(cs): Js.from(cs).ra().map(e -> Circuit.fromJs(e));
        case None: [Circuit.mkDefault()];
      }
    );
  }

  public static function fromJs (js: Js): Circuits {
    return new Circuits(js.ra().map(e -> Circuit.fromJs(e)));
  }

  // Object --------------------------------------------------------------------

  var value(default, null): Array<Circuit>;

  function new (value: Array<Circuit>) {
    setValue(value);
  }

  /// Returns a shallow copy of 'this.value'.
  public function getValue (): Array<Circuit> {
    return value.copy();
  }

  public function length (): Int {
    return value.length;
  }

  public function setValue (cs: Array<Circuit>): Void {
    Store.put(storeKey, Js.wa(cs.map(e -> e.toJs())).to());
    value = cs;
  }

  public function contains (c: Circuit): Bool {
    return It.from(value).contains(c, (c1, c2) -> c1.eq(c2));
  }

  /// Adds a circuit.
  ///   c: Circuit to add. If already exists, function throw a exception.
  public function add (c: Circuit): Void {
    if (contains(c)) throw new haxe.Exception("Duplicated circuit");
    value.push(c);
    setValue(value);
  }

  /// Removes a circuit.
  ///   ix: Index to delete in value.
  public function del (ix: Int): Void {
    if (value[ix].id == 0) throw new haxe.Exception("Try to delete circuit 0");

    value.splice(ix, 1);
    setValue(value);
  }

  /// Returns the circuit with index 'ix'.
  public function getByIndex (ix: Int): Circuit {
    return value[ix];
  }

  /// Returns the circuit with id 'id'. If 'id' is not found, returns
  /// the circuit with index = 0
  public function getById (id: Int): Circuit {
    var r = value[0];
    for (c in value) {
      if (c.id == id) {
        r = c;
        break;
      }
    }
    return r;
  }

  /// Swaps circuits 'i' and 'j'.
  public function swap (i: Int, j:Int): Void {
    final tmp = value[i];
    value[i] = value[j];
    value[j] = tmp;
    setValue(value);
  }

  public function toJs (): Js {
    return Js.wa(value.map(e -> e.toJs()));
  }

  public function nextId (): Int {
    return It.from(value).reduce(-1, (r, e) -> e.id > r ? e.id : r) + 1;
  }
}
