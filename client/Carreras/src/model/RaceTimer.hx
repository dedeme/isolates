// Copyright 01-Nov-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package model;

import dm.Opt;
import dm.Dt;
import data.Runner;

class RaceTimer {
  var ix = 0;
  var stopV = true;
  var action = () -> {};
  var runners: Array<Runner>;
  var tm: haxe.Timer;

  public function new () {
    runners = Model.getRunners();
  };

  public function start (): Void {
    stopV = false;
    var time = Opt.eget(Model.race).time;
    tm = new haxe.Timer(Cts.runnerMoveTime);
    tm.run = () -> {
      time += Cts.runnerMoveTime;
      setTime(time);
      final i = ix++;
      if (ix == runners.length) {
        tick();
        ix = 0;
      }
      if (stopV) {
        tm.stop();
        action();
        return;
      }
      moveRunner(runners[i]);
      if (stopV) {
        tm.stop();
        action();
        return;
      }
    }
  }

  public function stop (action: () -> Void): Void {
    if (stopV) {
      action();
      return;
    }
    this.action = action;
    stopV = true;
  }

  // CONTROL -------------------------------------------------------------------

  function setTime (time: Int): Void {
    Opt.eget(Model.race).setTime(time);
  }

  function moveRunner (r: Runner): Void {
    if (r.pos == None) return;
    control.MoveRunner.move(r);
    if (r.pos == None) r.raceEnd(Opt.eget(Model.race).time);
  }

  function tick (): Void {
    final race = Opt.eget(Model.race);
    var lap = race.lap;
    var finished = true;
    for (r in race.runners) {
      switch(r.pos) {
        case Some(p):
          if (p.lap > lap) lap = p.lap;
          finished = false;
        case None:
      }
    }

    if (finished) {
      Model.getRaceTimer().stop(() -> {
        race.end();
        race.save();
        Model.saveRunners(true);
        View.body.race.update();
      });
    } else {
      race.setLap(lap);
      race.save();
      Model.saveRunners(false);
      View.body.race.title.update();
    }
  }

}
