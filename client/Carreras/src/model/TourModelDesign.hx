// Copyright 16-Nov-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package model;

import dm.Store;
import dm.Js;
import dm.Opt;
import data.TourDesign;

/// Application tour design.
class TourModelDesign {
  static final storeKey = Cts.storeKey + "_tourDesign";

  /// Constructor from Store.
  public static function mk (): TourModelDesign {
    return new TourModelDesign(
      switch (Store.get(storeKey)) {
        case Some(value): TourDesign.fromJs(Js.from(value));
        case None: TourDesign.mkDefault();
      }
    );
  }

  public static function fromJs (js: Js): TourModelDesign {
    return new TourModelDesign(TourDesign.fromJs(js));
  }

  // Object --------------------------------------------------------------------

  public var value(default, null): TourDesign;

  function new (tour: TourDesign) {
    setValue(tour);
  }

  public function setValue (tour: TourDesign) {
    Store.put(storeKey, tour.toJs().to());
    value = tour;
  }

  /// Reset value to TourDesign.mkDefault()
  public function reset () {
    setValue(TourDesign.mkDefault());
  }

  /// Sets the id of the design
  public function setId (id: String): Void {
    value.setId(id);
    setValue(value);
  }

  /// Sets laps of circuit index 'ix'.
  ///   ix: Index of circuit-laps.
  ///   n : Number of laps.
  public function setLaps (ix: Int, n: Int): Void {
    value.setLaps(ix, n);
    setValue(value);
  }

  /// Sets as selected the circuit-laps with index 'ix' in design
  public function setSel (ix: Int) {
    value.setSel(ix);
    setValue(value);
  }

  /// Removes the selected circuit in design
  public function del () {
    var sel = value.sel;
    value.del(sel);
    final tt = value.getTotalCircuits();
    if (sel >= tt) {
      sel = tt - 1;
    }
    setSel(sel);
  }

  /// Moves the selected circuit to up in design
  public function up () {
    var sel = value.sel;
    var newSel = sel - Cts.circuitsDesignCols;
    value.change(sel, newSel);
    setSel(newSel);
  }

  /// Moves the selected circuit to right in design
  public function right () {
    var sel = value.sel;
    var newSel = sel + 1;
    value.change(sel, newSel);
    setSel(newSel);
  }

  /// Moves the selected circuit to down in design
  public function down () {
    var sel = value.sel;
    var newSel = sel + Cts.circuitsDesignCols;
    value.change(sel, newSel);
    setSel(newSel);
  }

  /// Moves the selected circuit to left in design
  public function left () {
    var sel = value.sel;
    var newSel = sel - 1;
    value.change(sel, newSel);
    setSel(newSel);
  }

  public function toJs (): Js {
    return value.toJs();
  }
}
