// Copyright 18-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package model;

import dm.Js;
import dm.Store;
import Cts;

enum MenuCircuitDesignOption {
  DESIGN; SORT;
}

/// Circuit design menu option.
class SelMenuCircuitDesign {
  static final storeKey = Cts.storeKey + "_menuCircuitDesign";

  /// Constructor from Store.
  public static function mk () {
    return new SelMenuCircuitDesign(
      switch (Store.get(storeKey)) {
        case Some(value): haxe.Unserializer.run(value);
        case None: DESIGN;
      }
    );
  }

  public static function fromJs (js: Js): SelMenuCircuitDesign {
    return new SelMenuCircuitDesign(haxe.Unserializer.run(js.rs()));
  }

  // Object --------------------------------------------------------------------

  public var value(default, null): MenuCircuitDesignOption;

  function new (option: MenuCircuitDesignOption) {
    setValue(option);
  }

  public function setValue (option: MenuCircuitDesignOption): Void {
    Store.put(storeKey, haxe.Serializer.run(option));
    value = option;
  }

  public function toJs (): Js {
    return Js.ws(haxe.Serializer.run(value));
  }

}
