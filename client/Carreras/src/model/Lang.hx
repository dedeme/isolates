// Copyright 30-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package model;

import dm.Js;
import dm.Store;
import Cts;

/// Application lang.
class Lang {
  static final storeKey = Cts.storeKey + "_lang";

  /// Constructor from Store.
  public static function mk (): Lang {
    return new Lang(
      switch (Store.get(storeKey)) {
        case Some(lang): lang == "en" || lang == "es" ? lang : "es";
        case None: "es";
      }
    );
  }

  public static function fromJs (js: Js): Lang {
    return new Lang(js.rs());
  }

  // Object --------------------------------------------------------------------

  /// Application language ("es" or "en").
  public var value(default, null): String;

  function new (lang: String): Void {
    setValue(lang);
  }

  /// Sets language value.
  ///   lang: Must be "es" or "en"
  public function setValue(lang: String): Void {
    Store.put(storeKey, lang);
    value = lang;
  }

  public function toJs (): Js {
    return Js.ws(value);
  }

}
