// Copyright 18-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package model;

import dm.Js;
import dm.Store;
import dm.Opt;

/// Race clock show value.
class ClockShow {
  static final storeKey = Cts.storeKey + "_clockShow";

  /// Constructor from Store.
  public static function mk (): ClockShow {
    return new ClockShow(
      switch (Store.get(storeKey)) {
        case Some(value): value == "true";
        case None: true;
      }
    );
  }

  public static function fromJs (js: Js): ClockShow {
    return new ClockShow(js.rb());
  }

  // Object --------------------------------------------------------------------

  public var value(default, null): Bool;

  function new (value: Bool) {
    setValue(value);
  }

  public function setValue (value: Bool) {
    Store.put(storeKey, value ? "true" : "false");
    this.value = value;
  }

  public function toJs (): Js {
    return Js.wb(value);
  }
}
