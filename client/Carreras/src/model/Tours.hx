// Copyright 31-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package model;

import dm.Js;
import dm.Store;
import Cts;
import data.Tour;

class Tours {
  static final storeKey = Cts.storeKey + "_tours";

  /// Constructor from Store.
  public static function mk (): Array<Tour> {
    return switch (Store.get(storeKey)) {
      case Some(ts): Js.from(ts).ra().map(e -> Tour.fromJs(e));
      case None: [Tour.mkDefault()];
    }
  }

  public static function save (tours: Array<Tour>): Void {
    Store.put(storeKey, Js.wa(tours.map(e -> e.toJs())).to());
  }

  public static function toJs (tours: Array<Tour>): Js {
    return Js.wa(tours.map(e -> e.toJs()));
  }

  public static function fromJs (js: Js): Array<Tour> {
    Store.put(storeKey, js.to());
    return js.ra().map(e -> Tour.fromJs(e));
  }
}
