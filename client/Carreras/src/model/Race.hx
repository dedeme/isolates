// Copyright 31-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package model;

import dm.Js;
import dm.Store;
import dm.Opt;
import Cts;
import data.Circuit;
import data.Coor;
import data.Pos;
import data.Runner;

enum RaceState { PRE_RACE; IN_RACE; POST_RACE; }

/// Race data.
class Race {
  static final storeKey = Cts.storeKey + "_raceData";

  public static function fromJs (js: Js): Race {
    final a = js.ra();
    return new Race(
      a[0].rs(),
      a[1].ri(),
      Circuit.fromJs(a[2]),
      haxe.Unserializer.run(a[3].rs()),
      a[4].ri(),
      a[5].ri(),
      a[6].ri()
    );
  }

  /// Constructor from Store.
  public static function mk (): Option<Race> {
    return switch (Store.get(storeKey)) {
      case Some(race):
        final js = Js.from(race);
        js.isNull() ? None : Some(fromJs(js));
      case None:
        final tour = Model.getTour(Cts.defaultTour);
        final r = new Race(
          Cts.defaultTour, 0,
          tour.getCircuit(0), PRE_RACE,
          tour.getLaps(0), 0, 0
        );
        r.save();
        Some(r);
    }
  }

  public static function reset (): Void {
    Store.put(storeKey, Js.wn().to());
  }

  // Object --------------------------------------------------------------------

  /// Tour name.
  public var tour(default, null): String;
  /// Circuit index in tour.
  public var circuitIx(default, null): Int;
  /// Circuit.
  public var circuit(default, null): Circuit;
  /// State.
  public var state(default, null): RaceState;
  /// Total laps.
  public var totalLaps(default, null): Int;
  /// Current lap number.
  public var lap(default, null): Int;
  /// Real time in milliseconds.
  public var time(default, null): Int;

  /// Rearranged runners
  public var runners(default, null): Array<Runner>;

  public function new (
    tour: String, circuitIx: Int, circuit: Circuit, state: RaceState,
    totalLaps: Int, lap: Int, time: Int
  ) {
    this.tour = tour;
    this.circuitIx = circuitIx;
    this.circuit = circuit;
    this.state = state;
    this.totalLaps = totalLaps;
    this.lap = lap;
    this.time = time;

    runners = Model.getSortedRunners(TOTAL_TIME);

    if (state == PRE_RACE) {
      var pos = new Pos(
        lap,
        Cts.circuitStart.row, Cts.circuitStart.col,
        1, Cts.gridCols - 1
      );
      var pos2 = pos;
      for (r in runners) {
        r.startPosition(pos2);
        final sec = Opt.eget(circuit.sections[pos2.srow][pos2.scol]);
        sec.grid[pos2.grow][pos2.gcol] = 1;
        if (pos2.grow == Cts.gridRows - 2) {
          pos = Pos.left(pos);
          pos2 = pos;
        } else {
          pos2 = Pos.down(pos2);
        }
      }
    }
  }

  public function save (): Void {
    Store.put(storeKey, toJs().to());
  }

  public function start (): Void {
    state = IN_RACE;
    for (r in Model.getRunners()) r.startRace();
  }

  /// Sets lap.
  public function setLap (lap: Int): Void {
    this.lap = lap;
  }

  /// Sets time (milliseconds).
  public function setTime (time: Int): Void {
    this.time = time;
  }

  /// Sets 'state' to POST_RACE.
  public function end (): Void {
    state = POST_RACE;
  }

  public function toJs (): Js {
    return Js.wa([
      Js.ws(tour),
      Js.wi(circuitIx),
      circuit.toJs(),
      Js.ws(haxe.Serializer.run(state)),
      Js.wi(totalLaps),
      Js.wi(lap),
      Js.wi(time)
    ]);
  }

}
