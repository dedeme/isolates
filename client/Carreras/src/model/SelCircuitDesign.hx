// Copyright 11-Nov-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package model;

import dm.Js;
import dm.Store;
import Cts;

/// Selected circuit index in circuit design.
class SelCircuitDesign {
  static final storeKey = Cts.storeKey + "_selCircuitDesign";

  /// Constructor from Store.
  public static function mk () {
    return new SelCircuitDesign(
      switch (Store.get(storeKey)) {
        case Some(ix): Std.parseInt(ix);
        case None: 0;
      }
    );
  }

  public static function fromJs (js: Js): SelCircuitDesign {
    return new SelCircuitDesign(js.ri());
  }

  // Object --------------------------------------------------------------------

  public var value(default, null): Int;

  function new (ix: Int) {
    setValue(ix);
  }

  /// Sets selected circuit index.
  public function setValue (ix: Int): Void {
    Store.put(storeKey, Std.string(ix));
    value = ix;
  }

  public function toJs (): Js {
    return Js.wi(value);
  }
}
