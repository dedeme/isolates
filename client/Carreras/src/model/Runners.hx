// Copyright 01-Nov-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package model;

import dm.Js;
import dm.Store;
import dm.Rnd;
import dm.It;
import Cts;
import data.Runner;

/// Data to make rankings of Runners and teams.
typedef RunnersRanking = {
  runners: Array<{ runner: Runner, time: Int }>,
  teams: Array<{ team: Int, time: Int}>
};

/// Application runners.
class Runners {
  static final storeKey = Cts.storeKey + "_runners";

  static function ranking (
    runners: Array<{ runner: Runner, time: Int }>
  ): {
    runners: Array<{ runner: Runner, time: Int }>,
    teams: Array<{ team: Int, time: Int}>
  } {
    final newRunners = runners.map(e -> {
      runner: e.runner,
      time: e.time
    });
    final teams = It.range(Cts.teamsNumber).map(e -> {
      team: e, time: 0
    }).to();

    newRunners.sort((e1, e2) -> e1.time > e2.time ? 1 : -1);
    final firstTime = newRunners[0].time;
    var isFirst = true;
    for (e in newRunners) {
      teams[e.runner.team].time += e.time;
      if (isFirst) {
        isFirst = false;
      } else {
        e.time -= firstTime;
      }
    }

    teams.sort((e1, e2) -> e1.time > e2.time ? 1 : -1);
    final firstTeamTime = teams[0].time;
    isFirst = true;
    for (e in teams) {
      if (isFirst) {
        isFirst = false;
      } else {
        e.time -= firstTeamTime;
      }
    }

    return {
      runners: newRunners,
      teams: teams
    };
  }

  /// Constructor from Store.
  public static function mk (): Array<Runner> {
    return switch (Store.get(storeKey)) {
      case Some(rs): Js.from(rs).ra().map(e -> Runner.fromJs(e));
      case None:
        final middle = Std.int(Cts.runnersNumber / 2);
        final rs: Array<Runner> = [];
        for (t in 0...Cts.teamsNumber)
          for (r in 0...Cts.runnersNumber)
            rs.push(new Runner(
              t, r,
              r <= middle ? 0 : 1,
              r % 2
            ));
        rs;
    }
  }

  public static function reset (): Void {
    Store.del(storeKey);
  }

  public static function save (rs: Array<Runner>): Void {
    Store.put(storeKey, Js.wa(rs.map(e -> e.toJs())).to());
  }

  public static function toJs (runners: Array<Runner>): Js {
    return Js.wa(runners.map(e -> e.toJs()));
  }

  public static function fromJs (js: Js): Array<Runner> {
    Store.put(storeKey, js.to());
    return js.ra().map(e -> Runner.fromJs(e));
  }

  /// Returns data for stage ranking.
  public static function stageRanking (runners: Array<Runner>): RunnersRanking {
    return ranking(runners.map(e -> {
      runner: e, time: e.raceTime
    }));
  }

  /// Returns data for race ranking.
  public static function tourRanking (runners: Array<Runner>): RunnersRanking {
    return ranking(runners.map(e -> {
      runner: e, time: e.tourTime
    }));
  }

  /// Returns data for race ranking.
  public static function totalRanking (runners: Array<Runner>): RunnersRanking {
    return ranking(runners.map(e -> {
      runner: e, time: e.totalTime
    }));
  }

}
