// Copyright 31-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package data;

import dm.Opt;
import data.SectionDesign;

/// Circuit desing data.
class CircuitDesign {
  static final nRs = Cts.circuitRows;
  static final nCs = Cts.circuitCols;
  static final startCoor = Cts.circuitStart;
  static final endCoor = Cts.circuitEnd;

  // Object --------------------------------------------------------------------

  public var finished(default, null) = false;
  public var pos: Coor;
  public final sections: Array<Array<SectionDesign>> = [];

  public function new () {
    for (r in 0...nRs) {
      final cols: Array<SectionDesign> = [];
      for (c in 0...nCs) {
        cols.push(new SectionDesign());
      }
      sections.push(cols);
    }
    sections[endCoor.row][endCoor.col].setLast();
    sections[startCoor.row][startCoor.col].inOpen = LEFT;
    sections[startCoor.row][startCoor.col].outOpen = RIGHT;
    sections[startCoor.row][startCoor.col + 1].inOpen = LEFT;

    pos = startCoor;
  }

  /// Create the default circuit
  public static function mkDefault (): CircuitDesign {
    final nRs1 = nRs - 1;
    final nCs1 = nCs - 1;

    final rs = new CircuitDesign();
    final ss = rs.sections;
    for (c in 1...nCs1) {
      ss[0][c].inOpen = RIGHT;
      ss[0][c].outOpen = LEFT;
      ss[nRs1][c].inOpen = LEFT;
      ss[nRs1][c].outOpen = RIGHT;
    }
    for (r in 1...nRs1) {
      ss[r][0].inOpen = UP;
      ss[r][0].outOpen = DOWN;
      ss[r][nCs1].inOpen = DOWN;
      ss[r][nCs1].outOpen = UP;
    }

    ss[0][0].inOpen = RIGHT;
    ss[0][0].outOpen = DOWN;

    ss[0][nCs1].inOpen = DOWN;
    ss[0][nCs1].outOpen = LEFT;

    ss[nRs1][0].inOpen = UP;
    ss[nRs1][0].outOpen = RIGHT;

    ss[nRs1][nCs1].inOpen = LEFT;
    ss[nRs1][nCs1].outOpen = UP;

    rs.finished = true;
    rs.pos = endCoor;

    return rs;
  }

  /// Returns true if 'this.pos' can be moved toward 'direction'.
  function canMove (direction: SectionDesignOpen): Bool {
    final sec = sections[pos.row][pos.col];
    switch (direction) {
      case UP:
        if (sec.inOpen == UP) return false;
        if (sec.outOpen != NONE && sec.outOpen != UP) return false;
        if (pos.row == 0) return false;
        final newSec = sections[pos.row - 1][pos.col];
        if (newSec.inOpen != NONE || newSec.inOpen != DOWN) return false;
        return true;
      case DOWN:
        if (sec.inOpen == DOWN) return false;
        if (sec.outOpen != NONE && sec.outOpen != DOWN) return false;
        if (pos.row == nRs - 1) return false;
        final newSec = sections[pos.row + 1][pos.col];
        if (newSec.inOpen != NONE || newSec.inOpen != UP) return false;
        return true;
      case LEFT:
        if (sec.inOpen == LEFT) return false;
        if (sec.outOpen != NONE && sec.outOpen != LEFT) return false;
        if (pos.col == 0) return false;
        final newSec = sections[pos.row][pos.col - 1];
        if (newSec.inOpen != NONE || newSec.inOpen != RIGHT) return false;
        return true;
      case RIGHT:
        if (sec.inOpen == RIGHT) return false;
        if (sec.outOpen != NONE && sec.outOpen != RIGHT) return false;
        if (pos.col == pos.col - 1) return false;
        final newSec = sections[pos.row][pos.col + 1];
        if (newSec.inOpen != NONE || newSec.inOpen != LEFT) return false;
        return true;
      case NONE:
        throw new haxe.Exception("Value NONE is not allowed");
    }
  }

  /// Open 'direction' if is NONE and change 'this.pos'
  public function move (direction: SectionDesignOpen) {
    final sec = sections[pos.row][pos.col];
    switch (direction) {
      case UP:
        if (sec.inOpen == UP)
          throw new haxe.Exception("inOpen is UP");
        if (sec.outOpen != NONE && sec.outOpen != UP)
          throw new haxe.Exception("DOWN direction is closed");
        if (pos.row == 0)
          throw new haxe.Exception("Coor (-1, col) is out of board");
        sec.outOpen = UP;
        final newSec = sections[pos.row - 1][pos.col];
        if (newSec.inOpen != NONE && newSec.inOpen != DOWN)
          throw new haxe.Exception("DOWN of target direction is closed");
        newSec.inOpen = DOWN;
        pos = Coor.up(pos);
      case DOWN:
        if (sec.inOpen == DOWN)
          throw new haxe.Exception("inOpen is DOWN");
        if (sec.outOpen != NONE && sec.outOpen != DOWN)
          throw new haxe.Exception("DOWN direction is closed");
        if (pos.row == nRs - 1)
          throw new haxe.Exception(
            "Coor (" + pos.row + ", col) is out of board"
          );
        sec.outOpen = DOWN;
        final newSec = sections[pos.row + 1][pos.col];
        if (newSec.inOpen != NONE && newSec.inOpen != UP)
          throw new haxe.Exception("UP of target direction is closed");
        newSec.inOpen = UP;
        pos = Coor.down(pos);
      case LEFT:
        if (sec.inOpen == LEFT)
          throw new haxe.Exception("inOpen is LEFT");
        if (sec.outOpen != NONE && sec.outOpen != LEFT)
          throw new haxe.Exception("LEFT direction is closed");
        if (pos.col == 0)
          throw new haxe.Exception("Coor (row, -1) is out of board");
        sec.outOpen = LEFT;
        final newSec = sections[pos.row][pos.col - 1];
        if (newSec.inOpen != NONE && newSec.inOpen != RIGHT)
          throw new haxe.Exception("RIGHT of target direction is closed");
        newSec.inOpen = RIGHT;
        pos = Coor.left(pos);
      case RIGHT:
        if (sec.inOpen == RIGHT)
          throw new haxe.Exception("inOpen is RIGHT");
        if (sec.outOpen != NONE && sec.outOpen != RIGHT)
          throw new haxe.Exception("RIGHT direction is closed");
        if (pos.col == pos.col - 1)
          throw new haxe.Exception(
            "Coor (row, " + pos.col + ") is out of board"
          );
        sec.outOpen = RIGHT;
        final newSec = sections[pos.row][pos.col + 1];
        if (newSec.inOpen != NONE && newSec.inOpen != LEFT)
          throw new haxe.Exception("LEFT of target direction is closed");
        newSec.inOpen = LEFT;
        pos = Coor.right(pos);
      case NONE:
        throw new haxe.Exception("Value NONE is not allowed");
    }
    if (pos.eq(endCoor)) {
      finished = true;
    }
  }
}
