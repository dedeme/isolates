// Copyright 20-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package data;

import dm.Js;

/// Cordinates of (Lap - Section - Grid) in a circuit.
class Pos {
  /// Returns a new position moving 'p' up.
  /// Limits are not checked.
  public static function up (p: Pos): Pos {
    if (p.grow == 0) {
      return new Pos(p.lap, p.srow - 1, p.scol, Cts.gridRows - 1, p.gcol);
    }
    return new Pos(p.lap, p.srow, p.scol, p.grow - 1, p.gcol);
  }

  /// Returns a new position moving 'p' down.
  /// Limits are not checked.
  public static function down (p: Pos): Pos {
    if (p.grow == Cts.gridRows - 1) {
      return new Pos(p.lap, p.srow + 1, p.scol, 0, p.gcol);
    }
    return new Pos(p.lap, p.srow, p.scol, p.grow + 1, p.gcol);
  }

  /// Returns a new position moving 'p' left.
  /// Limits are not checked.
  public static function left (p: Pos): Pos {
    if (p.gcol == 0) {
      if (p.section.eq(Cts.circuitStart)) {
        return new Pos(p.lap - 1, p.srow, p.scol - 1, p.grow, Cts.gridCols - 1);
      }
      return new Pos(p.lap, p.srow, p.scol - 1, p.grow, Cts.gridCols - 1);
    }
    return new Pos(p.lap, p.srow, p.scol, p.grow, p.gcol - 1);
  }

  /// Returns a new position moving 'p' rigth.
  /// Limits are not checked.
  public static function right (p: Pos): Pos {
    if (p.gcol == Cts.gridCols - 1) {
      if (p.section.eq(Cts.circuitEnd)) {
        return new Pos(p.lap + 1, p.srow, p.scol + 1, p.grow, 0);
      }
      return new Pos(p.lap, p.srow, p.scol + 1, p.grow, 0);
    }
    return new Pos(p.lap, p.srow, p.scol, p.grow, p.gcol + 1);
  }

  public static function fromJs (js: Js): Pos {
    final a = js.ra();
    return new Pos(a[0].ri(), a[1].ri(), a[2].ri(), a[3].ri(), a[4].ri());
  }

  // Object --------------------------------------------------------------------

  /// Lap
  public var lap(default, null): Int;
  /// Section row. From up to down.
  public var srow(default, null): Int;
  /// Section column. From left to right.
  public var scol(default, null): Int;
  /// Grid row. From up to down.
  public var grow(default, null): Int;
  /// Grid column. From left to right.
  public var gcol(default, null): Int;
  /// Section coordinate.
  public var section(get, never): Coor;
  function get_section () return new Coor(srow, scol);
  /// Grid coordinate.
  public var grid(get, never): Coor;
  function get_grid () return new Coor(grow, gcol);

  /// Constructor.
  ///   lap : Circuit lap.
  ///   srow: Section row. From up to down.
  ///   scol: Section column. From left to right.
  ///   grow: Grid row. From up to down.
  ///   gcol: Grid column. From left to right.
  public function new (lap: Int, srow: Int, scol: Int, grow: Int, gcol: Int) {
    this.lap = lap;
    this.srow = srow;
    this.scol = scol;
    this.grow = grow;
    this.gcol = gcol;
  }

  public function toJs (): Js {
    return Js.wa([
      Js.wi(lap),
      Js.wi(srow),
      Js.wi(scol),
      Js.wi(grow),
      Js.wi(gcol)
    ]);
  }
}
