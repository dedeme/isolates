// Copyright 31-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package data;

import dm.Js;

/// Races tour.
class Tour {

  public static function fromJs (js: Js): Tour {
    final a = js.ra();
    return new Tour(
      a[0].rs(),
      a[1].ra().map(e -> e.ri()),
      a[2].ra().map(e -> e.ri())
    );
  }

  public static function mkDefault (): Tour {
    return new Tour(Cts.defaultTour, [0], [5]);
  }

  // Object --------------------------------------------------------------------

  public var id(default, null): String;
  var circuits(default, null): Array<Int>;
  // Lap[i] matchs circuits[i].
  var laps(default, null): Array<Int>;

  /// Constructor.
  ///   id: Identifier.
  ///   circuits: Index of circuits in Circuits.read().
  ///   laps: Laps number of circuits (Lap[i] must match circuits[i]).
  public function new (id: String, circuits: Array<Int>, laps: Array<Int>) {
    this.id = id;
    this.circuits = circuits;
    this.laps = laps;
    if (circuits.length != laps.length) {
      throw new haxe.Exception(
        "Circuits length (" + circuits.length + ") !=  " +
        "laps length (" + laps.length + ")"
      );
    }
  }

  public function getCircuits (): Array<Int> {
    return circuits.copy();
  }

  public function getLapsArray (): Array<Int> {
    return laps.copy();
  }

  /// Returns the circuit with index 'ix' tuned.
  public function getCircuit (ix: Int): Circuit {
    return Model.circuits.getById(circuits[ix]).tune();
  }

  /// Returns the laps number of circuit 'ix'.
  public function getLaps (ix: Int): Int {
    return laps[ix];
  }

  public function getTotalCircuits (): Int {
    return circuits.length;
  }

  public function toJs (): Js {
    return Js.wa([
      Js.ws(id),
      Js.wa(circuits.map(e -> Js.wi(e))),
      Js.wa(laps.map(e -> Js.wi(e)))
    ]);
  }
}
