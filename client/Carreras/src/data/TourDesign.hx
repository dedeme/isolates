// Copyright 31-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package data;

import dm.Js;

/// Races design tour.
class TourDesign {

  public static function fromJs (js: Js): TourDesign {
    final a = js.ra();
    return new TourDesign(
      a[0].rs(),
      a[1].ra().map(e -> e.ri()),
      a[2].ra().map(e -> e.ri()),
      a[3].ri()
    );
  }

  public static function mkDefault (): TourDesign {
    return new TourDesign("", [], [], -1);
  }

  // Object --------------------------------------------------------------------

  public var id(default, null): String;
  var circuits(default, null): Array<Int>;
  // Lap[i] matchs circuits[i].
  var laps(default, null): Array<Int>;
  /// Index of selected circuit or -1
  public var sel(default, null): Int;

  /// Constructor.
  public function new (
    id: String, circuits: Array<Int>, laps: Array<Int>, sel: Int
  ) {
    this.id = id;
    this.circuits = circuits;
    this.laps = laps;
    this.sel = sel;
  }

  /// Sets tour id.
  public function setId (id: String) {
    this.id = id;
  }

  /// Adds a circuit.
  public function addCircuit (circuitId: Int, laps: Int): Void {
    circuits.push(circuitId);
    this.laps.push(laps);
  }

  /// Returns the circuit with index 'ix'.
  public function getCircuit (ix: Int): Circuit {
    return Model.circuits.getById(circuits[ix]);
  }

  /// Returns the laps number of circuit 'ix'.
  public function getLaps (ix: Int): Int {
    return laps[ix];
  }

  public function getTotalCircuits (): Int {
    return circuits.length;
  }

  /// Removes the circuit with index 'ix'
  public function del (ix: Int): Void {
    circuits.splice(ix, 1);
    laps.splice(ix, 1);
  }

  /// Change circuit-laps position.
  public function change (i1: Int, i2: Int): Void {
    final ctmp = circuits[i1];
    circuits[i1] = circuits[i2];
    circuits[i2] = ctmp;
    final ltmp = laps[i1];
    laps[i1] = laps[i2];
    laps[i2] = ltmp;
  }

  /// Sets laps of circuit 'ix'.
  ///   ix   : Index of circuit-laps.
  ///   value: Number of laps.
  public function setLaps (ix: Int, value: Int) {
    laps[ix] = value;
  }

  /// Set the circuit selected
  public function setSel (ix: Int) {
    if (ix < 0 || ix >= circuits.length) {
      sel = circuits.length > 0 ? 0 : -1;
      return;
    }
    sel = ix;
  }

  public function toTour (): Tour {
    return new Tour(id, circuits, laps);
  }

  public function toJs (): Js {
    return Js.wa([
      Js.ws(id),
      Js.wa(circuits.map(e -> Js.wi(e))),
      Js.wa(laps.map(e -> Js.wi(e))),
      Js.wi(sel)
    ]);
  }
}
