// Copyright 31-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package data;

import dm.Js;

enum SectionDesignOpen { NONE; UP; DOWN; LEFT; RIGHT; }

/// Circuit desing section data.
class SectionDesign {
  var inOpenv = NONE;
  // In value.
  public var inOpen(get, set): SectionDesignOpen;
  function get_inOpen (): SectionDesignOpen return inOpenv;
  function set_inOpen (value: SectionDesignOpen) return inOpenv = value;

  var outOpenv = NONE;
  /// Out value.
  public var outOpen(get, set): SectionDesignOpen;
  function get_outOpen (): SectionDesignOpen return outOpenv;
  function set_outOpen (value: SectionDesignOpen) {
    if (inOpen == NONE) throw new haxe.Exception("inOpen is NONE");
    if (inOpen == value)
      throw new haxe.Exception("inOpen == value (" + value + ")");
    return outOpenv = value;
  }

  /// Creates a section with 'opens' equals to 'NONE'.
  public function new () {}

  /// Configures 'this' as last section in circuit.
  public function setLast (): Void {
    inOpenv = NONE;
    outOpenv = RIGHT;
  }

  static function openToJs (value: SectionDesignOpen): Js {
    return switch(value) {
      case NONE: Js.wi(0);
      case UP: Js.wi(1);
      case DOWN: Js.wi(2);
      case LEFT: Js.wi(3);
      case RIGHT: Js.wi(4);
    }
  }

  static function openFromJs (js: Js): SectionDesignOpen {
    return switch (js.ri()) {
      case 1: UP;
      case 2: DOWN;
      case 3: LEFT;
      case 4: RIGHT;
      default: NONE;
    }
  }

  public function toJs (): Js {
    return Js.wa([
      openToJs(inOpenv),
      openToJs(outOpenv)
    ]);
  }

  public static function fromJs (js: Js): SectionDesign {
    final a = js.ra();
    final rs = new SectionDesign();
    rs.inOpen = openFromJs(a[0]);
    rs.outOpen = openFromJs(a[1]);
    return rs;
  }
}
