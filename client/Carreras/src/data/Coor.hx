// Copyright 31-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package data;

import dm.Js;

/// Cordinate of row-col.
class Coor {

  /// Returns a new Coordenate moving 'c' up.
  /// Limits are not checked.
  public static function up (c: Coor): Coor {
    return new Coor(c.row - 1, c.col);
  }

  /// Returns a new Coordenate moving 'c' down.
  /// Limits are not checked.
  public static function down (c: Coor): Coor {
    return new Coor(c.row + 1, c.col);
  }

  /// Returns a new Coordenate moving 'c' left.
  /// Limits are not checked.
  public static function left (c: Coor): Coor {
    return new Coor(c.row, c.col - 1);
  }

  /// Returns a new Coordenate moving 'c' right.
  /// Limits are not checked.
  public static function right (c: Coor): Coor {
    return new Coor(c.row, c.col + 1);
  }

  // Object --------------------------------------------------------------------

  /// From up to down.
  public var row(default, null): Int;
  /// From left to right.
  public var col(default, null): Int;

  /// Constructor.
  ///   row: From up to down.
  ///   col: From left to right.
  public function new (row: Int, col: Int) {
    this.row = row;
    this.col = col;
  }

  /// Operator ==
  public function eq (other: Coor): Bool {
    return row == other.row && col == other.col;
  }

  public function toJs (): Js {
    return Js.wa([
      Js.wi(row),
      Js.wi(col)
    ]);
  }

  public static function fromJs (js: Js): Coor {
    final a = js.ra();
    return new Coor(a[0].ri(), a[1].ri());
  }
}
