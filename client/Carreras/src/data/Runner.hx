// Copyright 01-Nov-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package data;

import dm.Js;
import dm.Opt;

/// Runner data.
class Runner {

  public static function fromJs (js: Js): Runner {
    final a = js.ra();
    final rs = new Runner(a[0].ri(), a[1].ri(), a[2].rf(), a[3].rf());
    // Last pos is 'None' because the runner has not been placed yet.
    rs.lastPos = None;
    rs.pos = a[4].isNull() ? None : Some(Pos.fromJs(a[4]));
    rs.totalTime = a[5].ri();
    rs.tourTime = a[6].ri();
    rs.raceTime = a[7].ri();
    return rs;
  }

  // Object --------------------------------------------------------------------

  /// Team (Index of team).
  public var team(default, null): Int;
  /// Identifier (runer index inside its team)
  public var teamIx(default, null): Int;
  /// Identifier equals to 'team * Cts.teamsNumber + teamIx'
  /// It is its index in Model.runners and Loader.runners.
  public var id(get, never): Int;
  function get_id () return team * Cts.runnersNumber + teamIx;

  /// Between 0 and 1
  public var hillyAbility(default, null): Float;
  /// Between 0 and 1
  public var wetAbility(default, null): Float;

  /// Runner last position in circuit (lap-section-grid).
  public var lastPos(default, null): Option<Pos> = None;
  /// Runner position in circuit (lap-section-grid).
  public var pos(default, null): Option<Pos> = None;
  // Total time in milliseconds.
  public var totalTime(default, null) = 0;
  // Time of the current tour in milliseconds
  public var tourTime(default, null) = 0;
  // Time of the current race in milliseconds
  public var raceTime(default, null) = 0;

  /// Constructor.
  ///   hillyAbility: Between 0 and 1
  ///   wetAbility  : Between 0 and 1
  public function new (
    team: Int, teamIx: Int, hillyAbility: Float, wetAbility: Float
  ) {
    this.team = team;
    this.teamIx = teamIx;
    this.hillyAbility = hillyAbility;
    this.wetAbility = wetAbility;
  }

  /// Returns runner time in milliseconds.
  public function time (type: Cts.TimeType): Int {
    return switch(type) {
      case RACE_TIME: raceTime;
      case TOUR_TIME: tourTime;
      case TOTAL_TIME: totalTime;
    }
  }

  /// Prepares 'this' to start a race:
  ///   - Sets initial 'pos' and 'lastPos'.
  public function startPosition (pos: Pos) {
    this.lastPos = None;
    this.pos = Some(pos);
  }

  /// Sets 'pos' and 'lastPos'.
  public function move (pos: Option<Pos>) {
    lastPos = this.pos;
    this.pos = pos;
  }

  /// Updates values of time using 't' (milliseconds).
  /// pos and lastPos are set to None.
  public function raceEnd (t: Int) {
    totalTime += t;
    tourTime += t;
    raceTime = t;
  }

  /// Set the time value at the begining of a new race.
  public function startRace () {
    raceTime = 0;
  }

  /// Set the time value at the begining of a new tour.
  public function startTour () {
    tourTime = raceTime = 0;
  }

  public function toJs (): Js {
    return Js.wa([
      Js.wi(team),
      Js.wi(teamIx),
      Js.wf(hillyAbility),
      Js.wf(wetAbility),
      // Last post is not saved.
      switch (pos) { case Some(p): p.toJs(); case None: Js.wn(); } ,
      Js.wi(totalTime),
      Js.wi(tourTime),
      Js.wi(raceTime)
    ]);
  }
}
