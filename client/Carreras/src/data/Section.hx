// Copyright 31-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package data;

import dm.Js;
import dm.It;
import data.SectionDesign;

enum SectionOpen { UP; DOWN; LEFT; RIGHT; }

class Section {
  static final gRs = Cts.gridRows;
  static final gCs = Cts.gridCols;

  static function openFromJs (js: Js): SectionOpen {
    return switch (js.ri()) {
      case 0: UP;
      case 1: DOWN;
      case 2: LEFT;
      default: RIGHT;
    }
  }

  public static function fromJs (js: Js): Section {
    final a = js.ra();
    final grid = a[0].ra().map(r ->
      r.ra().map(c -> c.ri())
    );

    return new Section(
      grid, a[1].rb(), a[2].rb(), openFromJs(a[3]), openFromJs(a[4])
    );
  }

  /// Constructor.
  public static function mk (sec: SectionDesign): Section {
    final grid = It.range(gRs).map(r ->
      It.range(gCs).map(c -> 0).to()
    ).to();

    function openFrom(open: SectionDesignOpen): SectionOpen {
      return switch (open) {
        case UP: UP;
        case DOWN: DOWN;
        case LEFT: LEFT;
        case RIGHT: RIGHT;
        case NONE: throw new haxe.Exception("Open is NONE");
      }
    }
    function closeUp () for (c in 0...gCs) grid[0][c] = 2;
    function closeDown () for (c in 0...gCs) grid[6][c] = 2;
    function closeLeft () for (r in 0...gRs) grid[r][0] = 2;
    function closeRight () for (r in 0...gRs) grid[r][6] = 2;

    final inOpen = openFrom(sec.inOpen);
    final outOpen = openFrom(sec.outOpen);

    switch (inOpen) {
      case UP:
        switch (outOpen) {
          case UP: throw new haxe.Exception("Section UP-UP");
          case DOWN: closeLeft(); closeRight();
          case LEFT: closeDown(); closeRight(); grid[0][0] = 2;
          case RIGHT: closeDown(); closeLeft(); grid[0][gCs - 1] = 2;
        }
      case DOWN:
        switch (outOpen) {
          case UP: closeLeft(); closeRight();
          case DOWN: throw new haxe.Exception("Section DOWN-DOWN");
          case LEFT: closeUp(); closeRight(); grid[gRs - 1][0] = 2;
          case RIGHT: closeUp(); closeLeft(); grid[gRs - 1][gCs- 1] = 2;
        }
      case LEFT:
        switch (outOpen) {
          case UP: closeDown(); closeRight(); grid[0][0] = 2;
          case DOWN: closeUp(); closeRight(); grid[gRs - 1][0] = 2;
          case LEFT: throw new haxe.Exception("Section LEFT-LEFT");
          case RIGHT: closeUp(); closeDown();
        }
      case RIGHT:
        switch (outOpen) {
          case UP: closeDown(); closeLeft(); grid[0][gCs - 1] = 2;
          case DOWN: closeUp(); closeLeft(); grid[gRs - 1][gCs- 1] = 2;
          case LEFT: closeUp(); closeDown();
          case RIGHT: throw new haxe.Exception("Section RIGHT-RIGHT");
        }
    }

    return new Section(grid, false, false, inOpen, outOpen);
  }

  // Object --------------------------------------------------------------------

  /// Matrix Cts.gridRows x Cts.gridCols of
  ///   {0: empty, 1: with runner, 2: with wall}
  /// Initialized without runners.
  public final grid: Array<Array<Int>> = [];
  /// Initialized to false.
  public var isHilly = false;
  /// Initialized to false.
  public var isWet = false;
  public var inOpen(default, null): SectionOpen;
  public var outOpen(default, null): SectionOpen;

  function new (
    grid: Array<Array<Int>>, isHilly: Bool, isWet: Bool,
    inOpen: SectionOpen, outOpen: SectionOpen
  ) {
    this.grid = grid;
    this.isHilly = isHilly;
    this.isWet = isWet;
    this.inOpen = inOpen;
    this.outOpen = outOpen;
  }

  /// Two sections are equals if their grid are equals.
  public function eq (other: Section) {
    return It.from(grid).eq(
      It.from(other.grid),
      (r1, r2) -> It.from(r1).eq(It.from(r2), (c1, c2) -> c1 == c2)
    );
  }

  function openToJs (value: SectionOpen): Js {
    return switch (value) {
      case UP: Js.wi(0);
      case DOWN: Js.wi(1);
      case LEFT: Js.wi(2);
      case RIGHT: Js.wi(3);
    }
  }

  /// Returns a new section.
  public function clone (): Section {
    return new Section(
      grid.map(r -> r.map(c -> c)), isHilly, isWet, inOpen, outOpen
    );
  }

  public function toJs (): Js {
    final gridJs = Js.wa(grid.map(r ->
      Js.wa(r.map(c -> Js.wi(c)))
    ));
    return Js.wa([
      gridJs,
      Js.wb(isHilly),
      Js.wb(isWet),
      openToJs(inOpen),
      openToJs(outOpen)
    ]);
  }

}
