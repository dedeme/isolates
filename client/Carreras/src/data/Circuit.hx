// Copyright 31-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package data;

import dm.Js;
import dm.Opt;
import dm.Rnd;
import dm.It;

/// Circuit data.
class Circuit {

  public static function fromJs (js: Js): Circuit {
    final a = js.ra();
    return new Circuit(
      a[0].ri(),
      a[1].ra().map(r -> r.ra().map(c -> c.isNull()
        ? None
        : Some(Section.fromJs(c))
      ))
    );
  }

  /// Constructor.
  /// 'Circuit' must be a finished circuit.
  public static function mk (id: Int, cir: CircuitDesign): Circuit {
    if (!cir.finished) {
      throw new haxe.Exception("Circuit is not finished");
    }
    final sections = cir.sections.map(r ->
      r.map(c -> c.inOpen == NONE ? None : Some(Section.mk(c)))
    );
    return new Circuit(id, sections);
  }

  /// Creates the default circuit.
  public static function mkDefault (): Circuit {
    return mk(0, CircuitDesign.mkDefault());
  }

  // Object --------------------------------------------------------------------

  public var id (default, null): Int;
  public final sections: Array<Array<Option<Section>>> = [];

  function new (id: Int, sections: Array<Array<Option<Section>>>) {
    this.id = id;
    this.sections = sections;
  }

  /// Two circuit are equals if have the same sections.
  public function eq (other: Circuit) {
    return It.from(sections).eq(
      It.from(other.sections),
      (ss1, ss2) -> It.from(ss1).eq(It.from(ss2),
        (e1, e2) -> switch(e1) {
          case Some(v1):
            switch(e2) {
              case Some(v2): v1.eq(v2);
              case None: false;
            };
          case None: e2 == None;
        }
      )
    );
  }

  public function grid (pos: Pos): Int {
    return Opt.eget(sections[pos.srow][pos.scol]).grid[pos.grow][pos.gcol];
  }

  public function tune (): Circuit {
    final r = new Circuit(
      id,
      sections.map(r -> r.map(c -> Opt.bind(c, s -> Some(s.clone()))))
    );
    var ix = 0;
    var pos = Cts.circuitStart;
    do {
      switch (Opt.eget(r.sections[pos.row][pos.col]).outOpen) {
        case UP: pos = Coor.up(pos);
        case DOWN: pos = Coor.down(pos);
        case LEFT: pos = Coor.left(pos);
        case RIGHT: pos = Coor.right(pos);
      }
      ++ix;
    } while (!pos.eq(Cts.circuitStart));
    final len = ix;
    final len2 = Std.int(len / 2);

    ix = 0;
    var pos = Cts.circuitStart;
    final wetValue = Rnd.i(2) == 1;
    final hillyValue = Rnd.i(2) == 1;
    final hillyStart = Rnd.i(len2);
    final hillyEnd = hillyStart + len2;
    do {
      final sec = Opt.eget(r.sections[pos.row][pos.col]);
      sec.isWet = ix < len2 ? wetValue : !wetValue;
      sec.isHilly = ix >= hillyStart && ix < hillyEnd
        ? hillyValue
        : !hillyValue
      ;
      switch (sec.outOpen) {
        case UP: pos = Coor.up(pos);
        case DOWN: pos = Coor.down(pos);
        case LEFT: pos = Coor.left(pos);
        case RIGHT: pos = Coor.right(pos);
      }
      ++ix;
    } while (!pos.eq(Cts.circuitStart));

    return r;
  }

  public function toJs (): Js {
    return Js.wa([
      Js.wi(id),
      Js.wa(sections.map(r -> Js.wa(r.map(c -> switch(c) {
        case Some(sec): sec.toJs();
        case None: Js.wn();
      }))))
    ]);
  }
}
