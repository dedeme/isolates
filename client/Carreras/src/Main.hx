// Copyright 29-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Application entry.
class Main {
  /// Application entry.
  static public function main (): Void {
    //dm.Store.clear(Cts.storeKey);
    Loader.load(() -> {
      Model.init();
      View.init();
      View.update();
    });
  }
}
