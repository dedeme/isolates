// Copyright 16-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

import dm.Ui.Q;
import dm.Client;
import I18n._;
import I18n._args;
import data.Coor;

enum TimeType { RACE_TIME; TOUR_TIME; TOTAL_TIME; }

/// Constants and global functions.
class Cts {
  /// Application name.
  public static final appName = "Carreras";
  /// Application version.
  public static final appVersion = "202010";
  /// Store key.
  public static final storeKey = appName + "__data";
  /// Default tour name.
  public static final defaultTour = _("Basic");
  /// Page foot.
  public static final foot = Q("table")
    .klass("main")
    .add(Q("tr")
      .add(Q("td")
        .add(Q("hr"))))
    .add(Q("tr")
      .add(Q("td")
        .style("text-align: right;color:#808080;font-size:x-small;")
        .html('- © ºDeme. ${appName} (${appVersion}) -')))
  ;
  /// 'true' if device is a mobil phone in portrait position.
  public static final isMobil = isMobilf();

  /// Number of rows of a circuit grid.
  public static final circuitRows = 6;
  /// Number of columns of a circuit grid.
  public static final circuitCols = 8;
  /// Coordinate of circuit start.
  public static final circuitStart = new Coor(5, 3);
  /// Coordinate of circuit end.
  public static final circuitEnd = new Coor(5, 2);
  /// Number of rows of a circuit section grid.
  public static final gridRows = 7;
  /// Number of columns of a circuit section grid.
  public static final gridCols = 7;
  /// Width (and height) of a circuit grid.
  public static var cellSide(default, null) = isMobil ? 22 : 14;
  /// Width (and height) of a Minicircuit grid.
  public static var cellSideMini(default, null) = isMobil ? 4 : 2;
  /// Width (and height) of a design circuit grid.
  public static var cellSideDesign(default, null) = isMobil ? 10 : 5;
  /// Number of cols of circuit deisign panel.
  public static final circuitsDesignCols = 3;
  /// Number of teams.
  public static final teamsNumber = 5;
  /// Number of runners for team.
  public static final runnersNumber = 9;
  /// Circuit background color.
  public static final circuitBackground = "#406045";
  /// Circuit section colors:
  ///   0 -> Normal, 1 -> wet, 2 -> hilly, 3 -> wetHilly
  public static final sectionColors = [
    "#ffffff", "#e0e0ff", "#ffe0e0", "#e0e0e0"
  ];
  /// Delay when moving runners (milliseconds).
  public static final runnerMoveTime = 11;


  static function isMobilf (): Bool {
    return
      ~/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.match(
        js.Browser.navigator.userAgent
      ) &&
      js.Browser.window.screen.orientation != null &&
      (
        js.Browser.window.screen.orientation.type == PORTRAIT_PRIMARY ||
        js.Browser.window.screen.orientation.type == PORTRAIT_SECONDARY
      )
    ;
  }

  /// Format time
  public static function formatTime (millis: Int) {
    var mll = Std.string(millis % 1000);
    if (mll.length == 1) mll = "00" + mll;
    else if (mll.length == 2) mll = "0" + mll;

    var rs = Std.int(millis / 1000);
    var s = Std.string(rs % 60);
    if (s.length == 1) s = "0" + s;
    var rs = Std.int(rs / 60);
    var m = Std.string(rs % 60);
    if (m.length == 1) m = "0" + m;
    var h = Std.string(Std.int(rs / 60));
    if (h.length == 1) h = "0" + h;
    return h + ":" + m + ":" + s + "," + mll;
  }

  /// Screen dimension.
  public static function screen (): {width: Int, height: Int} {
    return {
      width: js.Browser.window.screen.width,
      height: js.Browser.window.screen.height
    };
  }
}
