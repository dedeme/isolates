// Copyright 10-Nov-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package control;

import dm.Opt;
import data.Runner;
import data.Coor;
import data.Pos;

/// Move a runner in circuit.
class MoveRunner {
  /// Move a runner in circuit.
  public static function move (r: Runner): Void {
    final race = Opt.eget(Model.race);
    final ct = race.circuit;
    final pos = Opt.eget(r.pos);

    final section = Opt.eget(ct.sections[pos.srow][pos.scol]);
    var pond = 0.5;
    if (section.isWet) {
      pond -= .15 * r.wetAbility;
    } else {
      pond -= .15 * (1 - r.wetAbility);
    }
    if (section.isHilly) {
      pond -= .15 * r.hillyAbility;
    } else {
      pond -= .15 * (1 - r.hillyAbility);
    }

    if (Math.random() > pond) return;

    final newSecCoor = switch (section.outOpen) {
      case UP: new Coor(pos.srow - 1, pos.scol);
      case DOWN: new Coor(pos.srow + 1, pos.scol);
      case LEFT: new Coor(pos.srow, pos.scol - 1);
      case RIGHT: new Coor(pos.srow, pos.scol + 1);
    }
    final newSection = Opt.eget(ct.sections[newSecCoor.row][newSecCoor.col]);

    function gof (p1: Pos, p2: Pos, ?p3: Pos): Option<Pos> {
      return (ct.grid(p1) == 0) ? Some(p1)
        : ct.grid(p2) == 0 ? Some(p2)
        : p3 != null && ct.grid(p3) == 0 ? Some(p3)
        : None
      ;
    }
    var goPos = switch (section.outOpen) {
      case UP:
        switch (newSection.outOpen) {
          case UP: gof(
            Pos.up(pos), Pos.up(Pos.left(pos)), Pos.up(Pos.right(pos))
          );
          case DOWN: throw new haxe.Exception("DOWN is not allowed");
          case LEFT: pos.gcol > 0
            ? gof(Pos.up(Pos.left(pos)), Pos.up(pos), Pos.up(Pos.right(pos)))
            : gof(Pos.up(pos), Pos.up(Pos.right(pos)))
          ;
          case RIGHT: pos.gcol < Cts.gridCols - 1
            ? gof(Pos.up(Pos.right(pos)), Pos.up(pos), Pos.up(Pos.left(pos)))
            : gof(Pos.up(pos), Pos.up(Pos.left(pos)))
          ;
        };
      case DOWN:
        switch (newSection.outOpen) {
          case UP: throw new haxe.Exception("UP is not allowed");
          case DOWN: gof(
            Pos.down(pos), Pos.down(Pos.right(pos)), Pos.down(Pos.left(pos))
          );
          case LEFT: pos.gcol > 0
            ? gof(Pos.down(Pos.left(pos)), Pos.down(pos), Pos.down(Pos.right(pos)))
            : gof(Pos.down(pos), Pos.down(Pos.right(pos)))
          ;
          case RIGHT: pos.gcol < Cts.gridCols - 1
            ? gof(Pos.down(Pos.right(pos)), Pos.down(pos), Pos.down(Pos.left(pos)))
            : gof(Pos.down(pos), Pos.down(Pos.left(pos)))
          ;
        };
      case LEFT:
        switch (newSection.outOpen) {
          case UP: pos.grow > 0
            ? gof(Pos.left(Pos.up(pos)), Pos.left(pos), Pos.left(Pos.down(pos)))
            : gof(Pos.left(pos), Pos.left(Pos.down(pos)))
          ;
          case DOWN: pos.grow < Cts.gridRows - 1
            ? gof(Pos.left(Pos.down(pos)), Pos.left(pos), Pos.left(Pos.up(pos)))
            : gof(Pos.left(pos), Pos.left(Pos.up(pos)))
          ;
          case LEFT: gof(
            Pos.left(pos), Pos.left(Pos.up(pos)), Pos.left(Pos.down(pos))
          );
          case RIGHT: throw new haxe.Exception("RIGHT is not allowed");
        };
      case RIGHT:
        switch (newSection.outOpen) {
          case UP: pos.grow > 0
            ? gof(Pos.right(Pos.up(pos)), Pos.right(pos), Pos.right(Pos.down(pos)))
            : gof(Pos.right(pos), Pos.right(Pos.down(pos)))
          ;
          case DOWN: pos.grow < Cts.gridRows - 1
            ? gof(Pos.right(Pos.down(pos)), Pos.right(pos), Pos.right(Pos.up(pos)))
            : gof(Pos.right(pos), Pos.right(Pos.up(pos)))
          ;
          case LEFT: throw new haxe.Exception("LEFT is not allowed");
          case RIGHT: gof(
            Pos.right(pos), Pos.right(Pos.down(pos)), Pos.right(Pos.up(pos))
          );
        };
    }

    switch (goPos) {
      case Some(p):
        switch (section.outOpen) {
          case UP | DOWN: if (p.scol != pos.scol) return;
          case LEFT | RIGHT: if (p.srow != pos.srow) return;
        }

        section.grid[pos.grow][pos.gcol] = 0;
        if (p.lap == race.totalLaps) {
          goPos = None;
        } else {
          Opt.eget(ct.sections[p.srow][p.scol]).grid[p.grow][p.gcol] = 1;
        }
      case None: return;
    }
    r.move(goPos);
    View.body.race.circuit.update(r);
  }
}
