// Copyright 30-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.ModalBox;
import I18n._;
import model.SelMenu;
import view.Bye;
import view.Menu;
import view.Body;
import view.RestoreBox;

class View {

  static final menuWg = Q("td");
  static final bodyWg = Q("td");
  static final restoreBoxDiv = Q("div");

  static var restoreBox: ModalBox;

  public static var menu(default, null): Menu;
  public static var body(default, null): Body;


  /// Initializer.
  public static function init () {
    restoreBox = new ModalBox(restoreBoxDiv, false);

    Q("@head")
      .add(Q("title")
        .text(_("Races")))
      .add(Q("meta")
        .att("name", "lang")
        .att("content", Model.lang.value))
      .add(Q("link")
        .att("rel", "stylesheet")
        .att("href", Cts.isMobil ? "stylesM.css" : "styles.css")
        .att("type", "text/css"))
    ;

    Q("@body")
      .removeAll()
      .add(Q("table")
        .klass("main")
        .add(Q("tr").add(menuWg))
        .add(Q("tr").add(bodyWg))
        .add(Q("tr").add(Q("td").add(Cts.foot))))
      .add(restoreBox.wg)
    ;

    if (js.Browser.window.screen.orientation != null) {
      js.Browser.window.screen.orientation.onchange =
        e -> js.Browser.location.reload();
    }
  }

  public static function update (): Void {
    if (Model.isAppFinished) {
      menuWg.removeAll();
      new Bye().show(bodyWg);
      return;
    }
    menu = new Menu();
    menu.show(menuWg);
    body = new Body();
    body.show(bodyWg);

    if (Model.isRestore) {
      new RestoreBox().show(restoreBoxDiv);
      restoreBox.show(true);
    } else {
      restoreBox.show(false);
    }
  }

}
