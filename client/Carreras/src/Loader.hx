// Copyright 01-Nov-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

import dm.Domo;
import dm.Ui;

enum Icon {
  SEPARATOR; CIRCUIT_DESIGN; TOUR_DESIGN; TOUR_SEL; END;
  RACE_START; STOP; LOGO; END_RACE;
  RESET; SAVE; SELECT; RANKING; HELP; EN_FLAG; ES_FLAG; CROSS;
  GO_UP; GO_DOWN; GO_LEFT; GO_RIGHT;
  NEW; TRASH; RADIO_ON; RADIO_OFF; CLOCK;
}

/// Files loader.
class Loader {
  static final numbers: Array<Domo> = [];
  static final runners: Array<Domo> = [];
  static final teams: Array<Domo> = [];
  static final blanks:Array<Domo> = [];
  public static function load (fn: () -> Void): Void {
    final prefix = Cts.isMobil ? "runnersM/" : "runners/";
    var ix = 0;

    for (i in 0...10) {
      ++ix;
      numbers.push(
        Ui.img("nm/" + Std.string(i))
          .on(LOAD, e -> --ix)
      );
    }

    ++ix;
    numbers.push(
      Ui.img("nm/d")
        .on(LOAD, e -> --ix)
    );

    for (i in 0...4) {
      ++ix;
      blanks.push(
        Ui.img(prefix + "blank" + Cts.sectionColors[i].substring(1))
          .on(LOAD, e -> --ix)
      );
    }

    for (t in 0...Cts.teamsNumber) {
      ++ix;
      teams.push(Ui.img(prefix + t + "00").on(LOAD, e -> --ix));
      for (r in 0...Cts.runnersNumber) {
        ++ix;
        runners.push(Ui.img(prefix + t + r).on(LOAD, e -> --ix));
      }
    }

    final tm = new haxe.Timer(50);
    var c = 0;
    tm.run = () -> {
      if (ix == 0 || c > 100) {
        tm.stop();
        if (c > 100) Ui.alert(ix + " images are not been loaded");
        fn();
      } else {
        ++c;
      }
    }
  }

  public static function getNumber (ix: Int): Domo {
    return numbers[ix];
  }

  public static function getTeam (ix: Int): Domo {
    return teams[ix];
  }

  public static function getRunner (ix: Int): Domo {
    return runners[ix];
  }

  /// ix can be:
  ///   0 -> Normal, 1 -> wet, 2 -> hilly, 3 -> wetHilly
  public static function getBlank (ix: Int): Domo {
    return blanks[ix];
  }

  // Normal icons --------------------------------------------------------------

  /// Returns an DOM image.
  ///   id: Identifier.
  ///   light: 'true' if image is light (default 'false').
  public static function mkIcon (id: Icon, light = false): Domo {
    var path = switch (id) {
      case SEPARATOR: "separator";
      case CIRCUIT_DESIGN: "circuitDesign";
      case TOUR_DESIGN: "tourDesign";
      case TOUR_SEL: "tourSel";
      case END: "end";
      case RACE_START: "raceStart";
      case STOP: "stop";
      case LOGO: "logo";
      case END_RACE: "endRace";
      case RESET: "reset";
      case SAVE: "save";
      case SELECT: "select";
      case RANKING: "ranking";
      case HELP: "help";
      case EN_FLAG: "enFlag";
      case ES_FLAG: "esFlag";
      case CROSS: "cross";
      case GO_UP: "goUp";
      case GO_DOWN: "goDown";
      case GO_LEFT: "goLeft";
      case GO_RIGHT: "goRight";
      case NEW: "new";
      case TRASH: "trash";
      case RADIO_ON: "radioOn";
      case RADIO_OFF: "radioOff";
      case CLOCK: "clock";
    }
    if (Cts.isMobil) path = path + "M";

    return light
      ? Ui.lightImg(path)
      : Ui.img(path)
    ;
  }

}
