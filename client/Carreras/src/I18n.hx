// Generate by hxi18n. Don't modify

/// I18n management.
class I18n {

  static var enDic = [
    "'%0' is not a valid backup" => "'%0' is not a valid backup",
    "<p>Program is finised.<p>" => "<p>Program is finised.<p>",
    "Backup" => "Backup",
    "Basic" => "Basic",
    "Cancel" => "Cancel",
    "Circuit Design" => "Circuit Design",
    "Click %0 to continue." => "Click %0 to continue.",
    "Continue" => "Continue",
    "Continue Stage" => "Continue Stage",
    "End Stage" => "End Stage",
    "End Tour" => "End Tour",
    "End application" => "End application",
    "Fail reading file" => "Fail reading file",
    "Help" => "Help",
    "Lap" => "Lap",
    "Laps" => "Laps",
    "Races" => "Races",
    "Ranking" => "Ranking",
    "Remove circuit?" => "Remove circuit?",
    "Reset" => "Reset",
    "Restore" => "Restore",
    "Restore '%0'?" => "Restore '%0'?",
    "Restore Backup" => "Restore Backup",
    "Runners data will be reset.\nContinue?" => "Runners data will be reset.\nContinue?",
    "Select file" => "Select file",
    "Stage" => "Stage",
    "Stage Stop" => "Stage Stop",
    "Stage finished" => "Stage finished",
    "Start Stage" => "Start Stage",
    "Teams" => "Teams",
    "Time of this stage will not be added to results.\nContinue?" => "Time of this stage will not be added to results.\nContinue?",
    "Time of this tour will not be added to results.\nContinue?" => "Time of this tour will not be added to results.\nContinue?",
    "Total" => "Total",
    "Tour" => "Tour",
    "Tour Design" => "Tour Design",
    "Tour Selection" => "Tour Selection",
    "Tour finished" => "Tour finished",
    "here" => "here"
  ];

  static var esDic = [
    "'%0' is not a valid backup" => "'%0' no es una válida copia de seguridad.",
    "<p>Program is finised.<p>" => "<p>El programa ha finalizado.</p>",
    "Backup" => "Copia de seguridad",
    "Basic" => "Básico",
    "Cancel" => "Cancelar",
    "Circuit Design" => "Diseño de circuitos",
    "Click %0 to continue." => "Hacer click %0 para continuar.",
    "Continue" => "Continuar",
    "Continue Stage" => "Continuar la etapa",
    "End Stage" => "Finalizar la etapa",
    "End Tour" => "Finalizar el tour",
    "End application" => "Fin de la aplicación",
    "Fail reading file" => "Fallo leyendo el archivo.",
    "Help" => "Ayuda",
    "Lap" => "Vuelta",
    "Laps" => "Vueltas",
    "Races" => "Carreras",
    "Ranking" => "Clasificación",
    "Remove circuit?" => "¿Eliminar el circuito?",
    "Reset" => "Reiniciar",
    "Restore" => "Restaurar",
    "Restore '%0'?" => "¿Restaurar '%0'?",
    "Restore Backup" => "Restaurar copia de seguridad",
    "Runners data will be reset.\nContinue?" => "Los datos de los corredores serán reiniciados.\n¿Continuar?",
    "Select file" => "Selecionar archivo",
    "Stage" => "Etapa",
    "Stage Stop" => "Detener la etapa",
    "Stage finished" => "Fin de la etapa",
    "Start Stage" => "Empezar la etapa",
    "Teams" => "Equipos",
    "Time of this stage will not be added to results.\nContinue?" => "El tiempo de esta etapa no será añadido a los resultados.\n¿Continuar?",
    "Time of this tour will not be added to results.\nContinue?" => "El tiempo de este tour no será añadido a los resultados.\n¿Continuar?",
    "Total" => "Total",
    "Tour" => "Tour",
    "Tour Design" => "Diseño de tour",
    "Tour Selection" => "Selección de tour",
    "Tour finished" => "Fin del tour",
    "here" => "aquí"
  ];

  public static var lang(default, null) = "es";

  public static function en (): Void {
    lang = "en";
  }

  public static function es (): Void {
    lang = "es";
  }

  public static function _(key: String): String {
    final dic = lang == "en" ? enDic : esDic;
    return dic.exists(key) ? dic[key] : key;
  }

  public static function _args(key: String, args: Array<String>): String {
    var bf = "";
    final v = _(key);
    var isCode = false;
    for (i in 0...v.length) {
      final ch = v.charAt(i);
      if (isCode) {
        if (ch >= "0" && ch <= "9") bf += args[Std.parseInt(ch)];
        else bf += "%" + ch;
        isCode = false;
      } else if (ch == "%") {
        isCode = true;
      } else {
        bf += ch;
      }
    }
    return bf;
  }

}
