// Copyright 29-Oct-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

import dm.Js;
import dm.It;
import dm.B64;
import dm.Opt;
import model.Lang;
import model.SelMenu;
import model.SelMenuCircuitDesign;
import model.SelCircuitDesign;
import model.ClockShow;
import model.Race;
import model.Tours;
import model.TourModelDesign;
import model.Circuits;
import model.Runners;
import model.RaceTimer;
import data.Tour;
import data.TourDesign;
import data.CircuitDesign;
import data.Circuit;
import data.Section;
import data.Runner;
import data.Coor;

/// Application model
class Model {
  public static var lang(default, null): Lang;
  public static var selMenu(default, null): SelMenu;
  public static var selMenuCircuitDesign(default, null): SelMenuCircuitDesign;
  public static var selCircuitDesign: SelCircuitDesign;
  public static var circuitDesign: CircuitDesign;
  public static var clockShow: ClockShow;
  static var tours: Array<Tour>;
  public static var mtour: TourModelDesign;
  public static var circuits: Circuits;
  static var runners: Array<Runner>;
  public static var race: Option<Race>;

  // NO SERIALIZED

  static var raceTimer: RaceTimer;

  public static var isAppFinished = false;
  public static var isRestore = false;

  /// Model initialization.
  public static function init () {
    lang = Lang.mk();
    selMenu = SelMenu.mk();
    selMenuCircuitDesign = SelMenuCircuitDesign.mk();
    selCircuitDesign = SelCircuitDesign.mk();
    circuitDesign = new CircuitDesign();
    clockShow = ClockShow.mk();
    runners = Runners.mk();
    tours = Tours.mk();
    mtour = TourModelDesign.mk();
    circuits = Circuits.mk();
    race = Race.mk();

    raceTimer = new RaceTimer();

    if (lang.value == "es") I18n.es();
    else I18n.en();
    if (selMenu.value == RACE_START) {
      raceTimer.start();
    }
  }

  // Serializes Model.
  static function toJs (): Js {
    trace("Here2");
    return Js.wa([
      lang.toJs(), // 0
      selMenu.toJs(), // 1
      selMenuCircuitDesign.toJs(), // 2
      selCircuitDesign.toJs(), // 3
      clockShow.toJs(), // 4
      Runners.toJs(runners), // 5
      Tours.toJs(tours), // 6
      mtour.toJs(), // 7
      circuits.toJs(), // 8
      switch (race) { case None: Js.wn(); case Some(rc): rc.toJs(); }, // 9
    ]);
  }

  // Serializes Model in a B64-JSON string
  public static function serialize (): String {
    return B64.encode(toJs().to());
  }

  /// Restores Model. Return 'false' if serial is not a valid
  /// JSON-serialization.
  /// After call this functions, application must be realoaded.
  public static function initFromJs (serial: String): Bool {
/*    final back = toJs();
    try {*/
      final a = Js.from(serial).ra();
      lang = Lang.fromJs(a[0]);
      selMenu = SelMenu.fromJs(a[1]);
      selMenuCircuitDesign = SelMenuCircuitDesign.fromJs(a[2]);
      selCircuitDesign = SelCircuitDesign.fromJs(a[3]);
      clockShow = ClockShow.fromJs(a[4]);
      runners = Runners.fromJs(a[5]);
      tours = Tours.fromJs(a[6]);
      mtour = TourModelDesign.fromJs(a[7]);
      circuits = Circuits.fromJs(a[8]);
      if (a[9].isNull()) {
        race = None;
      } else {
        final rc = Race.fromJs(a[9]);
        race = Some(rc);
        rc.save();
      }
      return true;
/*    } catch (e) {
      initFromJs(back.to());
      return false;
    }*/
  }

  // Circuits ------------------------------------------------------------------

  /// Creates a new current circuit design.
  public static function newCircuitDesign (): Void {
    circuitDesign = new CircuitDesign();
  }

  /// Adds and save a new circuit. If circuit is duplicated, return false.
  public static function saveCircuitDesign (): Bool {
    if (!circuitDesign.finished) {
      throw new haxe.Exception("Cicuit design is not finished");
    }

    final newCircuit = Circuit.mk(circuits.nextId(), circuitDesign);
    if (circuits.contains(newCircuit)) {
      return false;
    }

    circuits.add(newCircuit);
    return true;
  }

  /// Removes the selected circuit and returns [].
  /// If circuit is being used in some tours, function do nothing and returns
  /// their names.
  public static function delCircuit (): Array<String> {
    final c = circuits.getValue()[selCircuitDesign.value];
    final r = tours
      .filter(tour ->
        It.range(tour.getTotalCircuits())
          .map(ix -> tour.getCircuit(ix))
          .contains(c, (a, b) -> a.eq(b))
      ).map(tour -> tour.id);

    if (r.length == 0) {
      final ix = selCircuitDesign.value;
      circuits.del(ix);

      if (circuits.length() >= ix) {
        selCircuitDesign.setValue(ix - 1);
      }
    }

    return r;
  }

  /// Moves the selected circuit 'inc' positions.
  public static function moveCircuit (inc: Int) {
    final oldPos = selCircuitDesign.value;
    final newPos = oldPos + inc;
    circuits.swap(oldPos, newPos);
    selCircuitDesign.setValue(newPos);
  }

  // Race ----------------------------------------------------------------------

  /// Create a new race
  public static function newRace (tourId: String, circuitIx: Int): Void {
    final tour = getTour(tourId);
    final rc = new Race(
      tourId, circuitIx,
      tour.getCircuit(circuitIx), PRE_RACE,
      tour.getLaps(circuitIx), 0, 0
    );
    race = Some(rc);
    rc.save();
  }

  // Tours ---------------------------------------------------------------------

  /// Returns tours list.
  public static function getTours (): Array<Tour> {
    return tours.copy();
  }

  /// Returns 'true' if id of the design tour already exist.
  public static function duplicateTour (): Bool {
    final tour = mtour.value.toTour();
    return It.from(tours).contains(tour, (a, b) -> a.id == b.id);
  }

  /// Returns a tour.
  public static function getTour (id: String): Tour {
    for (r in tours)
      if (r.id == id) return r;
    throw new haxe.Exception("Tour " + id + " not found");
  }

  /// Removes a tour.
  /// If there is less than 2 tours, throw an exception.
  public static function delTour (id: String): Void {
    if (tours.length < 2)
      throw new haxe.Exception("There is less than 2 tours");

    tours = tours.filter(e -> e.id != id);
    Tours.save(tours);
  }

  /// Save the design tour. If there is other tour with the same name,
  /// function throw an exception.
  public static function saveTour (): Void {
    if (duplicateTour()) throw new haxe.Exception("Duplicate tour");

    final newTour = mtour.value.toTour();
    tours.push(newTour);
    Tours.save(tours);
  }

  /// Finalize the current tour
  public static function finishTour (): Void {
    race = None;
    Race.reset();
  }

  /// Returns true if race.state == POST_RACE and
  /// race.circuitIx == currentTour.getTotalCircuits() - 1.
  public static function tourFinished (): Bool {
    final rc = Opt.eget(race);
    final len = getTour(rc.tour).getTotalCircuits();
    return rc.state == POST_RACE && rc.circuitIx == len - 1;
  }

  // Runners -------------------------------------------------------------------

  /// Returns a shallow copy of application runners.
  public static function getRunners (): Array<Runner> {
    return runners.copy();
  }

  /// Returns a rearranged shallow copy of application runners.
  public static function getSortedRunners (type: Cts.TimeType): Array<Runner> {
    final rs = runners.copy();
    dm.Rnd.shuffle(rs);
    rs.sort((r1, r2) -> r1.time(type) > r2.time(type) ? 1 : -1);
    return rs;
  }

  /// Save runners.
  ///   raceFinished: It it is 'true', the method 'runner.endRace' is called
  ///                 for every runner.
  public static function saveRunners (raceFinished: Bool): Void {
    Runners.save(runners);
  }

  /// Resets runners data.
  /// After call this functions, application must be realoaded.
  public static function reset (): Void {
    Runners.reset();
    Race.reset();
  }

  /// Returns stage ranking data.
  public static function getStageRanking (): RunnersRanking {
    return Runners.stageRanking(runners);
  }

  /// Returns tour ranking data.
  public static function getTourRanking (): RunnersRanking {
    return Runners.tourRanking(runners);
  }

  /// Returns total ranking data.
  public static function getTotalRanking (): RunnersRanking {
    return Runners.totalRanking(runners);
  }

  /// Returns the race timer
  public static function getRaceTimer (): RaceTimer {
    return raceTimer;
  }

}
