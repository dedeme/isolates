// Copyright 26-Mar-2021 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

import I18n._;
import data.Module;

/// Application entry.
class Main {
  /// Application entry.
  static public function main (): Void {
    Module.list(mods -> {
      Model.init(mods);
      View.init();
      View.update();
    });
  }
}
