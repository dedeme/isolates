// Copyright 26-Mar-2021 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

using StringTools;

import dm.Js;
import dm.B64;
import dm.Store;
import dm.Opt;
import dm.It;
import data.Snippet;

/// Application model
class Model {
  public static final appName = "HxSnippets";
  public static final appVersion = "2020103";
  static final storePrefix = "HxSnippets_";

  public static var modules(default, null): Map<String, Array<Snippet>>;
  public static var module(default, null) = "";
  public static var snippet(default, null) = "";

  /// Model initialization.
  public static function init (modules: Map<String, Array<Snippet>>) {
    Model.modules = modules;

    module = Opt.oget(Store.get(storePrefix + "module"), "File");
    if (!modules.exists(module)) {
      module = modules.keys().next();
    }
    snippet = Opt.oget(Store.get(storePrefix + "snippet"), "");
    if (
      snippet != "" &&
      It.from(modules[module]).indexf(s -> snippetName(s) == snippet) == -1
    ) {
      snippet = "";
    }
  }

  public static function snippetName (s: Snippet): String {
    final id = s.id;
    final parIx = id.indexOf("(");
    return parIx == -1
      ? id.trim()
      : id.substring(0, parIx).trim()
    ;
  }

  public static function selMod (id: String) {
    Store.put(storePrefix + "module", id);
    module = Opt.oget(Store.get(storePrefix + "module"), "File");
    if (!modules.exists(module)) {
      module = modules.keys().next();
    }

    Store.put(storePrefix + "snippet", "");
    snippet = "";

    View.update();
  }

  public static function selSnip (id: String) {
    Store.put(storePrefix + "snippet", id);
    snippet = id;

    View.update();
  }

}
