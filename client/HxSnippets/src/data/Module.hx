// Copyright 28-Mar-2021 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package data;

using StringTools;
import dm.It;
import dm.Tp;
import dm.Ui;

class Module {
  static final moduleList = [
    "File", "Io"
  ];
  static final ID = 0;
  static final CODE = 1;
  static final DOC = 2;

  static function readSnippets (tx: String): Array<Snippet> {
    final snippets: Array<Snippet> = [];

    final lines = tx.split("\n");
    var state = ID;
    var id = "";
    var code = "";
    var doc = "";
    for (line in lines) {
      final l = line.trim();
      switch (state) {
        case ID: {
          if (id == "") {
            if (l == "") continue;
            id = line.rtrim();
            continue;
          }
          if (l == "") {
            state = CODE;
            continue;
          }
          id += "\n" + line.rtrim();
          continue;
        }
        case CODE: {
          if (code == "") {
            if (l == "") continue;
            code = line.rtrim();
            continue;
          }
          if (l == "") {
            state = DOC;
            continue;
          }
          code += "\n" + line.rtrim();
          continue;
        }
        case DOC: {
          if (doc == "") {
            if (l == "") continue;
            doc = line.rtrim();
            continue;
          }
          if (l == "---") {
            snippets.push(new Snippet(id, code, doc));
            id = code = doc = "";
            state = ID;
            continue;
          }
          doc += "\n" + line.rtrim();
          continue;
        }
      }
    }

    if (id != "") {
      snippets.push(new Snippet(id, code, doc));
    }

    return snippets;
  }

  public static function list (
    action: Map<String, Array<Snippet>> -> Void
  ): Void {
    final mods: Map<String, Array<Snippet>> = [];

    It.from(moduleList).eachSync(
      (name, fn) ->
        Ui.upload("modules/" + name + ".txt", tx -> {
          fn(new Tp(name, readSnippets(tx)));
        }),
      nameSnippets -> mods[nameSnippets.e1] = nameSnippets.e2,
      () -> action(mods),
      (e) -> throw (new haxe.Exception('Fail reading module\n${e}'))
    );
  }

}
