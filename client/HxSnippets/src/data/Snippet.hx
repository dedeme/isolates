// Copyright 28-Mar-2021 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

package data;

@:build(dm.Mac.record([
  "id: String",
  "code: String",
  "doc: String",
  ], false))
class Snippet {}
