// Copyright 22-Dic-2020 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

using StringTools;

import dm.Domo;
import dm.Ui;
import dm.Ui.Q;
import dm.It;
import dm.Str;
import dm.Vmenu;
import data.Snippet;
import I18n._;

class View {
  static final leftTd = Q("td").style("width:5px;vertical-align:top");
  static final indexTd = Q("td").style("vertical-align:top");
  static final bodyTd = Q("td").style("vertical-align:top");

  /// Initializer.
  public static function init () {
    Q("@body")
      .removeAll()
      .add(Q("div")
        .klass("head")
        .text("Haxe Snippets"))
      .add(Q("div")
        .add(Q("hr")))
      .add(Q("table")
        .klass("main")
        .add(Q("tr")
          .add(leftTd)
          .add(Q("td")
            .style("vertical-align:top")
            .add(Q("table")
              .klass("main")
              .add(Q("tr")
                .add(indexTd))
              .add(Q("tr")
                .add(Q("td")
                  .add(Q("hr"))))
              .add(Q("tr")
                .add(bodyTd))))))
      .add(Q("table")
        .klass("main")
        .add(Q("tr")
          .add(Q("td")
            .add(Q("hr"))))
        .add(Q("tr")
          .add(Q("td")
            .style("text-align: right;color:#808080;font-size:x-small;")
            .html('- © ºDeme. ${Model.appName} (${Model.appVersion}) -'))))
      .add(Ui.upTop("up"))
      .adds(It.range(20).map(i -> Q("p").html("&nbsp;")))
    ;
  }

  public static function update (): Void {
    final mods = It.fromMap(Model.modules)
      .map(tp -> tp.e1)
      .sort(Str.compare)
      .map(e -> Vmenu.option(e, e, () -> Model.selMod(e)))
      .to()
    ;
    mods.unshift(Vmenu.separator());
    mods.unshift(Vmenu.title(_("Modules")));
    final vmenu = new Vmenu(mods, Model.module);

    leftTd.removeAll().add(vmenu.wg);

    final snippets = Model.modules[Model.module];
    indexTd.removeAll().add(mkIndex(snippets.map(e -> Model.snippetName(e))));

    bodyTd.removeAll().add(mkBody(snippets));

    if (Model.snippet == "") {
      js.Browser.window.scroll(0, 0);
    } else {
      Q("#" + Model.snippet).e.scrollIntoView(true);
    }
  }

  static function mkIndex (entries: Array<String>): Domo {
    entries.sort(Str.compare);
    var cols = 5;
    if (entries.length > 5 && entries.length < 10) {
      cols = 3;
    }
    final rows = Std.int((entries.length - 1) / cols) + 1;
    final trs: Array<Domo> = [];
    for (r in 0...rows) {
      final tr = Q("tr");
      for (c in 0...cols) {
        final ix = c * rows + r;
        if (ix < entries.length) {
          final e = entries[ix];
          tr.add(Q("td")
            .add(Ui.link(ev -> Model.selSnip(e))
              .klass("link")
              .text(e)))
          ;
        } else {
          tr.add(Q("td"));
        }
      }
      trs.push(tr);
    }

    return Q("table")
      .klass("main")
      .adds(trs)
    ;
  }

  static function mkBody (snippets: Array<Snippet>): Domo {
    final trs: Array<Domo> = [];
    var isFirst = true;
    for (s in snippets) {
      if (isFirst) {
        isFirst = false;
      } else {
        trs.push(Q("tr").add(Q("td").add(Q("hr"))));
      }

      trs.push(Q("tr")
        .add(Q("td")
          .att("id", Model.snippetName(s))
          .add(Q("pre")
            .style("color:#006080;")
            .text(s.id)))
      );
      trs.push(Q("tr")
        .add(Q("td")
          .add(Q("table")
            .add(Q("tr")
              .add(Q("td")
                .add(Q("pre")
                  .klass("frame")
                  .text(s.code))))))
      );
      trs.push(Q("tr")
        .add(Q("td")
          .add(Q("table")
            .add(Q("tr")
              .add(Q("td")
                .add(Q("pre")
                  .klass("frame0")
                  .text(s.doc))))))
      );
    }

    return Q("table")
      .klass("main")
      .adds(trs)
    ;
  }

}
