class Test {
  public static function main () {
    final tx1 = sys.io.File.getContent("data/f1.txt");
    if (tx1 == "") trace ("Reading of 'f1.txt' ok.");
    else trace ("Reading of 'f1.txt' failed!!!.");
    final tx2 = sys.io.File.getContent("data/f2.txt");
    if (tx2 == "A text\nof 3 lines\n") trace ("Reading of 'f2.txt' ok.");
    else trace ("Reading of 'f2.txt' failed!!!.");
  }
}
