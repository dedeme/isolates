
import dm.Mac;
import dm.Js;

enum TstType {A; B;}

@:build(dm.Mac.enumeration())
class JsTstType {
}

@:build(dm.Mac.record([
  "v0: Array<Array<Bool>>",
  "v1: Int",
  "v3: Map<String, Map<String, Int>>",
  "v4: Array<TstType>",
  "v5: Array<Tst>"
], true))
class Tst {


}

class Main {
  public static function main() {
    final t = new Tst(
      [[true]], 32, ["a" => ["aa" => 1, "bb" => 2]], [A, B, A], [null]
    );

    trace(t.v1 == 32);

    final t2 = new Tst([], 12, [], [], [t]);
    final t2Js = t2.toJs();

    final t3 = Tst.fromJs(t2Js);
    trace(t3.toJs().to() == t2Js.to());

    trace(JsTstType.to(B).to() == "\"B\"");
    trace(JsTstType.from(Js.ws("B")) == B);
  }

}
