// Copyright 03-Dic-2021 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Template to build C generics.
///
///      Use: genericsc [path] container[s] content
///      where
///      path: (both | include | src) relativePath
///        both   : Set 'include' and 'src' path.
///        include: Set .h file path.
///        src    : Set .c file path.
///        Its default value is 'include include src src'.
///        Include path must start with 'include'.
///      container: One or more of (H)ash (L)ist (M)ap (A)rr (K)v (R)s (O)pt
///                 (e.g. 'A', 'AOK', ...)
///      element: Realative path of type header without '.h'.
///               System types are prefixed with '/'
///               (e.g. '/char', 'Acc', 'data/Per')
///
/// Containers are placed in subdirectories of 'include' path and 'src' path
/// with the name of their element type.

using StringTools;
import dm.Dec;
import dm.Js;
import dm.Path;
import dm.Opt;
import dm.File;

class Main {
  static final endMark = "\n//--// Not remove";
  static final templatesPath = "/home/deme/.dmHxApp/genericsc";

  static var includePath = "include";
  static var srcPath = "src";
  static var container: String;
  static var cName: String;
  static var cPath: String;
  static var basicElementName: String;
  static var eName: String;
  static var ePath: Option<String>;
  static var prefix: String;

  static function help () {
    return "
      Use: genericsc [path] container[s] content
      where
      path: (both | include | src) relativePath
        both   : Set 'include' and 'src' path.
        include: Set .h file path.
        src    : Set .c file path.
        Its default value is 'include include src src'.
        Include path must start with 'include'.
      container: One or more of (H)ash (L)ist (M)ap (A)rr (K)v (R)s (O)pt
                 (e.g. 'A', 'AOK', ...)
      element: Realative path of type header without '.h'.
               System types are prefixed with '/'
               (e.g. '/char', 'Acc', 'data/Per')
    ".replace("      ", "");
  }

  static function err (msg: String) {
    Sys.println(msg + "\n" + help());
    Sys.exit(1);
  }

  static function test () {
    Sys.println("include path    : " + includePath);
    Sys.println("src path        : " + srcPath);
    Sys.println("cName           : " + cName);
    Sys.println("cPath           : " + cPath);
    Sys.println("basicElementName: " + basicElementName);
    Sys.println("eName           : " + eName);
    Sys.println("ePath           : " + ePath);
    Sys.println("prefix          : " + prefix);
  }

  static function capitalize (s: String): String {
    return s.substring(0, 1).toUpperCase() + s.substring(1);
  }

  static function uncapitalize (s: String): String {
    return s.substring(0, 1).toLowerCase() + s.substring(1);
  }

  static function cat(path: String, name: String): String {
    return path == "" ? name : Path.cat([path, name]);
  }

  static function containersList(containers: String): Array<String> {
    function add (list: Array<String>, subs: Array<String>, ct: String) {
      if (!list.contains(ct)) list.push(ct);

      for (sb in subs) {
        final ct2 = ct + sb;
        if (!list.contains(ct2)) list.push(ct2);
      }
    }

    final subs = containers.length > 1
      ? containersList(containers.substring(1))
      : []
    ;

    final r = subs.copy();
    switch (containers.charAt(0)) {
      case "O":
        add(r, subs, "O");
      case "A":
        add(r, subs, "O");
        add(r, subs, "A");
      case "K":
        add(r, subs, "K");
      case "M":
        add(r, subs, "O");
        add(r, subs, "K");
        add(r, subs, "OK");
        add(r, subs, "AK");
        add(r, subs, "M");
      case "R":
        add(r, subs, "R");
      case "L":
        add(r, subs, "O");
        add(r, subs, "A");
        add(r, subs, "L");
      case "H":
        add(r, subs, "O");
        add(r, subs, "K");
        add(r, subs, "OK");
        add(r, subs, "AK");
        add(r, subs, "LK");
        add(r, subs, "H");
      case c: err("Container " + c + " is unknown");
    }

    return r;
  }

  static function replaces (isContainer: Bool, tx: String): String {
    tx = tx.replace("@date@", DateTools.format(Date.now(), "%d-%b-%Y"))
      .replace("@defineId@",
          ( cPath == ""
              ? ""
              : cPath.toUpperCase().replace("/", "_") + "_"
          ) +  cName.toUpperCase() + "_H"
        )
      .replace("@hpath@", cat(cPath, cName + ".h"))
      .replace("@type@", cName)
      .replace("@typePath@", cat(cPath, ""))
      .replace("@prefix@", prefix)
      .replace("@subtype@", eName)
      .replace("@to@",
        switch (eName) {
          case "char": "js_ws";
          case "int": "js_wi";
          case "double": "js_wd";
          default: uncapitalize(eName) + "_to_js";
        })
      .replace("@from@",
        switch (eName) {
          case "char": "js_rs";
          case "int": "js_ri";
          case "double": "js_rd";
          default: uncapitalize(eName) + "_from_js";
        })
      .replace("\n@subtypeInclude@\n",
          switch (ePath) {
            case None: isContainer
              ? "\n#include \"" + cat(cPath, eName) + ".h\"\n"
              : ""
            ;
            case Some(p): "\n#include \"" +
              cat(isContainer ? cPath : p, eName) +
              ".h\"\n"
            ;
          }
        )
    ;

    return tx;
  }

  static function processInclude (isContainer: Bool): Void {
    if (!File.exists(includePath))
      err("Directory '" + includePath + "' not found");
    var parent = cat(includePath, basicElementName);
    if (!File.exists(parent))
      File.mkdir(parent);
    final target = Path.cat([parent, cName + ".h"]);
    var postfix = endMark + "\n\n#endif";
    if (File.exists(target)) {
      final tx = File.read(target);
      final ix = tx.indexOf(endMark);
      if (ix == -1)
        err("Mark '" + endMark + "' not found in '" + target + "'");
      postfix = tx.substring(ix);
    }

    final tx = File.read(Path.cat([templatesPath, container + ".h"]));
    File.write(target, replaces(isContainer, tx) + postfix);
  }

  static function processSrc () {
    if (!File.exists(srcPath))
      err("Directory '" + srcPath + "' not found");
    var parent = cat(srcPath, basicElementName);
    if (!File.exists(parent))
      File.mkdir(parent);
    final target = Path.cat([parent, cName + ".c"]);
    var postfix = endMark + "\n\n";
    if (File.exists(target)) {
      final tx = File.read(target);
      final ix = tx.indexOf(endMark);
      if (ix == -1)
        err("Mark '" + endMark + "' not found in '" + target + "'");
      postfix = tx.substring(ix);
    }

    final tx = File.read(Path.cat([templatesPath, container + ".c"]));
    File.write(target, replaces(false, tx) + postfix);
  }

  public static function main () {
    var args = Sys.args();
    var ix = 0;
    if (args.length < ix + 1) err("Arguments are missing");
    var a = args[ix++];
    if (a == "both" || a == "include" || a == "src") {
      while (true) {
        if (args.length < ix + 1) err("Expected path at end of command");
        var path = args[ix++];
        switch (a) {
        case "both": includePath = srcPath = path;
        case "include": includePath = path;
        default: srcPath = path;
        }
        if (args.length < ix + 1)
          err("Expected container name at end of command");
        a = args[ix++];
        if (a != "both" && a != "include" && a != "src") break;
      }
    }
    if (!includePath.startsWith("include"))
      err("Include path must start with 'include'");

    final containers = a;
    for (i in 0...containers.length) {
      final l = a.charAt(i);
      if ("HLMAKRO".indexOf(l) == -1)
        err("Cotainer(s) value '" + a +
          "' contains the wrong letter '" + l + "'"
        );
    }

    if (args.length < ix + 1) err("Element path is missing");
    final element = args[ix++];

    final csList = containersList(containers);

    for (ct in csList) {
      final isContainer = ct.length > 1;
      final cprefix = ct;
      final eprefix = ct.substring(1);

      if (args.length > ix)
        if (args.length > ix) err("Extra arguments: " + args[ix] + " ...");

      basicElementName = Path.name(element);
      eName = eprefix + basicElementName;
      ePath = element.startsWith("/") ? None : Some(Path.parent(element));
      cName = cprefix + basicElementName;
      cPath =  includePath == "include"
        ? eName
        : cat(includePath.substring(8), basicElementName)
      ;
      prefix = uncapitalize(cName) + "_";

      container = switch (cName.charAt(0)) {
        case "O": "opt";
        case "A": "arr";
        case "K": "kv";
        case "M": "map";
        case "R": "rs";
        case "L": "list";
        default: "hash";
      }

      processInclude(isContainer);
      processSrc();
    }
  }
}

