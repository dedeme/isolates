// Copyright 23-Nov-2021 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Tuple char*-char*

#ifndef DMC_CHAR_KSTR_H
  #define DMC_CHAR_KSTR_H

struct kStr_KStr {
  char *k;
  char *v;
};

/// Tuple of 'char*, void*'.
typedef struct kStr_KStr KStr;

/// Constructor of a tuple of 'char*, void*'.
KStr *kStr_new (char *k, char *v);

#endif

//--// Not remove

#endif