// Copyright 23-Nov-2021 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Option data.

#ifndef DMC/CHAR_OSTR_H
  #define DMC/CHAR_OSTR_H

/// Option structure
typedef struct oStr_OStr OStr;

/// Returns a none option.
OStr *oStr_mk_none();

/// Returns an option with a value.
OStr *oStr_mk_some(char *value);

/// Returns '1' if 'opt' is none and 0 otherwise.
int oStr_none(OStr *opt);

/// Returns the value of 'opt' or raise a FAIL if it is none.
char *oStr_some(OStr *opt);

/// Raise a fail if 'opt' is empty with 'msg' as message.
char *oStr_esome (OStr *opt, char *msg);

/// Returns 'value' if 'opt' is empty.
char *oStr_osome (OStr *opt, char *value);

/// Returns the value of 'opt' or NULL if 'this' is empty.
char *oStr_nsome (OStr *opt);

///
char *oStr_to_js (OStr *opt, char *(*to)(char *e));

///
OStr *oStr_from_js (char *js, char *(*from)(char *ejs));

//--// Not remove

#endif