// Copyright 23-Nov-2021 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Tuple char*-void*

#ifndef DMC_KV_H
  #define DMC_KV_H

struct kv_Kv {
  char *k;
  void *v;
};

/// Tuple of 'char*, void*'.
typedef struct kv_Kv Kv;

/// Constructor of a tuple of 'char*, void*'.
Kv *kv_new (char *k, void *v);

#endif

//--// Not remove

#endif