// Copyright 23-Nov-2021 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

#include "dmc/Kv.h"
#include "dmc/DEFS.h"

Kv *kv_new (char *k, void *v) {
  Kv *this = MALLOC(Kv);
  this->k = k;
  this->v = v;
  return this;
}

//--// Not remove

