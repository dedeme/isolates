// Copyright 23-Nov-2021 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

#include "dmc/char/KStr.h"
#include "dmc/DEFS.h"

KStr *kStr_new (char *k, char *v) {
  KStr *this = MALLOC(KStr);
  this->k = k;
  this->v = v;
  return this;
}

//--// Not remove

