// Copyright 23-Nov-2021 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

#include "dmc/char/OStr.h"
#include <string.h>
#include "dmc/DEFS.h"
#include "dmc/err.h"

struct oStr_OStr {
  char *value;
};

OStr *oStr_mk_none() {
  OStr *this = MALLOC(OStr);
  this->value = NULL;
  return this;
}

OStr *oStr_mk_some(char *value) {
  OStr *this = MALLOC(OStr);
  this->value = value;
  return this;
}

int oStr_none(OStr *opt) {
  return !opt->value;
}

char *oStr_some(OStr *opt) {
  if (opt->value) return opt->value;
  FAIL("Option is none");
  return NULL; // Unreachable
}

char *oStr_esome (OStr *opt, char *msg) {
  if (opt->value) return opt->value;
  FAIL(msg);
  return NULL; // Unreachable
}

char *oStr_oget (OStr *opt, char *value) {
  return opt->value ? opt->value : value;
}

char *oStr_nget (OStr *opt) {
  return opt->value;
}

char *oStr_to_js (OStr *opt, char *(*to)(char *e)) {
  return opt->value ? to(opt->value) : "null";
}

OStr *oStr_from_js (char *js, char *(*from)(char *jse)) {
  return strcmp(js, "null") ? oStr_mk_some(from(js)) : oStr_mk_none();
}
//--// Not remove

#endif