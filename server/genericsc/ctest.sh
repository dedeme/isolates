#!/bin/bash

PRG="genericsc"

export HAXE_STD_PATH=/dm/haxe/std
haxe build-test.hxml

echo '#!/bin/bash' > bin/$PRG
echo '/dm/hashlink/hl /dm/dmHaxe/app/server/'$PRG'/bin/Main.hl $*' >> bin/$PRG
chmod +x bin/$PRG
