// Copyright 11-Sep-2021 ºDeme
// GNU General Public License - V3 <http://www.gnu.org/licenses/>

/// Finds any occurence of any string in 'textsToFind' in files with extension
/// 'fileExtensions'.
/// The script search in the work directory and its subdirectories.
///
/// You should replace values of 'textsToFind' and 'fileExtensions'.

import dm.File;

class TextFind {
  static final textsToFind = [
    "Server writing client failed."
  ];
  static final fileExtensions = ["hx", "go"];


  public static function main () {
    final results = readDirectory("");
    for (r in results) {
      Sys.println(r.file + ":" + r.line);
    }
  }

  public static function readDirectory(relativeDir: String): Array<Rs> {
    final dir = "./" + relativeDir;
    var r: Array<Rs> = [];

    for (f in File.dir(dir)) {
      final fpath = dir + "/" + f;
      if (File.isDirectory(fpath)) {
        r = r.concat(readDirectory(
          relativeDir == "" ? f : relativeDir + "/" + f
        ));
        continue;
      }

      r = r.concat(readFile(fpath));
    }

    r.sort((r1, r2) -> r1.file == r2.file
      ? r1.line - r2.line
      : dm.Str.compare(r1.file, r2.file
    ));
    return r;
  }

  public static function readFile(f: String): Array<Rs> {
    var r = [];
    if (!fileExtensions.contains(haxe.io.Path.extension(f))) return r;

    final file = File.ropen(f);
    var nLine = 1;
    File.readLines(file, (l -> {
      for (tx in textsToFind) {
        if (l.indexOf(tx) != -1) {
          r.push(new Rs(f, nLine));
          break;
        }
      }
      ++nLine;
    }));
    file.close();

    return r;
  }
}

@:build(dm.Mac.record([
  "file: String",
  "line: Int"
  ]))
class Rs {}
